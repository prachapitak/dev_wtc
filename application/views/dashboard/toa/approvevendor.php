<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?><br>
   

 <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_extension_fixed_columns.js"></script>

</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
								<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">Approve Vendor  </span> - ตรวจสอบข้อมูลคู่ค้า </h4>
						</div>
                        
                        <?php print_r($this->session->userdata());?>
                        
                        
                        <?php $this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php $this->load->view('dashboard/toa/noti');?>
					
				
				
                                    

				<!-- Content area -->
				<div class="content">
                
               <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">รายการคู่ค้าที่รอการตรวจสอบข้อมูลจาก ท่าน</h5>
							
						</div>

						<div class="panel-body">
                        
                
                          <div class="alert alert-primary no-border">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">คำแนะนำ!</span> รู้หรือไม่ท่านสามารถ ท่านสามารถ กดปุ่ม 
							 <code>Reject</code> เพื่อยกเลิกคู่ค้าออกจาก ระบบ หรือกดปุ่ม <code>ReEdit</code> เพื่อให้ระบบทำการแจ้งไปที่ Vendor ให้เข้ามาทำการแก้ไขข้อมูลอีกครั้ง และ เข้า สู่ขั้นตอนการ Approve อีกครั้ง 
								    </div>
                             
						</div>

						<table class="table datatable-basic">
							<thead>
								<tr>
                                <th>Company</th>
												<th >Created</th>
												<th >Email</th>
												<th >Tel.</th>
												<th >Status</th>
												<th class="text-center">Actions</th>
                                
									
								</tr>
							</thead>
							<tbody>
                            
                            <?php if($getvendorapprove){
								foreach($getvendorapprove as $getvendorapproves){
									
									$getvendorprofile1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getvendorapproves->vendor_id.'"');
									$getvendorprofile3 = $this->Query_Db->record($this->db->dbprefix.'vp3','vp3_ref_id =  "'.$getvendorapproves->vendor_id.'"');
									
									?>
                                    <tr>
												<td>
													<div class="media-left media-middle">
														<a href="#"><img src="<?php echo base_url(); ?>assets/images/tmp.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="<?php echo base_url(); ?>/dashboard/approvevendordetail?vid=<?php echo $getvendorapproves->vendor_id;?>" class="text-default text-semibold"><?php echo $getvendorapproves->vendor_comname;?></a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															บันทึกข้อมูลเมื่อ : <?php echo $getvendorapproves->vendor_create;?>  
														</div>
													</div>
												</td>
												<td><span class="text-muted"> <?php echo $getvendorprofile3->vp3_upby;?></span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> <?php echo $getvendorapproves->vendor_cemail;?> </span></td>
												<td><h6 class="text-semibold"><?php echo $getvendorprofile1->vp1_tel;?> ,<?php echo $getvendorprofile1->vp1_etel;?> </h6></td>
												<td><?php if($getvendorapproves->vendor_st == "S2"){ echo '<span class="label bg-warning">รอการตรวจสอบ</span>'; }else echo '';?></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="<?php echo base_url(); ?>/dashboard/approvevendordetail?vid=<?php echo $getvendorapproves->vendor_id;?>"><i class="icon-file-stats"></i>  ดูข้อมูลคู่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																 
															</ul>
														</li>
													</ul>
												</td>
											</tr>
                                    <?php
									
									
									}
								
								}
							
							
							?>
                            
                            
                            
										
											
								
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->

               
						
                        
                          <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">รายการคู่ค้าที่อยู่ระหว่างการ อนุมัติ</h5>
							
						</div>

						<div class="panel-body">
                        
                
                          <table class="table datatable-fixed-left" width="100%">
							<thead>
						        <tr>
                                 				
                                                <th >Company</th>
                                                <th >ประเภท Vendor</th>
												
												<th >ผู้ตรวจสอบฝ่ายจัดซื้อ</th>
                                             
												<th >หัวหน้าฝ่ายจัดซื้อ</th>
                                                <th >ผู้ตรวจสอบฝ่ายบัญชี</th>
                                                <th > AVP อนุมัติ</th>
                                                 
											 
						            <th class="text-center">Actions</th>
						        </tr>
						    </thead>
						    <tbody>
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-18 15:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-success text-success-300">Completed</span>   </div>
														<div class="text-muted text-size-small">
															
															ราคาสินค้าถูกกว่าเจ้าอื่นถึง 20%  <br/>และยังสามารถทดแทนสินค้าเดิมได้..<br/>
                                                            2017-06-18 16:10:04
														</div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-success text-success-300">Review</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                        ข้อมูลการเงินปกติค่ะตรวจสอบแล้ว <br/>แต่เอกสาร Book Bank ยังไม่ชัดเท่าไหร่	 <br/> 2017-06-18 16:10:04
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                            
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-17 13:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-warning text-warning-300">In Progress</span>   </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                      
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-danger text-danger-300">Not Started</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                                
                                   <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-18 15:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-success text-success-300">Completed</span>   </div>
														<div class="text-muted text-size-small">
															
															ราคาสินค้าถูกกว่าเจ้าอื่นถึง 20%  <br/>และยังสามารถทดแทนสินค้าเดิมได้..<br/>
                                                            2017-06-18 16:10:04
														</div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-success text-success-300">Review</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                        ข้อมูลการเงินปกติค่ะตรวจสอบแล้ว <br/>แต่เอกสาร Book Bank ยังไม่ชัดเท่าไหร่	 <br/> 2017-06-18 16:10:04
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                            
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-17 13:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-warning text-warning-300">In Progress</span>   </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                      
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-danger text-danger-300">Not Started</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                                
                             
                             
                                <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-18 15:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-success text-success-300">Completed</span>   </div>
														<div class="text-muted text-size-small">
															
															ราคาสินค้าถูกกว่าเจ้าอื่นถึง 20%  <br/>และยังสามารถทดแทนสินค้าเดิมได้..<br/>
                                                            2017-06-18 16:10:04
														</div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-success text-success-300">Review</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                        ข้อมูลการเงินปกติค่ะตรวจสอบแล้ว <br/>แต่เอกสาร Book Bank ยังไม่ชัดเท่าไหร่	 <br/> 2017-06-18 16:10:04
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                            
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-17 13:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-warning text-warning-300">In Progress</span>   </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                      
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-danger text-danger-300">Not Started</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                                
                             
                                
                             
                            
                            
                              
                            </tbody>
                            
						</table>
                             
						</div>

						
                        
                        
                        
                        
                        
					</div>
					<!-- /basic datatable -->




                    
                    
			
                    

					
                    
		        

		         



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   



  
 



</body>




</html>