<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title;?></title>
<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	
    
    
   
    
    
    <!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/media/fancybox.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/user_pages_team.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->
    
   
    
 
</head>

<body>



	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

		

			<!-- Main content -->
			<div class="content-wrapper">

				
                
                
                
                
                
                
                

				
                
                <!-- Content area -->
				<div class="content">
                
                
                <div class="panel panel-flat">
						
						<div class="panel-body panel border-top-primary">
							
                    
                         
                         
                            
                            
                            	<!-- Dropdown menu -->
                                
                                 <h6 class="content-group text-semibold">
						Productivity Content
						<small class="display-block"> Productivity detail click line for more informaion. <br>
 </small> 
                        
                        
                        <div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="glyphicon glyphicon-plus text-primary"></i><span>Add Line</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-magazine text-primary"></i> <span>Report</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-bubble-notification text-primary"></i> <span>Help</span></a>
							</div>
						</div>
                        
                        
					</h6> 
                                 
                               
					
                            
                            
                            
                            
                            
						</div>
					</div>
                

					<div class="row">
						
                        
                        <?php
						
						for ($x = 0; $x <= 19; $x++) {
   ?>
   
   <div class="col-lg-3 col-md-6">
							<div class="panel panel-body">
								<div class="media">
									<div class="media-left">
										<a href="<?php echo base_url(); ?>assets/images/product01.png" data-popup="lightbox">
											<img src="<?php echo base_url(); ?>assets/images/product01.png" class="img-circle img-lg" alt="">
										</a>
									</div>

									<div class="media-body">
										<h6 class="media-heading">Dutch Mill นมเปรี้ยว 4 in 1</h6>
										<span class="text-muted">รสสตอเบอร์รี่</span>
									</div>

									<div class="media-right media-middle">
										<ul class="icons-list icons-list-vertical">
					                    	<li class="dropdown">
						                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
						                    	<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-file-eye2 pull-right"></i> Display </a></li>
													<li><a href="#"><i class="icon-pencil7 pull-right"></i> Capability Update </a></li>
													
													<li class="divider"></li>
													<li><a href="#"><i class="icon-bin pull-right"></i> Delete Line</a></li>
												</ul>
					                    	</li>
				                    	</ul>
									</div>
								</div>
							</div>
						</div>
   <?php
} 
                        
						?>

						

						
					</div>
                    

					
                    
                    
                    
					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2018. <a href="#">WTC</a> 
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
