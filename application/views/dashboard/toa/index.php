<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs2');?>



</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">Dashboard  </span> - หน้าหลัก TOA PAINT  <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php $this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php $this->load->view('dashboard/toa/noti');?>
					
				
				
                                    

				<!-- Content area -->
				<div class="content">
                
                <!-- Quick stats boxes -->
							<div class="row">
								<div class="col-lg-3">

									<!-- Members online -->
									<div class="panel bg-teal-400">
										<div class="panel-body">
                                        
                                        
                                            
                                            

											<h5 class="no-margin"> กว่า <span style="font-size:50px;">450</span> Vendor</h5>
											 เป็นคู่ค้าของ TOA GROUP
											<div class="text-muted text-size-small">Update : <?php echo date("Y-m-d")?></div>
										</div>

										<div class="container-fluid">
											<div id="members-online"></div>
										</div>
									</div>
									<!-- /members online -->

								</div>

								<div class="col-lg-3">

									<!-- Current server load -->
									<div class="panel bg-pink-400">
										<div class="panel-body">
											

											<h5 class="no-margin">
                                            กว่า <span style="font-size:50px;">49.4%</span>
                                            </h5>
											เป็นของ Packing (PK)
											<div class="text-muted text-size-small">Update : <?php echo date("Y-m-d")?></div>
										</div>

										<div id="server-load"></div>
									</div>
									<!-- /current server load -->

								</div>

<div class="col-lg-3">

									<!-- Members online -->
									<div class="panel bg-success-400">
										<div class="panel-body">
                                        
                                        
                                            
                                            
<h5 class="no-margin">
                                             กว่า <span style="font-size:50px;">98 % </span> 
                                           </h5>
                                           
											
											เป็นสินค้ามีเอกสารรับรอง
											<div class="text-muted text-size-small">Update : <?php echo date("Y-m-d")?></div>
										</div>

										<div class="container-fluid">
											<div id="members-online"></div>
										</div>
									</div>
									<!-- /members online -->

								</div>
								<div class="col-lg-3">

									<!-- Today's revenue -->
									<div class="panel bg-blue-400">
										<div class="panel-body">
											

											<h5 class="no-margin">
                                             กว่า <span style="font-size:50px;">1k</span> รายการ
                                           </h5>
											พร้อมจะขายให้กับ TOA GROUP
											<div class="text-muted text-size-small">Update : <?php echo date("Y-m-d")?></div>
										</div>

										<div id="today-revenue"></div>
									</div>
									<!-- /today's revenue -->

								</div>
							</div>
							<!-- /quick stats boxes -->
						<div class="row">
                        <div class="col-lg-12">

							<!-- Marketing campaigns -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Last Vendor Approved | คู่ค้าล่าสุดที่ผ่านการตรวจสอบแล้ว</h6>
								
								</div>


							<div class="table-responsive">
									<table class="table text-nowrap">
										<thead>
											<tr>
												<th>Company</th>
												<th class="col-md-2">Created</th>
												<th class="col-md-2">Email</th>
												<th class="col-md-2">Tel.</th>
												<th class="col-md-2">Status</th>
												<th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
											</tr>
										</thead>
										<tbody>
											
											<tr>
												<td>
													<div class="media-left media-middle">
														<a href="approveddetailview.php" target="_blank"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="approveddetailview.php" target="_blank" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															ผ่านการอนุมัติจากคุณ Napa
														</div>
													</div>
												</td>
												<td><span class="text-muted"> นิรันด์ ราชบพิตร </span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> sale@newvendor.com</span></td>
												<td><h6 class="text-semibold">02-3456789 ,0893332233</h6></td>
												<td><span class="label bg-success">ผ่านการอนุมัติ</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-file-stats"></i> ดูข้อมูลคุ่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																<li class="divider"></li>
																<li><a href="#"><i class="icon-gear"></i> แจ้งยกเลิก</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
											<tr>
												<td>
													<div class="media-left media-middle">
														<a href="approveddetailview.php" target="_blank"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="approveddetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์02 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															ผ่านการอนุมัติจากคุณ Napa
														</div>
													</div>
												</td>
												<td><span class="text-muted"> พิพัฒน์ รัชประภา </span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> sale@newvendor.com</span></td>
												<td><h6 class="text-semibold">02-3456789 ,0893332233</h6></td>
												<td><span class="label bg-success">ผ่านการอนุมัติ</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-file-stats"></i> ดูข้อมูลคุ่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																<li class="divider"></li>
																<li><a href="#"><i class="icon-gear"></i> แจ้งยกเลิก</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
                                            <tr>
												<td>
													<div class="media-left media-middle">
														<a href="approveddetailview.php" target="_blank"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="approveddetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์ จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															ผ่านการอนุมัติจากคุณ Napa
														</div>
													</div>
												</td>
												<td><span class="text-muted"> Jonathan Smith </span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> sale@newvendor.com</span></td>
												<td><h6 class="text-semibold">02-3456789 ,0893332233</h6></td>
												<td><span class="label bg-success">ผ่านการอนุมัติ</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-file-stats"></i> ดูข้อมูลคุ่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																<li class="divider"></li>
																<li><a href="#"><i class="icon-gear"></i> แจ้งยกเลิก</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
                                            <tr>
												<td>
													<div class="media-left media-middle">
														<a href="approveddetailview.php" target="_blank"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="approveddetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์03 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															ผ่านการอนุมัติจากคุณ Napa
														</div>
													</div>
												</td>
												<td><span class="text-muted"> ก่อเกีรติ ณ รามสอง </span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> sale@newvendor.com</span></td>
												<td><h6 class="text-semibold">02-3456789 ,0893332233</h6></td>
												<td><span class="label bg-success">ผ่านการอนุมัติ</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-file-stats"></i> ดูข้อมูลคุ่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																<li class="divider"></li>
																<li><a href="#"><i class="icon-gear"></i> แจ้งยกเลิก</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
                                            <tr>
												<td>
													<div class="media-left media-middle">
														<a href="approveddetailview.php" target="_blank"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="approveddetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์04 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															ผ่านการอนุมัติจากคุณ Napa
														</div>
													</div>
												</td>
												<td><span class="text-muted"> นพพร บวรนพคุณ </span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> sale@newvendor.com</span></td>
												<td><h6 class="text-semibold">02-3456789 ,0893332233</h6></td>
												<td><span class="label bg-success">ผ่านการอนุมัติ</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-file-stats"></i> ดูข้อมูลคุ่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																<li class="divider"></li>
																<li><a href="#"><i class="icon-gear"></i> แจ้งยกเลิก</a></li>
															</ul>
														</li>
													</ul>
												</td>
											</tr>
											
										</tbody>
									</table>
								</div>
                            
                            
								
								
							</div>
							<!-- /marketing campaigns -->


							

							



						</div>
                        
                     <!--   <div class="col-lg-12">
                        
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">สินค้าล่าสุดที่ผ่านการอนุมัติ </h6>
									
			                	</div>

								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<ul class="media-list content-group">
												<li class="media stack-media-on-mobile">
				                					<div class="media-left">
														<div class="thumb">
															<a href="#">
																<img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
																<span class="zoom-image"><i class="icon-play3"></i></span>
															</a>
														</div>
													</div>

				                					<div class="media-body">
														<h6 class="media-heading"><a href="#">Premium Resin 01</a></h6>
							                    		<ul class="list-inline list-inline-separate text-muted mb-5">
							                    			<li>14 minutes ago</li>
							                    		</ul>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur...
														
													</div>
												</li>

												<li class="media stack-media-on-mobile">
				                					<div class="media-left">
														<div class="thumb">
															<a href="#">
																<img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
																<span class="zoom-image"><i class="icon-play3"></i></span>
															</a>
														</div>
													</div>

				                					<div class="media-body">
														<h6 class="media-heading"><a href="#">Premium Film Former </a></h6>
							                    		<ul class="list-inline list-inline-separate text-muted mb-5">
							                    		      			<li>12 days ago</li>
							                    		</ul>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, provident...					
													</div>
												</li>
                                                
                                                <li class="media stack-media-on-mobile">
				                					<div class="media-left">
														<div class="thumb">
															<a href="#">
																<img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
																<span class="zoom-image"><i class="icon-play3"></i></span>
															</a>
														</div>
													</div>

				                					<div class="media-body">
														<h6 class="media-heading"><a href="#">Premium Resin 01</a></h6>
							                    		<ul class="list-inline list-inline-separate text-muted mb-5">
							                    			<li>14 minutes ago</li>
							                    		</ul>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur...
														
													</div>
												</li>
                                                
                                                <li class="media stack-media-on-mobile">
				                					<div class="media-left">
														<div class="thumb">
															<a href="#">
																<img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
																<span class="zoom-image"><i class="icon-play3"></i></span>
															</a>
														</div>
													</div>

				                					<div class="media-body">
														<h6 class="media-heading"><a href="#">Premium Resin 01</a></h6>
							                    		<ul class="list-inline list-inline-separate text-muted mb-5">
							                    			<li>14 minutes ago</li>
							                    		</ul>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur...
														
													</div>
												</li>
                                                
                                                <li class="media stack-media-on-mobile">
				                					<div class="media-left">
														<div class="thumb">
															<a href="#">
																<img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
																<span class="zoom-image"><i class="icon-play3"></i></span>
															</a>
														</div>
													</div>

				                					<div class="media-body">
														<h6 class="media-heading"><a href="#">Premium Resin 01</a></h6>
							                    		<ul class="list-inline list-inline-separate text-muted mb-5">
							                    			<li>14 minutes ago</li>
							                    		</ul>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur...
														
													</div>
												</li>
	
    		
												
											</ul>
										</div>

										
									</div>
								</div>
							</div>
							
                        </div>-->
                        </div>


                    
                    
			
                    

					
                    
		        

		         


		                 <!-- Primary modal -->
					<div id="modal_theme_primary" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									
									<h6 class="modal-title">ข้อความจากระบบ</h6>
								</div>

								<div class="modal-body">
									<h6 class="text-semibold">แจ้งผู้ค้ารายใหม่</h6>
									<p> ทางบริษัทได้รับข้อมูลของท่านแล้ว ขณะนี้อยู่ระหว่างการตรวจสอบจากทางเจ้าหน้าที่  </p>

									<hr>

									
									<p>หากตรวจพบว่าข้อมูลของท่านไม่ถูกต้อง บริษัทฯ ขอสงวนสิทธิ์ ในการสั่งซื้อสินค้าจาก ท่าน หรือ หากท่านไม่ได้รับความเป็นธรรมในการให้บริการจาก ฝ่ายจัดซื้อโปรดติดต่อ Tel. 02 345 6789 หน่วยงาน ตรวจสอบ </p>
									
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">รับทราบ</button>
									
								</div>
							</div>
						</div>
					</div>
					<!-- /primary modal -->



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
    <!-- Vertical form modal -->
					<div id="modal_form_vertical" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มสินค้า</h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ผลิต</label>
													<input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สินค้าที่จำหน่าย</label>
													<input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ประเทศที่ผลิต</label>
													<input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
													<input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<label>Load Time</label>
													<input type="text" placeholder="ระบุ Load Time" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Minimum Order</label>
													<input type="text" placeholder="ระบุ Minimum Order" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Packing Size </label>
													<input type="text" placeholder="ระบุ Packing Size" class="form-control">
												</div>
											</div>
										</div>

										
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
													<input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
													<span class="help-block">eg: k.หนูแดง T 023456789</span>
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>MSDS(ไทย/อังกฤษ) </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>เงือนไขในการชำระเงิน</label>
													<input type="text" placeholder="ระบุเงือนไขในการชำระเงิน" class="form-control">
													<span class="help-block">eg: 30 วัน , 90 วัน </span>
												</div>

												<div class="col-sm-6">
                                                
                                              
                                              
                                              <label>สถานะซื้อขายกับ TOA </label> 
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
              					                 
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>

                                                
												
													
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติมที่เกี่ยวกับตัวสินค้า : </label>
		                                    <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
		                                </div>
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


  <!-- Vertical form modal -->
					<div id="modal_form_acc" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มบัญชี / เช็ค </h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อบัญชี</label>
													<input type="text" placeholder="ระบุชื่อบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เลขที่บัญชี</label>
													<input type="text" placeholder="ระบุเลขที่บัญชี" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อธนาคาร</label>
													<input type="text" placeholder="ระบุชื่อธนาคาร" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สาขา</label>
													<input type="text" placeholder="ระบุสาขา " class="form-control">
												</div>
											</div>
										</div>

										
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ดูแลบัญชี</label>
													<input type="text" placeholder="ระบุชื่อผู้ดูแลบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เบอร์โทรติดต่อ</label>
													<input type="text" placeholder="ระบุเบอร์โทรติดต่อ " class="form-control">
												</div>
											</div>
										</div>
                                        								
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
                                                
                                                <label>หมายเหตุ / รายละเอียดเพิ่มเติม : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
                                                
													
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>สำเนาบัญชีธนาคาร  </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


 
<script type="text/javascript">

// Custom tag class

    $('.tokenfield').tokenfield();
    $('.tagsinput-custom-tag-class').tagsinput({
        tagClass: function(item){
            return 'label bg-success';
        }
    });
	
	    // Display values
    $('.tags-input, [data-role="tagsinput"], .tagsinput-max-tags, .tagsinput-custom-tag-class').on('change', function(event) {
        var $element = $(event.target),
            $container = $element.parent().parent('.content-group');

        if (!$element.data('tagsinput'))
        return;

        var val = $element.val();
        if (val === null)
        val = "null";
    
        $('pre.val > code', $container).html( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
        $('pre.items > code', $container).html(JSON.stringify($element.tagsinput('items')));
        Prism.highlightAll();
    }).trigger('change');




function noti(val) {
	
	 new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
	
    //alert("The input value has changed. The new value is: " + val);
}

	 $('#pnotify-successx').on('click', function () {
        new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
    });

 // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();




    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });


	

    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



 var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 1
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
					
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister2.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		/*	
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		*/
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
    });

 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


	
   // With validation
   
	
 $(window).on('load',function(){
	 
	 
	 /*swal({
		 
		  title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบระหว่างนี้ท่านจะไม่สามารถแก้ไข ข้อมูลใดๆ ได้ จนกว่าทางเจ้าหน้าที่ของ TOA GROUP ตรวจสอบข้อมูลเสร็จ",
            confirmButtonColor: "#66BB6A",
            type: "success",
			confirmButtonText: "ตกลง",
		 
			
			   title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบ",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        });*/
	 
	// $('#modal_theme_primary').modal('show');
	 //validation-next
	/* $('#validation-next').click(function(){
		 $('#modal_theme_primary').modal('show');
		 });*/
       // $('#modal_theme_primary').modal('show');
	  
	 /* bootbox.dialog({
                title: "กรุณาระบุรหัสผ่าน",
                message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                        '<form class="form-horizontal">' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">Name</label>' +
                                '<div class="col-md-8">' +
                                    '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                                    '<span class="help-block">Here goes your name</span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">How awesome is this?</label>' +
                                '<div class="col-md-8">' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked">' +
                                            'Really awesomeness' +
                                        '</label>' +
                                    '</div>' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                                            'Super awesome' +
                                        '</label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '</div>',
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            bootbox.alert("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        ); */
    });
	
	 
	/*
	 $('#validation-next').on('click', function() {
        
		  swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
    });*/
	
	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });

 // Custom bootbox dialog with form
   

</script>


</body>




</html>