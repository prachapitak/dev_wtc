<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Home extends CI_Controller {
	class Vendorregister extends CI_Controller {
	
	
	public function __construct() {
		
		parent::__construct();
		// $this->load->helper(array('form', 'url'));
		$this->load->model('Query_Db');
		$this->load->model('Lang_Model');
		$this->load->model('Lang_Menu');
		$this->load->library('email');  // เรียกใช้ email class
		//$this->load->helper('language');
		$this->load->library('upload');
		$this->load->model('Sendmail');
		
	}

	
	public function index()
	{
		
		
		
	if(empty($this->session->userdata('vendor_st'))){
			
			 redirect('./', 'refresh');
			
			}   
			else {
				//$data['title']  = 'Register Vendor Information Purchase Online';
				
				
				if($this->session->userdata('vendor_st') == 'S1'){
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				$data['getuser'] = $getuser;
				$data['title']  = 'New Vendor Register  ';
				
				//$this->load->view('vendorregister/vendorregister',$data);	
				 redirect('./vendorregister/vendorregister', 'refresh',$data);
					
					}else if($this->session->userdata('vendor_st') == 'S2'){
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				$data['getuser'] = $getuser;
				$data['title']  = 'Waiting Vendor Register  ';
				
				//$this->load->view('vendorregister/vendorregister',$data);	
				 redirect('vendor', 'refresh',$data);
					
					}else{
						
						
						
						
						
						
						
						}
				
				
				
				
				}
			
		
		
		
	
	}
	
	public function vendoragreement()
	{
		
		
			
		if(empty($this->session->userdata('tk_token'))){
			
			 redirect('./', 'refresh');
			
			} if(!empty($this->session->userdata('vendor_st'))){
				
				redirect('./vendorregister/vendorregister', 'refresh');
				}  else {
				$data['title']  = 'Register Vendor Information Purchase Online';
				$this->load->view('vendorregister/vendoragreement',$data);	
				
				}
			
			
		
		
		
		
		
				
		
		
	
	}
	
	
	public function vendorcreateacc()
	{
		
		
			
		if(empty($this->session->userdata('tk_token'))){
			
			 redirect('./', 'refresh');
			
			} else if(!empty($this->session->userdata('vendor_st'))){
				
				redirect('./vendorregister/vendorregister', 'refresh');
				} else {
				$data['title']  = 'Register Vendor Information Purchase Online';
				$this->load->view('vendorregister/vendorcreateacc',$data);	
				
				}
			
			
		
		
		
		
		
				
		
		
	
	}


public function vendorsend(){
		

	if(empty($this->session->userdata('vendor_username'))){
				exit;
				}
				else {
				
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
								
				if($getuser){
					
					
					$updatest  = $this->Query_Db->update2($this->db->dbprefix.'vendor','vendor_id='.$getuser->vendor_id.' and vendor_username ="'.$getuser->vendor_username.'" ',array ('vendor_st' => 'S2'));
					
					$sessionuserdata = array(
				'vendor_st' => 'S2',
				);
				
				
				
				$getflow = $this->Query_Db->record($this->db->dbprefix.'flow','flow_id =  "'.$getuser->vendor_flow_id.'"');
				
			
				
			//	$getuser->vendor_flow_id;
				
				$approve_data = array (
				
						'vp_ref_id' => $getuser->vendor_id,
						'vp_flow_id' => $getflow->flow_id,
						'vp_pr_id' => $getflow->flow_pr_id,
						
						'vp_mpr_id' => $getflow->flow_mpr_id,
						'vp_acc_id' =>$getflow->flow_acc_id,
						'vp_avp_id' =>$getflow->flow_avp_id,
						
						'vp_st' =>'0',
						
						
						
						//$qflow
						
						
						);	
						
						
					
				$vp_approve  = $this->Query_Db->insert($this->db->dbprefix.'vp_approve',$approve_data );
				
				
				
					
					
				$getstaff = $this->Query_Db->record($this->db->dbprefix.'users','username =  "'.$getflow->flow_pr_id.'"');
					
					
				if($getstaff){
					
					
			$sendmailtostaff = $this->Sendmail->toa_sendmail_staff($getstaff->fullname_th,$getuser->vendor_cemail,$getstaff->email,$getuser->vendor_comname);
			
			$sendmailtovendor = $this->Sendmail->toa_sendmail_vendor($getstaff->fullname_th,$getuser->vendor_cemail,$getstaff->email,$getuser->vendor_comname);
			
				
				
				
				
				
				
				
				
				
				
				
				$this->session->set_userdata($sessionuserdata);
               
			   $resultapi = json_encode(array('st' => 'true'),true);
			
			
				
				
			   
			   
					
					}
				
				
				
			
							
				
					} else {
						
						 $resultapi = json_encode(array('st' => 'fail'),true);
						}
						
						print_r($resultapi);
						
					
					}
				
				
				

		
		}
		



public function createvendor(){
	
	if(empty($_REQUEST['vendor_username'])){
		
		
		$resultapi = json_encode(array('create_st' => 'fail'),true);
		
		} else {
			
			$check = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$_REQUEST['vendor_username'].'"');
			
			if(!empty($check)){
				
				$resultapi = json_encode(array('create_st' => 'fail',
													'vendor_username' => $_REQUEST['vendor_username']
													),true);
				
				} else {
					
					
					$qflow = $this->Query_Db->record($this->db->dbprefix.'flow','flow_id =  "'.$this->session->userdata('tk_flow_id').'"');
			
					//print_r($qflow);
					$vendor_data = array (
				
						'vendor_username' => $_REQUEST['vendor_username'],
						'vendor_comname' => $_REQUEST['vendor_comname'],
						//'vendor_pass' => crypt($_REQUEST['vendor_pass']),
						'vendor_pass' => substr(sha1($_REQUEST['vendor_username'].$_REQUEST['vendor_pass']), 0, 30),
						
						'vendor_cemail' => $_REQUEST['vendor_cemail'],
												'vendor_q_id' => $this->session->userdata('tk_q_id'),
						'vendor_flow_id' =>$this->session->userdata('tk_flow_id'),
						'tk_user_create' =>$this->session->userdata('tk_user_create'),
						
						'vendor_jobtype' =>$qflow->flow_type_job,
						'vendor_jobstype' =>$qflow->flow_type_vendor,
						'vendor_st' =>'S1',
						'vendor_img' =>'tmp.jpg',
						
						
						//$qflow
						
						
						);	
						
						
					
						
						
											
							$insertvendor  = $this->Query_Db->insert($this->db->dbprefix.'vendor',$vendor_data );
							
							
								$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username ="'.$_REQUEST['vendor_username'].'"');
							
							
							
								if (!file_exists('vendorfile/'.$getuser->vendor_pass)) {
  mkdir('vendorfile/'.$getuser->vendor_pass, 0777, true);
  $vp1_path = $getuser->vendor_pass;
} else {
	
	$vp1_path = $getuser->vendor_pass.'_dup'.$getuser->vendor_id;
	  mkdir('vendorfile/'.$getuser->vendor_pass.'_dup'.$getuser->vendor_id, 0777, true);

	}
						
							
							
								$vendor_datavp1 = array (
				
						'vp1_ref_id' => $getuser->vendor_id,
						'vp1_cname' => $_REQUEST['vendor_comname'],
						'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_st' =>'S1',
						'vp1_path' =>$vp1_path,
						
						
						);	
						
						$vendor_datavp2 = array (
				
						'vp2_ref_id' => $getuser->vendor_id
						
						
						
						);	
						
						$vendor_datavp3 = array (
				
						'vp3_ref_id' => $getuser->vendor_id
						
						
						
						);	
						
						$insert_vp1  = $this->Query_Db->insert($this->db->dbprefix.'vp1',$vendor_datavp1 );

$insert_vp2  = $this->Query_Db->insert($this->db->dbprefix.'vp2',$vendor_datavp2 );
$insert_vp3  = $this->Query_Db->insert($this->db->dbprefix.'vp3',$vendor_datavp3 );

					
						


							
							
						$update_token  = $this->Query_Db->update2($this->db->dbprefix.'token','tk_token = "'.$this->session->userdata('tk_token').'" ',array('tk_st' => 0));
							
							$resultapi = json_encode(array('create_st' => 'true',
														'vendor_username' => $_REQUEST['vendor_username']
														),true);

							
							
						
							
							
							
							
							
						
					
					}
						
						
						$sessionuserdata = array(
				
				
				'vendor_st' => 'S1',
				'vendor_username' => $_REQUEST['vendor_username']
				
				
				
				);
				
				
				
				
				$this->session->set_userdata($sessionuserdata);
			
			//$this->checkuser($_REQUEST['vendor_username']);
						
			
			
			}
	
	
	print_r($resultapi);
	
	}

public function checkuser($newusername = NULL)
	{
		
		if(!empty($_REQUEST['newusername'])){
			
			
			$check = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$_REQUEST['newusername'].'"');
						
			
			//$username = $_REQUEST['newusername'];
			if( !empty($check)){
				
				$resultapi = json_encode(array('user_st' => 'fail'),true);
				
				}else {
					$resultapi = json_encode(array('user_st' => 'true'),true);
					
					}
			
			
			
			} else {
				
				$resultapi = json_encode(array('user_st' => 'fail'),true);
				
				}
				
				print_r($resultapi);
		
	//	echo 'checkusersustemmmm';
		//$something = $this->input->post('username');
		//$data['title']  = $something;
		//$data['vendorlogin'] = $something;
		//$this->load->view('main/index',$data);	
		//$this->load->view('welcome_message');
	}
	
	public function checklogin()
	{
		
		$something = $this->input->post('username');
		$data['title']  = $something;
		$data['vendorlogin'] = $something;
		$this->load->view('main/index',$data);	
		//$this->load->view('welcome_message');
	}
	
	
	
	
	public function vendor_profile1()
				{
					
					
					
				
					
					if(!empty($_REQUEST)){
						
						
						
	$profile_data = array (
						$_REQUEST['flow_name'] => $_REQUEST['flow_val']
						);	
						
	$update_profile  = $this->Query_Db->update2($this->db->dbprefix.'vp1','vp1_id='.$_REQUEST['flow_id'].'',$profile_data);
							
							
							//print_r($this->db->_error_message());
							$resultapi = json_encode(array('user_st' => 'true'),true);
							echo 'successsss';		
		
						//print_r($this->session->userdata());
						//print_r($_REQUEST);
						
						} else {
							
							$resultapi = json_encode(array('user_st' => 'fail'),true);
							//$resultapi = json_encode(array('user_st' => 'fail'),true);

							}
					
		print_r($resultapi);
		
	}
	
	public function vendor_profile2()
				{
					
					
					
				
					
					if(!empty($_REQUEST)){
						
						
						
	$profile_data = array (
						$_REQUEST['flow_name'] => $_REQUEST['flow_val']
						);	
						
	$update_profile  = $this->Query_Db->update2($this->db->dbprefix.'vp2','vp2_id='.$_REQUEST['flow_id'].'',$profile_data);
							
							
							//print_r($this->db->_error_message());
							$resultapi = json_encode(array('user_st' => 'true'),true);
							echo 'successsss';		
		
						//print_r($this->session->userdata());
						//print_r($_REQUEST);
						
						} else {
							
							$resultapi = json_encode(array('user_st' => 'fail'),true);
							//$resultapi = json_encode(array('user_st' => 'fail'),true);

							}
					
		print_r($resultapi);
		
	}
	
	
	public function vendor_profile3()
				{
					
					
					
				
					
					if(!empty($_REQUEST)){
						
						
						
	$profile_data = array (
						$_REQUEST['flow_name'] => $_REQUEST['flow_val']
						);	
						
	$update_profile  = $this->Query_Db->update2($this->db->dbprefix.'vp3','vp3_id='.$_REQUEST['flow_id'].'',$profile_data);
							
							
							//print_r($this->db->_error_message());
							$resultapi = json_encode(array('user_st' => 'true'),true);
							echo 'successsss';		
		
						//print_r($this->session->userdata());
						//print_r($_REQUEST);
						
						} else {
							
							$resultapi = json_encode(array('user_st' => 'fail'),true);
							//$resultapi = json_encode(array('user_st' => 'fail'),true);

							}
					
		print_r($resultapi);
		
	}
	
	public function uploadifiveshow()
				{
					 $this->load->view('upload/uploadifly', array('error' => ' ' ));
					}
	
	
		public function uploadifive()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }

		
		public function vendorregister(){
		
		
		
			if(empty($this->session->userdata('vendor_st'))){
			
			 redirect('./', 'refresh');
			
			}   else {
				//$data['title']  = 'Register Vendor Information Purchase Online';
				
				
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				
				
				$data['getuser'] = $getuser;
				$data['title']  = 'Register Vendor Information purchase Online';
				
				if($getuser){
					
				
				$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				$vendor_vp2  = $this->Query_Db->record($this->db->dbprefix.'vp2','vp2_ref_id = '.$getuser->vendor_id.'');
				
				
				$vendor_vp2store = $this->Query_Db->result($this->db->dbprefix.'vp2_store','vp2_ref_id =  "'.$getuser->vendor_id.'" and vp2_st = 1');
				
				$vendor_vp2product = $this->Query_Db->result($this->db->dbprefix.'vp2_product','vp2_ref_id =  "'.$getuser->vendor_id.'" and vp2_st = 1');
				
				$vendor_vp2sale = $this->Query_Db->result($this->db->dbprefix.'vp2_sale','vp2_ref_id =  "'.$getuser->vendor_id.'" and vp2_st = 1');


				$vendor_vp3  = $this->Query_Db->record($this->db->dbprefix.'vp3','vp3_ref_id = '.$getuser->vendor_id.'');

				
				$vp1_file_crop1 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				
				
				$vp1_file_crop2 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop2','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				
				$vp1_file_crop3 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop3','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');


				$vp1_file_crop4 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop4','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				$vp1_file_crop5 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop5','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				$vp1_file_crop6 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop6','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				$vp1_file_crop7 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop7','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				$vp1_file_crop8 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop8','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				$vp1_file_crop9 = $this->Query_Db->result($this->db->dbprefix.'vp1_filecrop9','vp1_filecrop_ref_id =  "'.$vendor_vp1->vp1_ref_id.'" and vp1_filecrop_st = 1');
				
									
					} else  {
						$vp1_file_crop1 = NULL;
						$vp1_file_crop2 = NULL;
						$vp1_file_crop3 = NULL;
						$vp1_file_crop4 = NULL;
						$vp1_file_crop5 = NULL;
						$vp1_file_crop6 = NULL;
						$vp1_file_crop7 = NULL;
						$vp1_file_crop8 = NULL;
						$vp1_file_crop9 = NULL;
						$vendor_vp1  = NULL;
						$vendor_vp2  = NULL;
						$vendor_vp2store  = NULL;
						$vendor_vp2product = NULL;
						$vendor_vp2sale = NULL;
						
						
						$vendor_vp3  = NULL;
						}
				
				
				$data['vendor_vp1'] = $vendor_vp1;	
				$data['vendor_vp2'] = $vendor_vp2;
				$data['vendor_vp2store'] = $vendor_vp2store;
				$data['vendor_vp2product'] = $vendor_vp2product;
				$data['vendor_vp2sale'] = $vendor_vp2sale;
				

				

				$data['vendor_vp3'] = $vendor_vp3;
				$data['vp1_file_crop1'] = $vp1_file_crop1;	
				$data['vp1_file_crop2'] = $vp1_file_crop2;	
				$data['vp1_file_crop3'] = $vp1_file_crop3;	
				$data['vp1_file_crop4'] = $vp1_file_crop4;
				$data['vp1_file_crop5'] = $vp1_file_crop5;
				$data['vp1_file_crop6'] = $vp1_file_crop6;
				$data['vp1_file_crop7'] = $vp1_file_crop7;
				$data['vp1_file_crop8'] = $vp1_file_crop8;
				$data['vp1_file_crop9'] = $vp1_file_crop9;
				
				//print_r($data);
				$this->load->view('vendorregister/vendorregister',$data);	
				
				}
			
		
		
		//$data['title']  = 'Register Vendor Information Purchase Online';
		//		$this->load->view('vendorregister/vendorregister',$data);	
		
		}
		
		public function vp1_fileupload1()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$config['encrypt_name'] = TRUE;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload2()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop2',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload3()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop3',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload4()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop4',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload5()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop5',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload6()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop6',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload7()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop7',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload8()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop8',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fileupload9()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
					if($getuser){
						
						
						$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
						
						
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				//$_FILES['Filedata']['name'];
				$new_name = time().$_FILES["Filedata"]['name'];
				$config['file_name'] = $new_name;
               // $config['max_width']            = 1024;
               // $config['max_height']           = 768;

               // $this->load->library('upload', $config);
				$this->upload->initialize( $config);

                if ( ! $this->upload->do_upload('Filedata'))
                {
                        //$error = array('error' => $this->upload->display_errors());

                       // $this->load->view('upload/index', $error);
					   exit;
                }
                else
                {
					
													$vendor_datafile = array (
				
						'vp1_filecrop_ref_id' => $vp1->vp1_id,
						'vp1_filecrop_name' => $new_name,
						//'vp1_email' => $_REQUEST['vendor_cemail'],
						'vp1_filecrop_st' =>'1',
						
						
						
						);	

					
					$vp1_filecrop  = $this->Query_Db->insert($this->db->dbprefix.'vp1_filecrop9',$vendor_datafile );
                        //$data = array('upload_data' => $this->upload->data());

                        //$this->load->view('upload/success', $data);
                }
						
						
						}
					
					
					}
			
               
        }
		
		public function vp1_fdelete1()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }
		
		public function vp1_fdelete2()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop2','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }
		
		
		
		
		
		public function vp1_fdelete3()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop3','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }
		
		public function vp1_fdelete4()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop4','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }
	
	public function vp1_fdelete5()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop5','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }

public function vp1_fdelete6()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop6','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }


public function vp1_fdelete7()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop7','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }


public function vp1_fdelete8()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop8','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }


public function vp1_fdelete9()
        {
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp1_filecrop_st' => '0'
						);	
						

						
						
						$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp1_filecrop9','vp1_filecrop_id='.$_REQUEST['fileid'].'',$profile_data);
					
					
					}
			
               
        }



public function locator(){
		
		if($this->session->userdata('vendor_st') == 'S1'){
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				$data['vendor_vp1'] = $vendor_vp1;

					}else {
						$data['vendor_vp1'] = NULL;

						}
				//$data['title']  = 'New Vendor Register  ';
				
				//$this->load->view('vendorregister/vendorregister',$data);	
				// redirect('./vendorregister/vendorregister', 'refresh',$data);
					
					}else{
						
						redirect('./vendorregister', 'refresh');
						}
		
		
		if(!empty($_GET['id']) && $_GET['mode'] == "editlocator"){
			//echo $_GET['id'];
			$storelocator = $this->Query_Db->record($this->db->dbprefix.'vp2_store','vp2_id =  "'.$_GET['id'].'" and vp2_ref_id ='.$getuser->vendor_id.' and vp2_st = 1');
			
			if($storelocator){
				$data['storelocator'] = $storelocator;
				}else{
					$data['storelocator'] = NULL;
					redirect('./vendorregister', 'refresh');
					}
			$data['title'] = 'Edit Locator';
			$data['id'] = $_GET['id'];
			$this->load->view('vendorregister/vendorlocator',$data);	
			} else if ($_GET['mode'] == "addlocator"){
				//echo 'Invalid Data';
				$data['title'] = 'Create Locator';
				$this->load->view('vendorregister/vendorlocator',$data);
				}
		
		
		}
		
public function deletelocator(){
		
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp2_st' => '0'
						);	
						

						
						$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
						
						if($getuser){
							$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_store','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.'',$profile_data);
							}
						
						
					
					
					}
		
		
	
		
		}
			
public function updatewherehouse(){
		
//print_r($_REQUEST)	;
//print_r($_FILES);	

	if(empty($this->session->userdata('vendor_username'))){
				exit;
				}
				else {
					
					
					
					
				
						
						
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				//$data['getuser'] = $getuser;
				
				if($getuser){
					
				
				$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
				
				
				
				if($vp1){
					
					
					//print_r($_FILES);
					
					if($_FILES["vp2_vmap"]['error'] == 0){
					
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				$new_name = time().$_FILES["vp2_vmap"]['name'];
				$config['file_name'] = $new_name;
				$this->upload->initialize( $config);
				
                if ( ! $this->upload->do_upload('vp2_vmap'))
                {
                    
					   
					   	$error = array('error' => $this->upload->display_errors());
					  $resultapi = json_encode(array('upload_st' => 'fail'),true);

                }
                else
                {
					
					$vendor_datafile = array (
				
						'vp2_ref_id' => $vp1->vp1_id,
						'vp2_vmap' => $new_name,
						'vp2_vstorename' => $_REQUEST['vp2_vstorename'],
						'vp2_vaddress' => $_REQUEST['vp2_vaddress'],
						'vp2_vcontact' => $_REQUEST['vp2_vcontact'],
						'vp2_vtelno' => $_REQUEST['vp2_vtelno'],
						'vp2_vremark' => $_REQUEST['vp2_vremark'],
						'vp2_lat' => $_REQUEST['vp2_lat'],
						'vp2_long' => $_REQUEST['vp2_long'],
						'vp2_st' =>'1',
						
						
						
						);	

					if(!empty($_GET['id'])){
						$vp1_filemap  = $this->Query_Db->update2($this->db->dbprefix.'vp2_store','vp2_id='.$_GET['id'].' and vp2_ref_id ='.$vp1->vp1_id.'',$vendor_datafile);
						
						 $resultapi = json_encode(array('upload_st' => 'true'),true);
						} else {
							
							
							$vp1_filemap  = $this->Query_Db->insert($this->db->dbprefix.'vp2_store',$vendor_datafile );
							
							 $resultapi = json_encode(array('upload_st' => 'true'),true);
							}
						 
                }
				
				print_r($resultapi);
					}
					else{
					
					
						
					$vendor_datafile = array (
				
						'vp2_ref_id' => $vp1->vp1_id,
						'vp2_vstorename' => $_REQUEST['vp2_vstorename'],
						'vp2_vaddress' => $_REQUEST['vp2_vaddress'],
						'vp2_vcontact' => $_REQUEST['vp2_vcontact'],
						'vp2_vtelno' => $_REQUEST['vp2_vtelno'],
						'vp2_vremark' => $_REQUEST['vp2_vremark'],
						'vp2_lat' => $_REQUEST['vp2_lat'],
						'vp2_long' => $_REQUEST['vp2_long'],
						'vp2_st' =>'1',
						
						
						
						);	

					if(!empty($_GET['id'])){
						$vp1_filemap  = $this->Query_Db->update2($this->db->dbprefix.'vp2_store','vp2_id='.$_GET['id'].' and vp2_ref_id ='.$vp1->vp1_id.'',$vendor_datafile);
						
						 $resultapi = json_encode(array('upload_st' => 'true'),true);
						 
						} 
						else {
							
							
							$vp1_filemap  = $this->Query_Db->insert($this->db->dbprefix.'vp2_store',$vendor_datafile );
							
							 $resultapi = json_encode(array('upload_st' => 'true'),true);
							}
						
						
					//	$tresultapi = json_encode(array('upload_sst' => 'fail'),true);
					print_r($resultapi);
						}
					
					}
				
				
				
               
				

				
				
					}
						
					
					}
				
				
				

		
		}

public function vp1_fdeletemap(){
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp2_vmap' => ''
						);	
						
						
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				//$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				if($vendor_vp1){
					unlink('./vendorfile/'.$vendor_vp1->vp1_path.'/'.$_REQUEST['filename']);		
					$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_store','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.'',$profile_data);
					
							}
				
				//$data['vendor_vp1'] = $vendor_vp1;

					}
						
					
					}
			
               
        }
		
public function seller(){
		
		if($this->session->userdata('vendor_st') == 'S1'){
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				$data['vendor_vp1'] = $vendor_vp1;

					}else {
						$data['vendor_vp1'] = NULL;

						}
				//$data['title']  = 'New Vendor Register  ';
				
				//$this->load->view('vendorregister/vendorregister',$data);	
				// redirect('./vendorregister/vendorregister', 'refresh',$data);
					
					}else{
						
						redirect('./vendorregister', 'refresh');
						}
		
		
		if(!empty($_GET['id']) && $_GET['mode'] == "editlocator"){
			//echo $_GET['id'];
			$storelocator = $this->Query_Db->record($this->db->dbprefix.'vp2_sale','vp2_id =  "'.$_GET['id'].'" and vp2_ref_id ='.$getuser->vendor_id.' and vp2_st = 1');
			
			if($storelocator){
				$data['storelocator'] = $storelocator;
				}else{
					$data['storelocator'] = NULL;
					redirect('./vendorregister', 'refresh');
					}
			$data['title'] = 'Edit Seller';
			$data['id'] = $_GET['id'];
			$this->load->view('vendorregister/vendorseller',$data);	
			} else if ($_GET['mode'] == "addlocator"){
				//echo 'Invalid Data';
				$data['title'] = 'Create Seller';
				$this->load->view('vendorregister/vendorseller',$data);
				}
		
		
		}
		
public function sdeletelocator(){
		
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
				//print_r($_REQUEST);	
					
				$profile_data = array (
						'vp2_st' => '0'
						);	
						

						
						$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
						
						if($getuser){
							$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_sale','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.'',$profile_data);
							}
						
						
					
					
					}
		
		
	
		
		}
		
public function supdatewherehouse(){
		
//print_r($_REQUEST)	;
//print_r($_FILES);	

	if(empty($this->session->userdata('vendor_username'))){
				exit;
				}
				else {
					
					
					
					
				
						
						
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				//$data['getuser'] = $getuser;
				
				if($getuser){
					
				
				$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
				
				
				
				if($vp1){
					
					
					//print_r($_FILES);
					
					if($_FILES["vp2_sncard"]['error'] == 0){
					
				$config['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
                $config['allowed_types']        = 'jpg|pdf|jpeg';
                $config['max_size']             = 20480;
				$new_name = time().$_FILES["vp2_sncard"]['name'];
				$config['file_name'] = $new_name;
				$this->upload->initialize( $config);
				
                if ( ! $this->upload->do_upload('vp2_sncard'))
                {
                    
					   
					   	$error = array('error' => $this->upload->display_errors());
					  $resultapi = json_encode(array('upload_st' => 'fail'),true);

                }
                else
                {
					
					$vendor_datafile = array (
				
						'vp2_ref_id' => $vp1->vp1_id,
						'vp2_sncard' => $new_name,
						'vp2_sname' => $_REQUEST['vp2_sname'],
						'vp2_stel' => $_REQUEST['vp2_stel'],
						'vp2_semail' => $_REQUEST['vp2_semail'],
						'vp2_sposition' => $_REQUEST['vp2_sposition'],
						'vp2_slang' => $_REQUEST['vp2_slang'],
						'vp2_remark' => $_REQUEST['vp2_remark'],
						'vp2_st' =>'1',
						
						
						
						);	

					if(!empty($_GET['id'])){
						$vp1_filemap  = $this->Query_Db->update2($this->db->dbprefix.'vp2_sale','vp2_id='.$_GET['id'].' and vp2_ref_id ='.$vp1->vp1_id.'',$vendor_datafile);
						
						 $resultapi = json_encode(array('upload_st' => 'true'),true);
						} else {
							
							
							$vp1_filemap  = $this->Query_Db->insert($this->db->dbprefix.'vp2_sale',$vendor_datafile );
							
							 $resultapi = json_encode(array('upload_st' => 'true'),true);
							}
						 
                }
				
				print_r($resultapi);
					}
					else{
					
					
						
					$vendor_datafile = array (
				
						
						'vp2_ref_id' => $vp1->vp1_id,
						'vp2_sname' => $_REQUEST['vp2_sname'],
						'vp2_stel' => $_REQUEST['vp2_stel'],
						'vp2_semail' => $_REQUEST['vp2_semail'],
						'vp2_sposition' => $_REQUEST['vp2_sposition'],
						'vp2_slang' => $_REQUEST['vp2_slang'],
						'vp2_remark' => $_REQUEST['vp2_remark'],
						'vp2_st' =>'1',
						
						
						
						
						);

					if(!empty($_GET['id'])){
						$vp1_filemap  = $this->Query_Db->update2($this->db->dbprefix.'vp2_sale','vp2_id='.$_GET['id'].' and vp2_ref_id ='.$vp1->vp1_id.'',$vendor_datafile);
						
						 $resultapi = json_encode(array('upload_st' => 'true'),true);
						 
						} 
						else {
							
							
							$vp1_filemap  = $this->Query_Db->insert($this->db->dbprefix.'vp2_sale',$vendor_datafile );
							
							 $resultapi = json_encode(array('upload_st' => 'true'),true);
							}
						
						
					//	$tresultapi = json_encode(array('upload_sst' => 'fail'),true);
					print_r($resultapi);
						}
					
					}
				
				
				
               
				

				
				
					}
						
					
					}
				
				
				

		
		}

public function svp1_fdeletemap(){
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp2_sncard' => ''
						);	
						
						
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				//$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				if($vendor_vp1){
					unlink('./vendorfile/'.$vendor_vp1->vp1_path.'/'.$_REQUEST['filename']);		
					$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_sale','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.' ',$profile_data);
					
							}
				
				//$data['vendor_vp1'] = $vendor_vp1;

					}
						
					
					}
			
               
        }
	
public function vendorproduct(){
		
		if($this->session->userdata('vendor_st') == 'S1'){
					$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				$data['vendor_vp1'] = $vendor_vp1;

					}else {
						$data['vendor_vp1'] = NULL;

						}
				//$data['title']  = 'New Vendor Register  ';
				
				//$this->load->view('vendorregister/vendorregister',$data);	
				// redirect('./vendorregister/vendorregister', 'refresh',$data);
					
					}else{
						
						redirect('./vendorregister', 'refresh');
						}
		
		
		
		
		
		
		$findstore = $this->Query_Db->result($this->db->dbprefix.'vp2_store','vp2_ref_id =  "'.$getuser->vendor_id.'" and vp2_st = 1','','','vp2_id DESC');
		$data['findstore'] = $findstore;
		


		
		if(!empty($_GET['id']) && $_GET['mode'] == "edit"){
			//echo $_GET['id'];
			$storelocator = $this->Query_Db->record($this->db->dbprefix.'vp2_product','vp2_id =  "'.$_GET['id'].'" and vp2_ref_id ='.$getuser->vendor_id.' and vp2_st = 1');
			
						
			
			if($storelocator){
				$data['storelocator'] = $storelocator;
				}else{
					$data['storelocator'] = NULL;
					redirect('./vendorregister', 'refresh');
					}
			$data['title'] = 'Edit Product';
			$data['id'] = $_GET['id'];
			$this->load->view('vendorregister/vendorproduct',$data);	
			} else if ($_GET['mode'] == "add"){
				//echo 'Invalid Data';
				$data['title'] = 'Create Product';
				$this->load->view('vendorregister/vendorproduct',$data);
				}
		
		
		}
		
public function deleteproduct(){
		
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp2_st' => '0'
						);	
						

						
						$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
						
						if($getuser){
							$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_product','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.'',$profile_data);
							}
						
						
					
					
					}
		
		
	
		
		}

public function vp1_fdeletesds(){
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp2_vp_sds' => ''
						);	
						
						
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				//$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				if($vendor_vp1){
					unlink('./vendorfile/'.$vendor_vp1->vp1_path.'/'.$_REQUEST['filename']);		
					$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_product','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.'',$profile_data);
					
							}
				
				//$data['vendor_vp1'] = $vendor_vp1;

					}
						
					
					}
			
               
        }
			
public function vp1_fdeletetds(){
			
			if(empty($this->session->userdata('vendor_username'))){
				exit;
				}else {
					
					
					
					
				$profile_data = array (
						'vp2_vp_tds' => ''
						);	
						
						
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
				//$data['getuser'] = $getuser;
				
				if($getuser){
					$vendor_vp1  = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id = '.$getuser->vendor_id.'');
				
				if($vendor_vp1){
					unlink('./vendorfile/'.$vendor_vp1->vp1_path.'/'.$_REQUEST['filename']);		
					$tagfile  = $this->Query_Db->update2($this->db->dbprefix.'vp2_product','vp2_id='.$_REQUEST['fileid'].' and vp2_ref_id ='.$getuser->vendor_id.'',$profile_data);
					
							}
				
				//$data['vendor_vp1'] = $vendor_vp1;

					}
						
					
					}
			
               
        }
						
public function updateproduct(){
		

	if(empty($this->session->userdata('vendor_username'))){
				exit;
				}
				else {
					
					//print_r($_REQUEST);
				//	exit;
							
				$getuser = $this->Query_Db->record($this->db->dbprefix.'vendor','vendor_username =  "'.$this->session->userdata('vendor_username').'"');
				
								
				if($getuser){
					
				
				$vp1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getuser->vendor_id.'"');
			
				if($vp1){
					
					// upload image
					
					if(empty($_REQUEST['vp2_vp_stbuy'])){
						$vp2_vp_stbuy = 0;
						
						} else {
							$vp2_vp_stbuy = $_REQUEST['vp2_vp_stbuy'];
							}
					
					//$vp2_vp_stbuy = '';
					
							$vendor_datafile = array (
				
						'vp2_ref_id' => $vp1->vp1_id,
						'vp2_vp_owner' => $_REQUEST['vp2_vp_owner'],
						'vp2_vp_name' => $_REQUEST['vp2_vp_name'],
						'vp2_vp_country' => $_REQUEST['vp2_vp_country'],
						'vp2_vp_tax' => $_REQUEST['vp2_vp_tax'],
						'vp2_vp_ltime' => $_REQUEST['vp2_vp_ltime'],
						'vp2_vp_min' => $_REQUEST['vp2_vp_min'],
						'vp2_vp_pksize' => $_REQUEST['vp2_vp_pksize'],
						'vp2_vp_info' => $_REQUEST['vp2_vp_info'],
						'vp2_vp_store' => $_REQUEST['vp2_vp_store'],
						'vp2_vp_stbuy' => $vp2_vp_stbuy,
						'vp2_vp_remark' => $_REQUEST['vp2_vp_remark'],
						'vp2_st' =>'1',
						
						
						
						);	
		
					
						if(!empty($_FILES["vp2_vp_sds"])){
											
						$config1['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
						$config1['allowed_types']        = 'jpg|pdf|jpeg';
						$config1['max_size']             = 20480;
						$new_name1 = time().$_FILES["vp2_vp_sds"]['name'];
						$config1['file_name'] = $new_name1;
						$vendor_datafile['vp2_vp_sds'] = $new_name1; 
						$this->upload->initialize( $config1);
						
							 if ( ! $this->upload->do_upload('vp2_vp_sds') )
                {
                    
					   
					  $error = array('error' => $this->upload->display_errors());
					  $resultapi = json_encode(array('upload_st' => 'fail'),true);
					  print_r($resultapi);
					  exit;
					  

                } 

						}
						
						 if(!empty($_FILES["vp2_vp_tds"])){
						
						$config2['upload_path']          = './vendorfile/'.$vp1->vp1_path.'/';
						$config2['allowed_types']        = 'jpg|pdf|jpeg';
						$config2['max_size']             = 20480;
						$new_name2 = time().$_FILES["vp2_vp_tds"]['name'];
						$config2['file_name'] = $new_name2;
						$vendor_datafile['vp2_vp_tds'] = $new_name2; 
						//$vendor_datafile = array ('vp2_vp_tds' => $new_name2);	

						$this->upload->initialize( $config2);
						
							 if ( !$this->upload->do_upload('vp2_vp_tds'))
                {
                    
					   
					  $error = array('error' => $this->upload->display_errors());
					  $resultapi = json_encode(array('upload_st' => 'fail'),true);
					  print_r($resultapi);
					  exit;
					  	

                }
						
						}
						
						
						
						
						
						
						
						
     					 if(!empty($_GET['id'])){
							 
						$vp1_filemap  = $this->Query_Db->update2($this->db->dbprefix.'vp2_product','vp2_id='.$_GET['id'].' and vp2_ref_id ='.$vp1->vp1_id.'',$vendor_datafile);
						
						$resultapi = json_encode(array('upload_st' => 'true'),true);
						//print_r($vendor_datafile);
						
						 
						 
						} 
						else {
							
							
							$vp1_filemap  = $this->Query_Db->insert($this->db->dbprefix.'vp2_product',$vendor_datafile );
							
								
						$resultapi = json_encode(array('upload_st' => 'true'),true);
						//print_r($vendor_datafile);
						
						 
						 
							}
							
							print_r($resultapi);

					
		
					
					}
				
			
				
               
				

				
				
					}
						
					
					}
				
				
				

		
		}
		
			
	
public function vendor_upload()
				{
			
					
			print_r($_FILES);
			print_r($_REQUEST);
		  		$config['upload_path']          = './vendorfile/75107b3ddffd5675d3878968fa2b7d/';
                $config['allowed_types']        = 'gif|jpg|png|pdf';
                $config['max_size']             = 10240;
              //  $config['max_width']            = 1024;
              //  $config['max_height']           = 768;


				 $this->upload->initialize( $config);
               // $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('vp1_filecrop_name'))
                {
                      
					    $error = array('error' => $this->upload->display_errors());
						//print_r($error);
                      //  $this->load->view('upload/index', $error);
					  $resultapi = json_encode(array('upload_st' => 'fail'),true);
                }
                else
                {
                         $data = array('upload_data' => $this->upload->data());
						//print_r($data);
						
						$resultapi = json_encode(array('upload_st' => 'true'),true);
                      //  $this->load->view('upload/success', $data);
                }
				
				print_r($resultapi);
		
	}
	
	
	
	
}
