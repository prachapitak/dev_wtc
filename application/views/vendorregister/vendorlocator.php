<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php echo $title;?></title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
<?php $this->load->view('main/allcss');?>
<?php $this->load->view('main/alljs3');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>uploadify/uploadifive.css">
</head>

<body >
<?php


  $thislang = $this->lang->load('vendorregister',$this->session->userdata('lang'));
  
  
?>
<?php $this->load->view('mainvendor/navbar');?>

<!-- Page container -->
<div class="page-container"> 
  
  <!-- Page content -->
  <div class="page-content">
    <?php $this->load->view('mainvendor/navigation');?>
    
    <!-- Main content -->
    <div class="content-wrapper"> 
      
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4>
            </h4>
          </div>
          <div class="heading-elements">
            <div class="heading-btn-group"> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-cube4 text-primary "></i><span>Product</span></a> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-file-text text-primary"></i> <span>Doccument</span></a> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-users4 text-primary"></i> <span>Contact</span></a> </div>
          </div>
        </div>
      </div>
      <!-- /page header -->
      
      
      <?php //$this->load->view('dashboard/toa/noti');?>
      
      <!-- Content area -->
      <div class="content"> 
        
        <!-- Input group buttons -->
        <div class="panel panel-flat">
          <div class="panel-body">
            <form class="form-validation form-validate-jquery" id="regis_st1" action="#"  method="post" enctype="multipart/form-data" >
              <h6 class="form-wizard-title text-semibold"> <span class="form-wizard-count">!</span> Add and Edit Warehouse <small class="display-block"> <?php echo $this->lang->line('s_topic_1', TRUE);?></small> </h6>
              
                            

            
            <?php if(!empty($storelocator)){
				
				?>
                
                
                 <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label>Warehouse Name : <span class="text-danger">*</span></label>
                <input type="text" placeholder="ระบุชื่อสถานที่เก็บสินค้า" name="vp2_vstorename" id="vp2_vstorename" value="<?php echo $storelocator->vp2_vstorename;?>" class="form-control required">
                
               
                            
                            
                
                
              </div>
              <div class="col-sm-4">
                <label>Contact Name : <span class="text-danger">*</span> </label>
                <input type="text" placeholder="ระบุชื่อผู้ติดต่อ" name="vp2_vcontact" id="vp2_vcontact" value="<?php echo $storelocator->vp2_vcontact;?>" class="form-control required">
                
             
                
                
                
              </div>
                <div class="col-sm-4">
                <label>Tel No. : <span class="text-danger">*</span></label>
                <input type="text" placeholder="ระบุเบอร์ติดต่อ" name="vp2_vtelno" id="vp2_vtelno" value="<?php echo $storelocator->vp2_vtelno;?>" class="form-control required">
                
                
                 
                
              </div>
            </div>
          </div>
          
                      
          <div class="form-group">
            <div class="row">
            
            <div class="col-sm-4">
            
                <label>Warehouse Address : <span class="text-danger">*</span></label>
                
                
                <input type="text" placeholder="ระบุข้อมูลที่อยู่ที่จัดเก็บสินค้า" name="vp2_vaddress" id="vp2_vaddress" value="<?php echo $storelocator->vp2_vaddress;?>" class="form-control required" >
                
               
                
                
                
                
                </div>
              <div class="col-sm-4">
                <label>Latitude</label>
                <input type="text" placeholder="ระบุข้อมูล Latitude" name="vp2_lat" id="vp2_lat" value="<?php echo $storelocator->vp2_lat;?>" class="form-control">
                
                
                
                </div>
                <div class="col-sm-4">
                <label>Longitude</label>
                <input type="text" placeholder="ระบุข้อมูล Longitude" name="vp2_long" id="vp2_long" value="<?php echo $storelocator->vp2_long;?>" class="form-control">
                </div>
              
            </div>
          </div>
          
          
          <div class="form-group">
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
            <label>Remark : </label>
            <textarea  rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับสถานที่เก็บสินค้าของท่านเท่านั้น" name="vp2_vremark" id="vp2_vremark" class="form-control"><?php echo $storelocator->vp2_vremark;?></textarea>
            
            
            
            
            
          </div>
              </div>
              
              
              
                            
                            
                            
              
              <div class="col-sm-6"  id="vp_upmap" <?php if(!empty($storelocator->vp2_vmap)){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                <label>Map : </label> <br>
                <input type="file" name="vp2_vmap" id="vp2_vmap" class="file-styled" >
                 <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                
                 <div class="col-sm-6" id="vp_stshow_map"   <?php if(!empty($storelocator->vp2_vmap)){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>Map : </label>
                              <a  data-toggle="modal" href="#vp_viewmap"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> /
                            
                             <a href="#nogo" onclick="vp_deletemap('<?php echo  $storelocator->vp2_id;?>','<?php echo  $storelocator->vp2_vmap;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                             
                           
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewmap" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $storelocator->vp2_vmap;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            

                            
                            </div>
                
            </div>
          </div>
            
            
            
           
										  
    
                
                <?php
				
				}else{
					
					?>
                      <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label>Warehouse Name : <span class="text-danger">*</span></label>
                <input type="text" placeholder="ระบุชื่อสถานที่เก็บสินค้า" name="vp2_vstorename" id="vp2_vstorename" value="" class="form-control required">
                
               
                            
                            
                
                
              </div>
              <div class="col-sm-4">
                <label>Contact Name : <span class="text-danger">*</span> </label>
                <input type="text" placeholder="ระบุชื่อผู้ติดต่อ" name="vp2_vcontact" id="vp2_vcontact" value="" class="form-control required">
                
             
                
                
                
              </div>
                <div class="col-sm-4">
                <label>Tel No. : <span class="text-danger">*</span></label>
                <input type="text" placeholder="ระบุเบอร์ติดต่อ" name="vp2_vtelno" id="vp2_vtelno" value="" class="form-control required">
                
                
                 
                
              </div>
            </div>
          </div>
          
            
          <div class="form-group">
            <div class="row">
            
            <div class="col-sm-4">
            
                <label>Warehouse Address : <span class="text-danger">*</span></label>
                
                
                <input type="text" placeholder="ระบุข้อมูลที่อยู่ที่จัดเก็บสินค้า" name="vp2_vaddress" id="vp2_vaddress" value="" class="form-control required" >
                
               
                
                
                
                
                </div>
              <div class="col-sm-4">
                <label>Latitude</label>
                <input type="text" placeholder="ระบุข้อมูล Latitude" name="vp2_lat" id="vp2_lat" value="" class="form-control">
                
                
                
                </div>
                <div class="col-sm-4">
                <label>Longitude</label>
                <input type="text" placeholder="ระบุข้อมูล Longitude" name="vp2_long" id="vp2_long" value="" class="form-control">
                </div>
              
            </div>
          </div>
          
          
          <div class="form-group">
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
            <label>Remark : </label>
            <textarea  rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับสถานที่เก็บสินค้าของท่านเท่านั้น" name="vp2_vremark" id="vp2_vremark" class="form-control"></textarea>
            
            
            
            
            
          </div>
              </div>
              
              
              
                            
                            
                            
              
              <div class="col-sm-6"  id="vp_upmap" >
                <label>Map : </label> <br>
                <input type="file" name="vp2_vmap" id="vp2_vmap" class="file-styled" >
                 <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                
                 
                
            </div>
          </div>
                    
                    <?php
					
					}
				
				
				?>
            
           
           
     <div class="text-right">
     
     <div class="btn-group">
									<a href="<?php echo base_url(); ?>vendorregister/vendorregister#section2"  class="btn btn-success" ><i class="icon-arrow-left13 position-right"></i> Cancle </a>
									<?php if(empty($storelocator)){
				
				?>
                <button type="reset" class="btn btn-danger">Reset
                                     </i>
                                     </button>
                <?php } 
				?>		
									 <button type="submit" class="btn btn-primary">Save
                                     <i class="icon-arrow-right14 position-right"></i>
                                     </button>								</div>
									 
                                     
                                             
								</div>
            
             
            </form>
          </div>
        </div>
        <!-- /input group buttons --> 
        
        <!-- Primary modal -->
        
        <!-- /primary modal -->
        
        <?php $this->load->view('main/footer');?>
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container --> 

<!-- Vertical form modal -->

<!-- /vertical form modal --> 
<script type="text/javascript">

function vp_deletemap(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงไฟล์ดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vp1_fdeletemap",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}






    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



 var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
			
			/*vp1_tel: {
                number: true
            },*/
            numbers: {
                number: true
            },
			
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
           // text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
				
		
		 var formData = new FormData($("#regis_st1")[0]);
		  $.ajax({ 
					 url:'<?php echo base_url(); ?>vendorregister/updatewherehouse/?<?php if(!empty($storelocator)){echo "id=$storelocator->vp2_id";}?>',    
                     //url:"<?php //echo base_url(); ?>main/ajax_upload",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data: formData,  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
					// alert (data);
			 var resultAjax = JSON.parse(data);
			 
			 if(resultAjax.upload_st == 'true'){
				 

  

				 
				  $.blockUI({ 
           
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='<?php echo base_url(); ?>vendorregister/vendorregister#section2';
  }  , 1000 );
				
				
				}
            }
        });
				 
				 } else {
					 
					 //alert('fail');
					 swal({
            title: "Oop.. File type  incorrect...",
            text: "ขออภัยประเภทของเอกสารไม่ถูกต้อง | Sorry This file type incorrect.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					 }
				//console.log(data);
					 
		
						 
                     }  
                });  	
					
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		
		
            }
    });
	

 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


   

	


	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });





		


</script>

</body>
</html>