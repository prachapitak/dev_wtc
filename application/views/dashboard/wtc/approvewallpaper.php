<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head> 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
     <?php $this->load->view('main/alljs3');?>
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_extension_fixed_columns.js"></script>
    
    
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/daterangepicker.js"></script>


    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
	
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>

	
 
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>

	
</head>


<body class="sidebar-xs has-detached-left">


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">Dashboard  </span> -  Approve Wallpaper  
						    <?php //print_r($getwallpaper);?> <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php //$this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php //$this->load->view('dashboard/toa/noti');?>
					
				
				
                                    

				
                
                
                 <!-- Content area -->
				<div class="content">

					<!-- Basic view -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"> Approve Wallpaper </h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                	
			                	</ul>
		                	</div>
						</div>
						
						<div class="panel-body">
                        
                       
                        <div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>วันที่ ประกาศ</th>
										<th>เจ้าของ</th>
										<th>รูป</th>
                                        <th>Action</th>
									</tr>
                                    
								</thead>
								<tbody>
                                
                                 <?php 
						if($getwallpaper){
							
							foreach($getwallpaper as $getwallpapers){
								?>
                                
                                <tr>
										<td><?php echo $getwallpapers->id;?></td>
										<td><?php echo $getwallpapers->bookdate;?></td>
										<td><?php echo $getwallpapers->contact;?></td>
										<td> <a href="<?php echo base_url(); ?>assets/wallpaper/<?php echo $getwallpapers->imgname;?>" target="_blank"><img src="<?php echo base_url(); ?>assets/wallpaper/<?php echo $getwallpapers->imgname;?>" alt="" class="img-responsive" style="max-width:100px;"></a>	</td>
                                        <td>  <a href="<?php echo base_url(); ?>dashboard/approvenow/<?php echo $getwallpapers->id;?>"  class="btn btn-primary">Approve</a> | <a href="<?php echo base_url(); ?>dashboard/rejectnow/<?php echo $getwallpapers->id;?>"  class="btn btn-primary">Reject</a>  </td>
									</tr>
                                <?php
								
								}
							
							
							} 
						
						?>
                        
									
								
								</tbody>
							</table>
						</div>
                        
                        </div>
					</div>
					<!-- /basic view -->
                    
                    
                    	<!-- Pickadate picker -->
					
					<!-- /pickadate picker -->


					<!-- Agenda view -->
					
					<!-- agenda view -->


					<!-- Footer -->
					<!--<div class="footer text-muted">
						&copy; 2018. <a href="#">TOA</a> by AOF
					</div>-->
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
    <!-- Vertical form modal -->
					<div id="modal_form_vertical" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มสินค้า</h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ผลิต</label>
													<input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สินค้าที่จำหน่าย</label>
													<input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ประเทศที่ผลิต</label>
													<input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
													<input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<label>Load Time</label>
													<input type="text" placeholder="ระบุ Load Time" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Minimum Order</label>
													<input type="text" placeholder="ระบุ Minimum Order" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Packing Size </label>
													<input type="text" placeholder="ระบุ Packing Size" class="form-control">
												</div>
											</div>
										</div>

										
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
													<input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
													<span class="help-block">eg: k.หนูแดง T 023456789</span>
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>MSDS(ไทย/อังกฤษ) </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>เงือนไขในการชำระเงิน</label>
													<input type="text" placeholder="ระบุเงือนไขในการชำระเงิน" class="form-control">
													<span class="help-block">eg: 30 วัน , 90 วัน </span>
												</div>

												<div class="col-sm-6">
                                                
                                              
                                              
                                              <label>สถานะซื้อขายกับ TOA </label> 
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
              					                 
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>

                                                
												
													
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติมที่เกี่ยวกับตัวสินค้า : </label>
		                                    <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
		                                </div>
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


  <!-- Vertical form modal -->
					<div id="modal_form_acc" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มบัญชี / เช็ค </h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อบัญชี</label>
													<input type="text" placeholder="ระบุชื่อบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เลขที่บัญชี</label>
													<input type="text" placeholder="ระบุเลขที่บัญชี" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อธนาคาร</label>
													<input type="text" placeholder="ระบุชื่อธนาคาร" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สาขา</label>
													<input type="text" placeholder="ระบุสาขา " class="form-control">
												</div>
											</div>
										</div>

										
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ดูแลบัญชี</label>
													<input type="text" placeholder="ระบุชื่อผู้ดูแลบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เบอร์โทรติดต่อ</label>
													<input type="text" placeholder="ระบุเบอร์โทรติดต่อ " class="form-control">
												</div>
											</div>
										</div>
                                        								
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
                                                
                                                <label>หมายเหตุ / รายละเอียดเพิ่มเติม : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
                                                
													
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>สำเนาบัญชีธนาคาร  </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->

<?php

function convertdatetojs($datebook){
	
	$jsyear = date("Y",strtotime ($datebook));
		$jsmoth = date("n",strtotime ($datebook));
		$jsmothnew = $jsmoth-1;
		$jsdate = date("j",strtotime ($datebook));
		
		return $newdate = $jsyear.','.$jsmothnew.','.$jsdate;
	}

// or 

//$date = new DateTime('2008-09-22');
//echo $date->getTimestamp();

?>
 
<script type="text/javascript">
 // Date limits
    

$(function() {
	$('.pickadate-limits').pickadate({
		//formatSubmit: 'yyyy-mm-dd',
		format: 'yyyy-mm-dd',
		 disable: [
   <?php
   if($getwallpaper ){
		foreach($getwallpaper as $getwallpapers){ 
		?>
		new Date(<?php echo convertdatetojs($getwallpapers->bookdate);?>),
		<?php
		}
		
   }
		
   ?>
    
  ],
		min: new Date(<?php echo convertdatetojs(date("Y-n-j",strtotime ( '+1 days' ))); ?>),
		max: new Date(<?php echo convertdatetojs(date("Y-n-j",strtotime ( '+30 days' ))); ?>),
		//today : false,
		closeOnSelect: true,
	
 
    });
	
	});


 var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
			
			/*vp1_tel: {
                number: true
            },*/
            numbers: {
                number: true
            },
			
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
           // text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                
				
				
		
		 var formData = new FormData($("#regis_st1")[0]);
		  $.ajax({ 
					 url:'<?php echo base_url(); ?>dashboard/editwallpaperupload/',    
                   
                     method:"POST",  
                     data: formData,  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
			//alert (data);
			//console.log(data);
			 var resultAjax = JSON.parse(data);
			 
			 if(resultAjax.upload_st == 'true'){
				 
				 swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				 
				  $.blockUI({ 
           
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='<?php echo base_url(); ?>dashboard/';
  }  , 1000 );
				
				
				}
            }
        });
				 
				 } else {
					 
					 //alert('fail');
					 swal({
            title: "Oop.. File type  incorrect...",
            text: "ขออภัยประเภทของเอกสารไม่ถูกต้อง | Sorry This file type incorrect.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					 }
				//console.log(data);
					 
		
						 
                     }  
                });  	
					
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		
		
            }
    });

</script>


</body>




</html>