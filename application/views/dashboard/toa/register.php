<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?>

    
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>

    
   <!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/prism.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/extension_blockui.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->

</head>


<body class="login login-cover" style=" background:url('<?php echo base_url(); ?>assets/images/login_cover.jpg');    background-repeat: round;">

 <?php $this->load->view('main/allheader');?>
<?php //echo $this->session->userdata('lang');
//print_r($title);
//print_r($response);
?>

<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
        
				

		<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content pb-20 " style="padding-top:100px;">
                
                  <div style="color:#F00; font-size:16px;" id="displayerror" > </div> 
               
                

					<!-- Form with validation -->
					<form action="#" id="form-agreememt" class="form-validate col-lg-8 col-lg-offset-2">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="border-slate-300 text-slate-300"><img src="<?php echo base_url(); ?>assets/images/logo_dark.png" alt="" style="max-height:45px;"></div>
								
							</div>
                            
                            	<div class="content" style="padding-bottom:20px; padding-top:20px;">
				<div class="alert alert-success alert-styled-left">
										
										<span class="text-semibold">ยินดีด้วย คุณ <?php echo ucfirst($this->session->userdata('username'));?> คุณเข้าสู่ระบบสำเร็จ !!</span> เพิ่งใช้งานเป็นครั้งแรกใช่มั๊ย ? ไม่ต้องกังวล ทำตามที่เราแนะนำ ง่ายนิดเดียว มาเริ่มกันเลย !!!  
                                       
                                        			    </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="panel panel-flat border-left-info border-right-info">
								<div class="panel-heading">
									<h6 class="panel-title"> <strong>ข้อมูลส่วนตัวของท่าน </strong> </h6>
								</div>
								
								<div class="panel-body">
                                
                               <fieldset class="content-group">
								



									<div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>ระบุชื่อ - นามสกุล ไทย </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="fullname_th" id="fullname_th" class="form-control" placeholder="ระบุ ชื่อ - นามสกุล " onchange="noti2(this.name,this.value)" required="required" value="" >
                                            
                                             <span class="help-block label  text-info-600">*ชื่อนี้จะปรากฎใน Role และ Permission ต่างๆ ที่ท่านได้รับจากระบบ</span>
										</div>
									</div>
                                    
                                    <div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>Name  - Surname </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="fullname_en" id="fullname_en" class="form-control" placeholder="Name - Surname " onchange="noti2(this.name,this.value)" required="required" value="">
                                            
                                             <span class="help-block label  text-info-600">*This Name Display in Your Role and Permission</span>
										</div>
									</div>
                                    
                                    <div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>IP Phone </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="ipphone" id="ipphone" class="form-control" placeholder="ระบุหมายเลขโทรศัพท์ภายใน ของท่าน" onchange="noti2(this.name,this.value)"  required="required" value="">
                                            
                                              <span class="help-block label  text-info-600">*ช่วยให้การประสานภายในระบบสะดวกยิ่งขึ้น เมื่อมีส่วนงานต้องการติดต่อท่าน</span>
										</div>
									</div>
                               
                              	 <div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>Mobile Phone </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="mobile" id="mobile" class="form-control" placeholder="ระบุเบอร์มือถือของท่าน" onchange="noti2(this.name,this.value)"  required="required" value="">
                                            
                                             <span class="help-block label  text-info-600">*จะแสดงผลเมื่อมีการร้องขอการติดต่อที่สำคัญจากระบบของเรา</span>
                                            
                                            
										</div>
									</div>
                                    
                                    <div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>Employee ID </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="empid" id="empid" class="form-control" placeholder="ระบุรหัสพนักงานของท่าน เช่น TP170101 " onchange="noti2(this.name,this.value)"  required="required"  value="">
                                            
                                             <span class="help-block label  text-info-600">*รหัสพนักงานมีส่วนทำให้ เราบริการท่านได้อย่างมีประสิทธิภาพ รวมไปถึงการระบุตัวตนที่ชัดเจนขึ้น</span>
										</div>
									</div>
                              
                              </fieldset>
									  
								</div>
							</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    <?php //echo $this->session->userdata('role');
									
									//print_r($this->session->userdata('username'));
									$newrole = explode(",",$this->session->userdata('role'));
									
									
									if($newrole){
										foreach($newrole as $newroles => $val){
											
											if($val != NULL){
												$this->session->set_userdata(array('role_'.$val => $val));
												}
											
											
											}
										
										
				
										
										
										}
									//$this->session->unset_userdata('role_Admin');
									//$this->session->unset_userdata('role_AVP');
									//$this->session->unset_userdata('role_MPR');
									//$this->session->unset_userdata('role_PR');
									//$this->session->unset_userdata('role_ACC');
									
									//print_r($this->session->userdata());
									if(!empty($this->session->userdata('role_Admin'))){
										
										?>
                                        
                                        <div class="panel panel-flat border-left-info border-right-info">
								<div class="panel-heading">
									<h6 class="panel-title">- คุณได้รับ Permission เป็น <strong>Administrator</strong> ของระบบ</h6>
								</div>
								
								<div class="panel-body">
                                
                                ยังคิดไม่ออกเอาไว้ก่อน
									  
								</div>
							</div>
                                        
                                        
                                        <?php
										
										}
										
										
										if(!empty($this->session->userdata('role_AVP'))){
										
										?>
                                        
                                       
                                       <div class="panel panel-flat border-left-info border-right-info">
								<div class="panel-heading">
									<h6 class="panel-title">- ท่านได้รับ Role เป็น <strong>Assistant Vice President (AVP)</strong> ของระบบ โปรดเลือก ประเภทของหน่วยงานจัดซื้อที่ท่านจะทำการ อนุมัติ (โดยปกติท่านจะเลือกทั้งคู่)</h6>
								</div>
								
								<div class="panel-body">
									  <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                        
                                        <div class="col-lg-6">
                                        <div class="checkbox">
												<label>
                                                
                                                <input type="checkbox" name="GP" id="GP" class="styled" onchange="noti(this.name,this.value,'GP')" value="<?php echo $this->session->userdata('role_AVP')?>">
                                                
													กลุ่มงานจัดซื้อทั่วไป (General Purchase)
												</label>
											</div>
                                            </div>
										
										<div class="col-lg-6">
											
										
											<div class="checkbox">
												<label>
													<input type="checkbox" name="RMPK" id="RMPK" class="styled" onchange="noti(this.name,this.value,'RMPK')" value="<?php echo $this->session->userdata('role_AVP')?>">
													กลุ่มงานจัดซื้อ วัตถุดิบ และ Packaging (PK &amp; RM)
												</label>
											</div>
										
                                        	
                                        
									
										</div>
                                        
                                      
                                        </div>
								</div>
							</div>
                                       
                                           
                                      
                                        <?php
										
										
										
										
										
										}
										
										if(!empty($this->session->userdata('role_MPR'))){
										
										?>
                                        
                                        <div class="panel panel-flat border-left-info border-right-info">
								<div class="panel-heading">
									<h6 class="panel-title">- ท่านได้รับ Role เป็น <strong>Purchasing manager (MPR)</strong> ของระบบ โปรดเลือก ประเภทของหน่วยงานจัดซื้อที่ท่านจะทำการ อนุมัติ (โดยปกติท่านจะเลือกอย่างใดอย่างนึง)</h6>
								</div>
								
								<div class="panel-body">
                                
                                
                                   
                                    
                                     
                                
                                
									  <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                       <div class="col-lg-6">
                                       <div class="checkbox">
											<label>
											
												กลุ่มงานจัดซื้อทั่วไป (General Purchase)
											</label>
										</div>
                                        
                                         <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                        
                                      
										
										  <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
														  <input type="checkbox" name="MCM" id="MCM" class="styled" onchange="noti(this.name,this.value,'CM')" value="<?php echo $this->session->userdata('role_MPR')?>">
													Commercial 
												</label>
											</div>
                                            </div>
                                              <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													 <input type="checkbox" name="MOP" id="MOP" class="styled" onchange="noti(this.name,this.value,'OP')" value="<?php echo $this->session->userdata('role_MPR')?>">
													Operation
												</label>
											</div>
                                            </div>
                                        
                                      
                                        </div>
                                        
                                        </div>
                                        
                                         <div class="col-lg-6">

										<div class="checkbox">
											<label>
												
												กลุ่มงานจัดซื้อ วัตถุดิบ และ Packaging (PK &amp; RM)
											</label>
										</div>
                                        
                                        <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                        
                                      
										
										  <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="MRM" id="MRM" class="styled" onchange="noti(this.name,this.value,'RM')" value="<?php echo $this->session->userdata('role_MPR')?>">
													Raw Material 
												</label>
											</div>
                                            </div>
                                              <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="MPK" id="MPK" class="styled" onchange="noti(this.name,this.value,'PK')" value="<?php echo $this->session->userdata('role_MPR')?>">
													Packaging
												</label>
											</div>
                                            </div>
                                        
                                      
                                        </div>
                                        
                                        </div>
                                        
                                      
                                        
                                      
                                        </div>
                                        
                                        
                                        
								</div>
							</div>
                                        
                                       
                                        <?php
										
										}
										
										if(!empty($this->session->userdata('role_PR'))){
										
										?>
                                        
                                        <div class="panel panel-flat border-left-info border-right-info">
								<div class="panel-heading">
									<h6 class="panel-title">- ท่านได้รับ Role เป็น <strong>Purchasing  (PR)</strong> ของระบบ โปรดเลือก ประเภทของหน่วยงานจัดซื้อที่ท่านจะทำการ ตรวจสอบ (โดยปกติท่านจะเลือกอย่างใดอย่างนึง)</h6>
								</div>
								
								<div class="panel-body">
                                
                                
                                   
                                    
                                     
                                
                                
									  <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                       <div class="col-lg-6">
                                       <div class="radio">
											<label>
												
												กลุ่มงานจัดซื้อทั่วไป (General Purchase)
											</label>
										</div>
                                        
                                         <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                        
                                      
										
										  <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="PCM" id="PCM" class="styled" onchange="noti(this.name,this.value,'CM')" value="<?php echo $this->session->userdata('role_PR')?>">
													Commercial 
												</label>
											</div>
                                            </div>
                                              <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="POP" id="POP" class="styled" onchange="noti(this.name,this.value,'OP')" value="<?php echo $this->session->userdata('role_PR')?>">
													Operation
												</label>
											</div>
                                            </div>
                                        
                                      
                                        </div>
                                        
                                        </div>
                                        
                                         <div class="col-lg-6">

										<div class="radio">
											<label>
												
												กลุ่มงานจัดซื้อ วัตถุดิบ และ Packaging (PK &amp; RM)
											</label>
										</div>
                                        
                                        <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                        
                                      
										
										  <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="PRM" id="PRM" class="styled" onchange="noti(this.name,this.value,'RM')" value="<?php echo $this->session->userdata('role_PR')?>">
													Raw Material 
												</label>
											</div>
                                            </div>
                                              <div class="col-lg-12">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="PPK" id="PPK" class="styled" onchange="noti(this.name,this.value,'PK')" value="<?php echo $this->session->userdata('role_PR')?>">
													Packaging
												</label>
											</div>
                                            </div>
                                        
                                      
                                        </div>
                                        
                                        </div>
                                        
                                      
                                        
                                      
                                        </div>
                                        
                                        
                                        
								</div>
							</div>
                                        
                                       
                                        <?php
										
										
										
										
										
										}
										
										if(!empty($this->session->userdata('role_ACC'))){
										
										?>
                                        
                                        <div class="panel panel-flat border-left-info border-right-info">
								<div class="panel-heading">
									<h6 class="panel-title">- ท่านได้รับ Role เป็น <strong>Accounting  (ACC)</strong> ของระบบ โปรดเลือก ประเภทของหน่วยงานจัดซื้อที่ท่านจะทำการ ตรวจสอบ (โดยปกติท่านจะเลือกทั้งคู่)</h6>
								</div>
								
								<div class="panel-body">
									  <div class="form-group" style="padding-left:50px; padding-right:50px;">
                                        
                                        <div class="col-lg-6">
                                        <div class="checkbox">
												<label>
													<input type="checkbox" name="AGP" id="AGP" class="styled" onchange="noti(this.name,this.value,'GP')" value="<?php echo $this->session->userdata('role_ACC')?>">
													กลุ่มงานจัดซื้อทั่วไป (General Purchase)
												</label>
											</div>
                                            </div>
										
										<div class="col-lg-6">
											
										
											<div class="checkbox">
												<label>
														<input type="checkbox" name="ARMPK" id="ARMPK" class="styled" onchange="noti(this.name,this.value,'RMPK')" value="<?php echo $this->session->userdata('role_ACC')?>">
													กลุ่มงานจัดซื้อ วัตถุดิบ และ Packaging (PK &amp; RM)
												</label>
											</div>
										
                                        	
                                        
									
										</div>
                                        
                                      
                                        </div>
								</div>
							</div>
                                        
                                        <?php
										
										
										
										
										
										}
										
										
										
										
									//print_r($newrole);
									if($newrole){
										foreach($newrole as $newroles){
											
											if($newroles != NULL){
												
												//echo $newroles.'<br/>';
												}
											
											}
										}
									
									//$str = "Hello world. It's a beautiful day.";

									
									?>

                            
                          

							
<hr/>

							<div class="form-group login-options">
								<div class="row">
									



									<div class="col-sm-12 text-right">
										<label class="checkbox-inline">
                                        
                                        <input type="checkbox" name="agreement" id="agreement" class="styled">
										
											ยืนยันข้อมูลถูกต้อง
										</label>
									</div>
								</div>
							</div>

							<div class="form-group login-options">
								<div class="row">
									



									<div class="col-sm-12 text-right">
                                  
                                    
                                      <button type="submit" class="btn btn-primary legitRipple">บันทึกและดำเนินการต่อ <i class="icon-arrow-right14 position-right"></i></button>
										
									</div>
								</div>
							</div>
                            
							

							
             
							
						</div>
					</form>
					<!-- /form with validation -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->


              


	</div>
	<!-- /page container -->
    
	



	
        <?php
    if(ENVIRONMENT == 'development'){
		
		$this->load->view('main/pagerender');
		
		}
		
		//print_r($this->session->userdata());
	?>

    
    
    
 <script type="text/javascript">
  
   function noti(name,val,permission) {
	
	//alert(permission);
	var newname =  '#'+name;
	
	 if(document.getElementById(name).checked) {
        //Do stuff
		//alert (val);
		var newval = '1';
		
    } else { 
	
	var newval = '0';
	
	//alert (val);
	}
	
		$.ajax({
					
			url: "<?php echo base_url(); ?>dashboard/toa_updaterole",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					parameter	: 	permission,
					role	: 	val,
					value	: 	newval,
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				//alert (data);
				new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
				 
				
			
				
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	

	
	
    //alert("The input value has changed. The new value is: " + val);
} 

function noti2(name,val) {
	
	
	
	$.ajax({
						
			url: "<?php echo base_url(); ?>dashboard/toa_editprofile",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: "<?php echo $this->session->userdata('username');?>",
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			//	alert (data);
				new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
			
				
			}
		});	
		

	
	
    //alert("The input value has changed. The new value is: " + val);
} 

 

    
$(function() {
	


  
	// Style checkboxes and radios
	$('.styled').uniform();
	

    
	
	$("#form-agreememt").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
			
            label.addClass("validation-valid-label").text("Successfully")
        },
        rules: {
            password: {
                minlength: 5
            }
        },
        messages: {
            username: "Enter your username",
            password: {
            	required: "Enter your password",
            	minlength: jQuery.validator.format("At least {0} characters required")
            }
        },
		submitHandler: function (form) {
	
	
	
	$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | เรากำลังตรวจสอบความถูกต้อง </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
				
				 if(document.getElementById('agreement').checked) {
        
		
			$.ajax({
					
			url: "<?php echo base_url(); ?>dashboard/toa_updatest",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					status	: 	"T02",
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			}
		});	
		
		
				$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				
				
				

				
				 setTimeout( function(){ 
				 
				 
			
	window.location='./toa/';
  }  , 1000 );
				
				
				}
            }
        });
		
		
		
		
    } else { 
	
	
	
	$.blockUI({ 
            message: '<i class="icon-shield-notice " style="font-size: 60px;color: red;"></i> <p style="font-size: 17px;"> ข้อมูลของคุณยังไม่ถูกยืนยันความถูกต้อง โปรดยืนยันข้อมูลถูกต้อง  </p>',
           timeout: 3000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
				 
				 
				
				
	//window.location='../toa/';
  }  , 1000 );
				
				
				}
				
            }
        });
	
	
	}
				
			 
			 		
		
			   
			   
            } 
        });
	
			
			
	
			
			
		
		
					
			
            }
		

		
		
    });
	
	
	
	


});

    
    </script>   
    
    
</body>


</html>