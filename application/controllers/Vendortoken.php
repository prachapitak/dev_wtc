<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Home extends CI_Controller {
	class Vendortoken extends CI_Controller {
	
	
	public function __construct() {
		
		parent::__construct();
		$this->load->model('Query_Db');
		$this->load->model('Lang_Model');
		$this->load->model('Lang_Menu');
		$this->load->library('email');  // เรียกใช้ email class
		
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		if(!empty($_REQUEST['token'])){
			
			$checktoken = $this->Query_Db->record($this->db->dbprefix.'token','tk_st = 1 and tk_token =  "'.$_REQUEST['token'].'"');
			if($checktoken){
				
				$result = time() - strtotime($checktoken->tk_date);
				$checktoken->timeresult =  $result;
				if( $result > 86400 ||  $result < 0 ){
					
					$checktoken->token_st =  'fail';
					
					}else {
						$checktoken->token_st =  'pass';
						
						$sessionuserdata = array(
				
				'tk_token' => $checktoken->tk_token,
				'tk_company' => $checktoken->tk_company,
				'tk_email' => $checktoken->tk_email,
				'tk_flow_id' => $checktoken->tk_flow_id,
				'tk_q_id' => $checktoken->tk_q_id,
				'tk_user_create' => $checktoken->tk_user_create,
				
				
				);
				
				
				
				
				$this->session->set_userdata($sessionuserdata);
						
						
						
						
						}
				
				
				
				$resultapi = json_encode($checktoken, true);
				
				}else {
					//$checktoken->token_st =  'fail';
					$resultapi = json_encode(array('token_st' => 'misstoken'),true);
					
					}
					
					
					
					print_r($resultapi);
			
			
				
							
			//print_r($_REQUEST);
			} else{
				
				$data['title']  = 'Register Vendor Information purchase Online';
				$this->load->view('vendorregister/vendortoken',$data);	
				
				}
		
		
		
				
		
		
	
	}
	
	

	
	public function checklogin()
	{
		
		$something = $this->input->post('username');
		$data['title']  = $something;
		$data['vendorlogin'] = $something;
		$this->load->view('main/index',$data);	
		//$this->load->view('welcome_message');
	}
}
