<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs2');?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>

	


	
</head>


<body class="sidebar-xs has-detached-left">


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">Dashboard  </span> - ดาวโหลด Template  <?php //print_r($getwallpaper);?> <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php //$this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php //$this->load->view('dashboard/toa/noti');?>
					
				
				
                                    

				
                
                
                 <!-- Content area -->
				<div class="content">

					<!-- Basic view -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">ดาวโหลด  Template Wallpaper </h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                	
			                	</ul>
		                	</div>
						</div>
						
						<div class="panel-body">
						
                            
                            
                            <div class="alert alert-danger no-border">
										
										<span class="text-semibold">ประกาศ </span> โปรดปฎิบัติตาม  Template อย่างเคร่งครัด  ระบบเป็นเพียงเครื่องมือช่วยอำนวยความสะดวกในการดำเนินการเท่านั้น
								    </div>
                                    
                                    
                                    
                                    <p><strong>ตัวอย่างที่ 1</strong> 	: Click ขวา ที่รูปเพื่อบันทึกเป็น Template</p>
                                    <img src="<?php echo base_url(); ?>assets/template/template01.jpg"  class="img-responsive" alt="">
                                    
                                    <hr>
                                    
                                     <p><strong>ตัวอย่างที่ 2 	</strong>: Click ขวา ที่รูปเพื่อบันทึกเป็น Template</p>
                                    <img src="<?php echo base_url(); ?>assets/template/template02.jpg"  class="img-responsive" alt="">

							
						</div>
					</div>
					<!-- /basic view -->


					<!-- Agenda view -->
					
					<!-- agenda view -->


					<!-- Footer -->
					<!--<div class="footer text-muted">
						&copy; 2018. <a href="#">TOA</a> by AOF
					</div>-->
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
    <!-- Vertical form modal -->
					<div id="modal_form_vertical" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มสินค้า</h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ผลิต</label>
													<input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สินค้าที่จำหน่าย</label>
													<input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ประเทศที่ผลิต</label>
													<input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
													<input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<label>Load Time</label>
													<input type="text" placeholder="ระบุ Load Time" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Minimum Order</label>
													<input type="text" placeholder="ระบุ Minimum Order" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Packing Size </label>
													<input type="text" placeholder="ระบุ Packing Size" class="form-control">
												</div>
											</div>
										</div>

										
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
													<input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
													<span class="help-block">eg: k.หนูแดง T 023456789</span>
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>MSDS(ไทย/อังกฤษ) </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>เงือนไขในการชำระเงิน</label>
													<input type="text" placeholder="ระบุเงือนไขในการชำระเงิน" class="form-control">
													<span class="help-block">eg: 30 วัน , 90 วัน </span>
												</div>

												<div class="col-sm-6">
                                                
                                              
                                              
                                              <label>สถานะซื้อขายกับ TOA </label> 
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
              					                 
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>

                                                
												
													
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติมที่เกี่ยวกับตัวสินค้า : </label>
		                                    <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
		                                </div>
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


  <!-- Vertical form modal -->
					<div id="modal_form_acc" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มบัญชี / เช็ค </h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อบัญชี</label>
													<input type="text" placeholder="ระบุชื่อบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เลขที่บัญชี</label>
													<input type="text" placeholder="ระบุเลขที่บัญชี" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อธนาคาร</label>
													<input type="text" placeholder="ระบุชื่อธนาคาร" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สาขา</label>
													<input type="text" placeholder="ระบุสาขา " class="form-control">
												</div>
											</div>
										</div>

										
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ดูแลบัญชี</label>
													<input type="text" placeholder="ระบุชื่อผู้ดูแลบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เบอร์โทรติดต่อ</label>
													<input type="text" placeholder="ระบุเบอร์โทรติดต่อ " class="form-control">
												</div>
											</div>
										</div>
                                        								
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
                                                
                                                <label>หมายเหตุ / รายละเอียดเพิ่มเติม : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
                                                
													
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>สำเนาบัญชีธนาคาร  </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


 
<script type="text/javascript">

$(function() {


    // Add events
    // ------------------------------

    // Default events
	
	
    var events = [
	
	<?php
	if($getwallpaper ){
		foreach($getwallpaper as $getwallpapers){
			?>
		 {
            title: '<?php echo $getwallpapers->title;?> | <?php echo $getwallpapers->contact;?> | <?php echo $getwallpapers->tel;?>',
			 url: '<?php echo base_url(); ?>dashboard/wallpaperdetail/<?php echo $getwallpapers->id;?>',
            start: '<?php echo $getwallpapers->bookdate;?>'
        },
		<?php
			
			}
		
		
		}
	?>
        {
            title: 'All Day Event',
            start: '2018-11-01'
        },
        {
            title: 'Long Event',
            start: '2014-11-07',
            end: '2014-11-10'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-09T16:00:00'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-16T16:00:00'
        },
        {
            title: 'Conference',
            start: '2014-11-11',
            end: '2014-11-13'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T10:30:00',
            end: '2014-11-12T12:30:00'
        },
        {
            title: 'Lunch',
            start: '2014-11-12T12:00:00'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T14:30:00'
        },
        {
            title: 'Happy Hour',
            start: '2014-11-12T17:30:00'
        },
        {
            title: 'Dinner',
            start: '2014-11-12T20:00:00'
        },
        {
            title: 'Birthday Party',
            start: '2014-11-13T07:00:00'
        },
        {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: '2014-11-28'
        }
    ];


    // Event colors
    var eventColors = [
        {
            title: 'All Day Event',
            start: '2014-11-01',
            color: '#EF5350'
        },
        {
            title: 'Long Event',
            start: '2014-11-07',
            end: '2014-11-10',
            color: '#26A69A'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-09T16:00:00',
            color: '#26A69A'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-16T16:00:00',
            color: '#5C6BC0'
        },
        {
            title: 'Conference',
            start: '2014-11-11',
            end: '2014-11-13',
            color: '#546E7A'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T10:30:00',
            end: '2014-11-12T12:30:00',
            color: '#546E7A'
        },
        {
            title: 'Lunch',
            start: '2014-11-12T12:00:00',
            color: '#546E7A'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T14:30:00',
            color: '#546E7A'
        },
        {
            title: 'Happy Hour',
            start: '2014-11-12T17:30:00',
            color: '#546E7A'
        },
        {
            title: 'Dinner',
            start: '2014-11-12T20:00:00',
            color: '#546E7A'
        },
        {
            title: 'Birthday Party',
            start: '2014-11-13T07:00:00',
            color: '#546E7A'
        },
        {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: '2014-11-28',
            color: '#FF7043'
        }
    ];


    // Event background colors
    var eventBackgroundColors = [
        {
            title: 'All Day Event',
            start: '2014-11-01'
        },
        {
            title: 'Long Event',
            start: '2014-11-07',
            end: '2014-11-10',
            color: '#DCEDC8',
            rendering: 'background'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-06T16:00:00'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-16T16:00:00'
        },
        {
            title: 'Conference',
            start: '2014-11-11',
            end: '2014-11-13'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T10:30:00',
            end: '2014-11-12T12:30:00'
        },
        {
            title: 'Lunch',
            start: '2014-11-12T12:00:00'
        },
        {
            title: 'Happy Hour',
            start: '2014-11-12T17:30:00'
        },
        {
            title: 'Dinner',
            start: '2014-11-24T20:00:00'
        },
        {
            title: 'Meeting',
            start: '2014-11-03T10:00:00'
        },
        {
            title: 'Birthday Party',
            start: '2014-11-13T07:00:00'
        },
        {
            title: 'Vacation',
            start: '2014-11-27',
            end: '2014-11-30',
            color: '#FFCCBC',
            rendering: 'background'
        }
    ];



    // Initialization
    // ------------------------------

    // Basic view
    $('.fullcalendar-basic').fullCalendar({
        header: {
          //  left: 'prev,next today',
            center: 'title',
           // right: 'month,basicWeek,basicDay'
        },
       // defaultDate: '2014-11-12',
        editable: true,
        events: events
    });




});
   

</script>


</body>




</html>