<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs');?>

    
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>

    
   <!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/prism.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/extension_blockui.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->

</head>


<body class="login login-cover">

 <?php $this->load->view('main/allheader');?>
<?php //echo $this->session->userdata('lang');
//print_r($this->session->userdata());
//print_r($response);
?>

<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
        
				

			<!-- Main content -->
			<div class="content-wrapper">
				
                
                
                
				<!-- Content area -->
				<div class="content pb-20 " style="padding-top:100px;">
                
                
                
                <div style="color:#F00; font-size:16px;" id="displayerror" > </div> 


<!-- Form with validation -->

					<form id="form-validate"  class="form-validate col-lg-8 col-lg-offset-2" enctype="multipart/form-data">
						<div class="panel panel-body login-form" >
							<div class="text-center">
								<div class="border-slate-300 text-slate-300"><img src="<?php echo base_url(); ?>assets/images/logo_dark.png" alt="" style="max-height:45px;"></div>
								<h5 class="content-group">เงื่อนไขและการใช้บริการระบบของ บริษัท ทีโอเอ กรุ๊ป จำกัด(มหาชน) <br> Terms and conditions of use of the system of TOA Group Public Company Limited. </h5>
							</div>
                            
                            
                            <div  style="max-height: 400px;
    overflow: auto;">
    
    
    					

                            
                            
                            <h6 class="text-semibold">1 Start your development with no hassle!</h6>

							<p class="content-group">Common problem of templates is that all code is deeply integrated into the core. This limits your freedom in decreasing amount of code, i.e. it becomes pretty difficult to remove unnecessary code from the project. Limitless allows you to remove unnecessary and extra code easily just by removing the path to specific LESS file with component styling. All plugins and their options are also in separate files. Use only components you actually need!</p>



   <h6 class="text-semibold">2 Start your development with no hassle!</h6>

							<p class="content-group">Common problem of templates is that all code is deeply integrated into the core. This limits your freedom in decreasing amount of code, i.e. it becomes pretty difficult to remove unnecessary code from the project. Limitless allows you to remove unnecessary and extra code easily just by removing the path to specific LESS file with component styling. All plugins and their options are also in separate files. Use only components you actually need!</p>
							
                            
                               <h6 class="text-semibold">3 Start your development with no hassle!</h6>

							<p class="content-group">Common problem of templates is that all code is deeply integrated into the core. This limits your freedom in decreasing amount of code, i.e. it becomes pretty difficult to remove unnecessary code from the project. Limitless allows you to remove unnecessary and extra code easily just by removing the path to specific LESS file with component styling. All plugins and their options are also in separate files. Use only components you actually need!</p>
                            
                            
                             <h6 class="text-semibold">4 Start your development with no hassle!</h6>

							<p class="content-group">Common problem of templates is that all code is deeply integrated into the core. This limits your freedom in decreasing amount of code, i.e. it becomes pretty difficult to remove unnecessary code from the project. Limitless allows you to remove unnecessary and extra code easily just by removing the path to specific LESS file with component styling. All plugins and their options are also in separate files. Use only components you actually need!</p>
                            
                             <h6 class="text-semibold">5 Start your development with no hassle!</h6>

							<p class="content-group">Common problem of templates is that all code is deeply integrated into the core. This limits your freedom in decreasing amount of code, i.e. it becomes pretty difficult to remove unnecessary code from the project. Limitless allows you to remove unnecessary and extra code easily just by removing the path to specific LESS file with component styling. All plugins and their options are also in separate files. Use only components you actually need!</p>



    
                            
                            </div>
                               
<hr/>

							<div class="form-group login-options">
								<div class="row">
									



									<div class="col-lg-12 text-right">
										<label class="checkbox-inline">
											<input type="checkbox" name="agreement" id="agreement" class="styled" required="required">
											ข้าพเจ้าได้อ่านและยอมรับเงื่อนไขของบริษัท / I have read and accept the terms of the company.
										</label>
									</div>
								</div>
							</div>

							<div class="form-group login-options">
								<div class="row">
									



									<div class="col-sm-12 text-right">
                                  
                                   
                                    
                                     <a href="<?php echo base_url(); ?>" class="btn btn-danger legitRipple" > ไม่ยอมรับ / Decline</a>
                                   
                                    
                                     or 
                                     
                                   
                                    
                                      <button type="submit" id="btt_en" class="btn btn-primary legitRipple">ยอมรับข้อตกลง / Accepted <i class="icon-arrow-right14 position-right"></i></button>
										
									</div>
								</div>
							</div>
                            
							

							
             
							
						</div>
					</form>
					<!-- /form with validation -->
					
                    
						
					
					

				</div>
				<!-- /content area -->
                
                
                
  
    

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->


              


	</div>
	<!-- /page container -->
    
	


	
        <?php
    if(ENVIRONMENT == 'development'){
		
		$this->load->view('main/pagerender');
		
		}
	?>

    
    
    
 <script type="text/javascript">
    
    
    
$(function() {
	



	// Style checkboxes and radios
	$('.styled').uniform();
	

    // Setup validation
    $("#form-validate").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
			
            label.addClass("validation-valid-label").text("Successfully")
        },
        rules: {
            password: {
                minlength: 5
            }
        },
        messages: {
            username: "Enter your username",
            password: {
            	required: "Enter your password",
            	minlength: jQuery.validator.format("At least {0} characters required")
            }
        },
		submitHandler: function (form) {
	
			
			
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 1000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px;color: green;"></i> <p style="font-size: 17px;"> Accepted Successful  </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
				 
//				 alert(resultAjax.ad_group + resultAjax.message);
 				window.location='./vendorcreateacc/';
  }  , 1000 );
				
				
				}
            }
        });
		
			   
			   
            } 
        });
			
			
		
					
					
			
		
            }
		

		
		
    });
	

	
	$("#form-createacc").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
			
            label.addClass("validation-valid-label").text("Successfully")
        },
        rules: {
            password: {
                minlength: 5
            }
        },
        messages: {
            username: "Enter your username",
            password: {
            	required: "Enter your password",
            	minlength: jQuery.validator.format("At least {0} characters required")
            }
        },
		submitHandler: function (form) {
	
			
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		
			
			
			
			
			
		
			
			
				
		
				
			
			
					
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
		

		
		
    });
	
	


});

    
    </script>   
    
    
</body>


</html>