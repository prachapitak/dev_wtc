<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs2');?>



</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">Dashboard  </span> - หน้าหลัก WTC  <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php //$this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php $this->load->view('dashboard/toa/noti');?>
					
				
				
                                    

				<!-- Content area -->
				<div class="content">
                
               <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">รายการคู่ค้าที่รอการตรวจสอบข้อมูลจาก ท่าน</h5>
							
						</div>

						<div class="panel-body">
                        
                
                          <div class="alert alert-primary no-border">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">คำแนะนำ!</span> รู้หรือไม่ท่านสามารถ ท่านสามารถ กดปุ่ม 
							 <code>Reject</code> เพื่อยกเลิกคู่ค้าออกจาก ระบบ หรือกดปุ่ม <code>ReEdit</code> เพื่อให้ระบบทำการแจ้งไปที่ Vendor ให้เข้ามาทำการแก้ไขข้อมูลอีกครั้ง และ เข้า สู่ขั้นตอนการ Approve อีกครั้ง 
								    </div>
                             
						</div>

						<table class="table datatable-basic">
							<thead>
								<tr>
                                <th>Company</th>
												<th >Created</th>
												<th >Email</th>
												<th >Tel.</th>
												<th >Status</th>
												<th class="text-center">Actions</th>
                                
									
								</tr>
							</thead>
							<tbody>
                            
                            <?php if($getvendorapprove){
								foreach($getvendorapprove as $getvendorapproves){
									
									$getvendorprofile1 = $this->Query_Db->record($this->db->dbprefix.'vp1','vp1_ref_id =  "'.$getvendorapproves->vendor_id.'"');
									$getvendorprofile3 = $this->Query_Db->record($this->db->dbprefix.'vp3','vp3_ref_id =  "'.$getvendorapproves->vendor_id.'"');
									
									?>
                                    <tr>
												<td>
													<div class="media-left media-middle">
														<a href="#"><img src="<?php echo base_url(); ?>assets/images/tmp.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="<?php echo base_url(); ?>/dashboard/approvevendordetail?vid=<?php echo $getvendorapproves->vendor_id;?>" class="text-default text-semibold"><?php echo $getvendorapproves->vendor_comname;?></a></div>
														<div class="text-muted text-size-small">
															<span class="status-mark border-blue position-left"></span>
															บันทึกข้อมูลเมื่อ : <?php echo $getvendorapproves->vendor_create;?>  
														</div>
													</div>
												</td>
												<td><span class="text-muted"> <?php echo $getvendorprofile3->vp3_upby;?></span></td>
												<td><span class="text-success-600"><i class="icon-mail5 position-left"></i> <?php echo $getvendorapproves->vendor_cemail;?> </span></td>
												<td><h6 class="text-semibold"><?php echo $getvendorprofile1->vp1_tel;?> ,<?php echo $getvendorprofile1->vp1_etel;?> </h6></td>
												<td><?php if($getvendorapproves->vendor_st == "S2"){ echo '<span class="label bg-warning">รอการตรวจสอบ</span>'; }else echo '';?></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="<?php echo base_url(); ?>/dashboard/approvevendordetail?vid=<?php echo $getvendorapproves->vendor_id;?>"><i class="icon-file-stats"></i>  ดูข้อมูลคู่ค่า</a></li>
																<li><a href="#"><i class="icon-file-text2"></i> Export to .CSV</a></li>
																<li><a href="#"><i class="icon-file-locked"></i> Export to .PDF</a></li>
																 
															</ul>
														</li>
													</ul>
												</td>
											</tr>
                                    <?php
									
									
									}
								
								}
							
							
							?>
                            
                            
                            
										
											
								
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->

               
						
                        
                          <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">รายการคู่ค้าที่อยู่ระหว่างการ อนุมัติ</h5>
							
						</div>

						<div class="panel-body">
                        
                
                          <table class="table datatable-fixed-left" width="100%">
							<thead>
						        <tr>
                                 				
                                                <th >Company</th>
                                                <th >ประเภท Vendor</th>
												
												<th >ผู้ตรวจสอบฝ่ายจัดซื้อ</th>
                                             
												<th >หัวหน้าฝ่ายจัดซื้อ</th>
                                                <th >ผู้ตรวจสอบฝ่ายบัญชี</th>
                                                <th > AVP อนุมัติ</th>
                                                 
											 
						            <th class="text-center">Actions</th>
						        </tr>
						    </thead>
						    <tbody>
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-18 15:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-success text-success-300">Completed</span>   </div>
														<div class="text-muted text-size-small">
															
															ราคาสินค้าถูกกว่าเจ้าอื่นถึง 20%  <br/>และยังสามารถทดแทนสินค้าเดิมได้..<br/>
                                                            2017-06-18 16:10:04
														</div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-success text-success-300">Review</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                        ข้อมูลการเงินปกติค่ะตรวจสอบแล้ว <br/>แต่เอกสาร Book Bank ยังไม่ชัดเท่าไหร่	 <br/> 2017-06-18 16:10:04
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                            
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-17 13:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-warning text-warning-300">In Progress</span>   </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                      
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-danger text-danger-300">Not Started</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                                
                                   <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-18 15:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-success text-success-300">Completed</span>   </div>
														<div class="text-muted text-size-small">
															
															ราคาสินค้าถูกกว่าเจ้าอื่นถึง 20%  <br/>และยังสามารถทดแทนสินค้าเดิมได้..<br/>
                                                            2017-06-18 16:10:04
														</div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-success text-success-300">Review</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                        ข้อมูลการเงินปกติค่ะตรวจสอบแล้ว <br/>แต่เอกสาร Book Bank ยังไม่ชัดเท่าไหร่	 <br/> 2017-06-18 16:10:04
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                            
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-17 13:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-warning text-warning-300">In Progress</span>   </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                      
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-danger text-danger-300">Not Started</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                                
                             
                             
                                <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-18 15:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-success text-success-300">Completed</span>   </div>
														<div class="text-muted text-size-small">
															
															ราคาสินค้าถูกกว่าเจ้าอื่นถึง 20%  <br/>และยังสามารถทดแทนสินค้าเดิมได้..<br/>
                                                            2017-06-18 16:10:04
														</div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-success text-success-300">Review</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                        ข้อมูลการเงินปกติค่ะตรวจสอบแล้ว <br/>แต่เอกสาร Book Bank ยังไม่ชัดเท่าไหร่	 <br/> 2017-06-18 16:10:04
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                            
                            
                            <tr>
						            <td>
                                                
                                                
                                                
													<div class="media-left media-middle">
														<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt=""></a>
													</div>
													<div class="media-left">
														<div class=""><a href="prapprovedetailview.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      ผลตรวจ :<span class="label label-flat  text-success-600"><i class="icon-checkmark4"></i></span>: ผ่าน | คะแนนที่ได้ : <span class="label label-flat  text-success-800" style="font-size:14px;">9</span>/10
                                                        
                                                        <br/>2017-06-18 15:12:04 
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="">Raw Material</div>
														
													</div>
												</td>
												<td><div class="media-left">
														<div class=""> Chonlakorn Bunjua <span class="label label-flat border-success text-success-300">Completed</span> </div>
														<div class="text-muted text-size-small">
															
															ข้อมูลวัตถุดิบตรงตามที่ทางฝ่ายผลิตต้องการ <br/>รวมไปถึงติดต่อ จนท มีการตอบกลับรวดเร็ว..<br/>
                                                            2017-06-17 13:12:04
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> Somchai Baimontar <span class="label label-flat border-warning text-warning-300">In Progress</span>   </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                <div class="media-left">
														<div class="">Puntila Theeraraktrakul  <span class="label label-flat border-warning text-warning-300">In Progress</span></div>
                                                        
														<div class="text-muted text-size-small">
														
                                                      
														 
														</div>
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">Napa Sitawarin  <span class="label label-flat border-danger text-danger-300">Not Started</span></div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <a href="prapprovedetailview.php" class="btn btn-default legitRipple"><i class="icon-eye position-left"></i> View Detail</a>
                                    
													
												</td>
						        </tr>
                                
                             
                                
                             
                            
                            
                              
                            </tbody>
                            
						</table>
                             
						</div>

						
                        
                        
                        
                        
                        
					</div>
					<!-- /basic datatable -->




                    
                    
			
                    

					
                    
		        

		         



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
    <!-- Vertical form modal -->
					<div id="modal_form_vertical" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มสินค้า</h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ผลิต</label>
													<input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สินค้าที่จำหน่าย</label>
													<input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ประเทศที่ผลิต</label>
													<input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
													<input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<label>Load Time</label>
													<input type="text" placeholder="ระบุ Load Time" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Minimum Order</label>
													<input type="text" placeholder="ระบุ Minimum Order" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Packing Size </label>
													<input type="text" placeholder="ระบุ Packing Size" class="form-control">
												</div>
											</div>
										</div>

										
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
													<input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
													<span class="help-block">eg: k.หนูแดง T 023456789</span>
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>MSDS(ไทย/อังกฤษ) </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>เงือนไขในการชำระเงิน</label>
													<input type="text" placeholder="ระบุเงือนไขในการชำระเงิน" class="form-control">
													<span class="help-block">eg: 30 วัน , 90 วัน </span>
												</div>

												<div class="col-sm-6">
                                                
                                              
                                              
                                              <label>สถานะซื้อขายกับ TOA </label> 
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
              					                 
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>

                                                
												
													
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติมที่เกี่ยวกับตัวสินค้า : </label>
		                                    <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
		                                </div>
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


  <!-- Vertical form modal -->
					<div id="modal_form_acc" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มบัญชี / เช็ค </h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อบัญชี</label>
													<input type="text" placeholder="ระบุชื่อบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เลขที่บัญชี</label>
													<input type="text" placeholder="ระบุเลขที่บัญชี" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อธนาคาร</label>
													<input type="text" placeholder="ระบุชื่อธนาคาร" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สาขา</label>
													<input type="text" placeholder="ระบุสาขา " class="form-control">
												</div>
											</div>
										</div>

										
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ดูแลบัญชี</label>
													<input type="text" placeholder="ระบุชื่อผู้ดูแลบัญชี" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>เบอร์โทรติดต่อ</label>
													<input type="text" placeholder="ระบุเบอร์โทรติดต่อ " class="form-control">
												</div>
											</div>
										</div>
                                        								
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
                                                
                                                <label>หมายเหตุ / รายละเอียดเพิ่มเติม : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
                                                
													
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>สำเนาบัญชีธนาคาร  </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


 
<script type="text/javascript">

// Custom tag class

    $('.tokenfield').tokenfield();
    $('.tagsinput-custom-tag-class').tagsinput({
        tagClass: function(item){
            return 'label bg-success';
        }
    });
	
	    // Display values
    $('.tags-input, [data-role="tagsinput"], .tagsinput-max-tags, .tagsinput-custom-tag-class').on('change', function(event) {
        var $element = $(event.target),
            $container = $element.parent().parent('.content-group');

        if (!$element.data('tagsinput'))
        return;

        var val = $element.val();
        if (val === null)
        val = "null";
    
        $('pre.val > code', $container).html( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
        $('pre.items > code', $container).html(JSON.stringify($element.tagsinput('items')));
        Prism.highlightAll();
    }).trigger('change');




function noti(val) {
	
	 new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
	
    //alert("The input value has changed. The new value is: " + val);
}

	 $('#pnotify-successx').on('click', function () {
        new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
    });

 // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();




    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });


	

    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



 var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 1
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
					
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister2.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		/*	
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		*/
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
    });

 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


	
   // With validation
   
	
 $(window).on('load',function(){
	 
	 
	 /*swal({
		 
		  title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบระหว่างนี้ท่านจะไม่สามารถแก้ไข ข้อมูลใดๆ ได้ จนกว่าทางเจ้าหน้าที่ของ TOA GROUP ตรวจสอบข้อมูลเสร็จ",
            confirmButtonColor: "#66BB6A",
            type: "success",
			confirmButtonText: "ตกลง",
		 
			
			   title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบ",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        });*/
	 
	// $('#modal_theme_primary').modal('show');
	 //validation-next
	/* $('#validation-next').click(function(){
		 $('#modal_theme_primary').modal('show');
		 });*/
       // $('#modal_theme_primary').modal('show');
	  
	 /* bootbox.dialog({
                title: "กรุณาระบุรหัสผ่าน",
                message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                        '<form class="form-horizontal">' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">Name</label>' +
                                '<div class="col-md-8">' +
                                    '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                                    '<span class="help-block">Here goes your name</span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">How awesome is this?</label>' +
                                '<div class="col-md-8">' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked">' +
                                            'Really awesomeness' +
                                        '</label>' +
                                    '</div>' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                                            'Super awesome' +
                                        '</label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '</div>',
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            bootbox.alert("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        ); */
    });
	
	 
	/*
	 $('#validation-next').on('click', function() {
        
		  swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
    });*/
	
	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });

 // Custom bootbox dialog with form
   

</script>


</body>




</html>