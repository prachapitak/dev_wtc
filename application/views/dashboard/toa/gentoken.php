<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?> 
         
   

    
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_extension_fixed_columns.js"></script>
    
    <!-- /theme JS files -->
    
    <!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jasny_bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/autosize.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/formatter.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/passy.js"></script>
    
    
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/maxlength.min.js"></script>

	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/form_controls_extended.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->
	
	
    
   
    



</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">- Generate Token  </span> - สร้างรหัสผ่านชั่วคราวสำหรับ Vendor ใหม่  <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php $this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php  $this->load->view('dashboard/toa/noti');?>
					
				
		
                                    		
                                    

				<!-- Content area -->
				<div class="content">
                
                <!-- Quick stats boxes -->
							<div class="row">
								
							</div>
						
                    
			
            <div class="panel panel-flat">
                        <div class="panel-heading">
                        
									
                                   
                                    	
									
								</div>
                                
                        
								  
						
                                

								<div class="panel-body">
                                 <div class="col-md-12">
                                 
                                 
                                 <div class="alert alert-info alert-styled-left">
										
										<span class="text-semibold">โปรดทราบ !</span> หาก คู่ค้าของ ท่านได้ลงเบียนในขั้นตอนแรกเรียบร้อยแล้ว ข้อมูลใน ตารางจะหายไป และนำไปจัดเก็บเข้าระบบ ในเมนู Approve Vendor
                                       
                                        			    </div>
                                    
                                
                                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal_form_acc">Generate New Token <i class="icon-plus3 position-right"></i></button>
                                 <hr>
                                 </div>
                               
                               <div class="col-md-12">
                               
                               
                               
                               <table class="table datatable-basic">
							<thead>
								<tr>
                                	<th class="col-md-4 text-center" >ชื่อบริษัท</th>
                                    <th class="col-md-4 text-center">ผู้ออกเอกสาร</th>
                                 	<th >Flow</th>
                                    <th >เกณฑ์ ประเมิน</th>
                                   
                                    <th >Status</th>
                                    
                                    
                                    <th class="text-center">Actions</th>
                                   
                                
								</tr>
							</thead>
							<tbody>
                            
                            <?php
                            if($showflowtoken){
								foreach($showflowtoken as $showflowtokens){
									?>
                                    <tr>
												<td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="text-default text-semibold"><?php echo $showflowtokens->tk_company;?></div>
														<div class="text-muted text-size-small ">
                                                         
                                                      Email :<span class="label  text-success-600"><?php echo $showflowtokens->tk_email;?></span> <br> Token Expire :<span class="label  text-danger-600"><?php echo $showflowtokens->tk_date;?></span>
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                               
												<td>
                                                	<div class="media-left">
														<div class="text-default text-semibold"><?php echo $showflowtokens->tk_user_create;?></div>
														<div class="text-muted text-size-small">
                                                         
                                                      Email :<span class="label  text-success-600">sale@newvendor.com</span> <br> Date :<span class="label  text-danger-600"><?php echo $showflowtokens->tk_date;?></span>
															
														</div>
													</div>
                                                
                                                
                                                </td>
                                                    
                                                    
                                                 <td><div class="media-left">
														<div class="text-muted"> <span class="label label-flat border-success text-success-300"><?php echo $showflowtokens->tk_flow_id;?> กลุ่มงานจัดซื้อทั่วไป (Commercial) </span> </div>
														
													</div></td>
                                               <td><div class="media-left">
														<div class="text-muted"> <span class="label label-flat border-success text-success-300"><?php echo $showflowtokens->tk_q_id;?>เกณฑ์ของงานจัดซื้อทั่วไป</span> </div>
														
													</div></td>
                                            
                                             
												
												
                                               
                                                <td><span class="label bg-info">กำลังลงทะเบียน</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																
																<!--<li><a href="#"><i class="icon-pencil7"></i> Edit / แก้ไข</a></li>-->
																<li class="disabled"><a href="#"><i class="icon-bin"></i> Delete / ลบข้อมูล</a></li>
																 
															</ul>
														</li>
													</ul>
												</td>
											</tr>
                                    <?php
									
									}
								
								}
							?>
                            
                            
                                           
                             <tr>
												<td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class=""><a href="mprapprovedetail.php" class="text-default text-semibold">บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) </a></div>
														<div class="text-muted text-size-small">
                                                         
                                                      Email :<span class="label  text-success-600">sale@newvendor.com</span> <br> Token Expire :<span class="label  text-danger-600">04/10/2017</span>
															
														</div>
													</div>
                                                    
                                                    
												</td>
                                               
                                               
												<td>
                                                	<div class="media-left">
														<div class="text-default text-semibold">ประชาพิทักษ์  สารีบุตร</div>
														<div class="text-muted text-size-small">
                                                         
                                                      Email :<span class="label  text-success-600">sale@newvendor.com</span> <br> Date :<span class="label  text-danger-600">04/10/2017</span>
															
														</div>
													</div>
                                                
                                                
                                                </td>
                                                    
                                                    
                                                 <td><div class="media-left">
														<div class="text-muted"> <span class="label label-flat border-success text-success-300">กลุ่มงานจัดซื้อทั่วไป (Commercial) </span> </div>
														
													</div></td>
                                               <td><div class="media-left">
														<div class="text-muted"> <span class="label label-flat border-success text-success-300">เกณฑ์ของงานจัดซื้อทั่วไป</span> </div>
														
													</div></td>
                                            
                                             
												
												
                                               
                                                <td><span class="label bg-danger">ยังไม่ลงทะเบียน</span></td>
												<td class="text-center">
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
																
																<!--<li><a href="#"><i class="icon-pencil7"></i> Edit / แก้ไข</a></li>-->
																<li class=""><a href="#"><i class="icon-bin"></i> Delete / ลบข้อมูล</a></li>
																 
															</ul>
														</li>
													</ul>
												</td>
											</tr>              
											
								
							</tbody>
						</table>
                               </div>
                                
                                 
									
								</div>
							</div>
            
                    

					<!-- Form horizontal -->
					
					<!-- /form horizontal -->

                    
		        

		         


		                 <!-- Primary modal -->
					<div id="modal_theme_primary" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									
									<h6 class="modal-title">ข้อความจากระบบ</h6>
								</div>

								<div class="modal-body">
									<h6 class="text-semibold">แจ้งผู้ค้ารายใหม่</h6>
									<p> ทางบริษัทได้รับข้อมูลของท่านแล้ว ขณะนี้อยู่ระหว่างการตรวจสอบจากทางเจ้าหน้าที่  </p>

									<hr>

									
									<p>หากตรวจพบว่าข้อมูลของท่านไม่ถูกต้อง บริษัทฯ ขอสงวนสิทธิ์ ในการสั่งซื้อสินค้าจาก ท่าน หรือ หากท่านไม่ได้รับความเป็นธรรมในการให้บริการจาก ฝ่ายจัดซื้อโปรดติดต่อ Tel. 02 345 6789 หน่วยงาน ตรวจสอบ </p>
									
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">รับทราบ</button>
									
								</div>
							</div>
						</div>
					</div>
					<!-- /primary modal -->



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
    <!-- Vertical form modal -->
					<div id="modal_form_vertical" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มสินค้า</h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ผลิต</label>
													<input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สินค้าที่จำหน่าย</label>
													<input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
												</div>
											</div>
										</div>
                                        
                                        

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ประเทศที่ผลิต</label>
													<input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
													<input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<label>Load Time</label>
													<input type="text" placeholder="ระบุ Load Time" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Minimum Order</label>
													<input type="text" placeholder="ระบุ Minimum Order" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Packing Size </label>
													<input type="text" placeholder="ระบุ Packing Size" class="form-control">
												</div>
											</div>
										</div>

										
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
													<input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
													<span class="help-block">eg: k.หนูแดง T 023456789</span>
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>MSDS(ไทย/อังกฤษ) </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>เงือนไขในการชำระเงิน</label>
													<input type="text" placeholder="ระบุเงือนไขในการชำระเงิน" class="form-control">
													<span class="help-block">eg: 30 วัน , 90 วัน </span>
												</div>

												<div class="col-sm-6">
                                                
                                              
                                              
                                              <label>สถานะซื้อขายกับ TOA </label> 
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
              					                 
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>

                                                
												
													
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติมที่เกี่ยวกับตัวสินค้า : </label>
		                                    <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
		                                </div>
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


  <!-- Vertical form modal -->
					<div id="modal_form_acc" class="modal fade">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i>  Generate New Token </h5>
								</div>

								 <form class="form-validation form-validate-jquery" id="add_editflow" action="#"  method="post">
									<div class="modal-body">
									 
								<fieldset class="content-group">
									<legend class="text-bold"  style="font-size:20px;">สร้างรหัสผ่านชั่วคราว </legend>



									
                                    
                                    
                                    
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-3"><strong>โปรดเลือก Flow การอนุมัติ</strong></label>
			                        	<div class="col-lg-9 ">
				                            <select name="tk_flow_id" id="tk_flow_id" class="form-control" required>
                                            
                                            
                                             <?php 
											if($queryflow){
												foreach($queryflow as $queryflows ){
													
													?>
													 <option value="<?php echo $queryflows->flow_id;?>"><?php echo $queryflows->flow_name;?></option>
													<?php
													
													}
												}
											?>
                                            
				                               
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Flow ที่ท่านสร้างไว้ และได้ รับ Permission จากระบบเท่านั้น</span>
                                            
			                            </div>
			                        </div>
                                    
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-3"><strong>โปรดเลือก เกณฑ์ การประเมิณ</strong></label>
			                        	<div class="col-lg-9 ">
				                            <select name="tk_q_id" id="tk_q_id" class="form-control" required>
                                            
                                              <?php 
											if($querytype_question){
												foreach($querytype_question as $querytype_questions ){
													
													?>
													 <option value="<?php echo $querytype_questions->q_id;?>"><?php echo $querytype_questions->q_name;?></option>
													<?php
													
													}
												}
											?>
                                            
                                            
				                            
				                              
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ เกณฑ์ ที่ท่านสร้างไว้จากระบบเท่านั้น</span>
                                            
			                            </div>
			                        </div>
                                    
                                    
                                    <div class="form-group">
                                    
                                    <label class="control-label col-lg-3"><strong>สร้างรหัสผ่านชั่วคราว </strong></label>
                                    
                                    <div class="col-lg-9 ">
				                          
                                          <div class="col-lg-12">
                                          
                                          <div class="label-indicator-absolute">
												<input type="text" name="tk_token"  id="tk_token" class="form-control" placeholder="Enter your password" required="required" readonly>
												<span class="label password-indicator-label-absolute"></span>
											</div>
                                            
                                            
                                            <span class="help-block help-block label  text-danger-600">รหัสผ่านชั่วคราวมีอายุ 24 ชม. หาก คู่ค้าไม่เข้ามาทำการลงทะเบียน ภายในเวลาที่กำหนด จะต้องทำการสร้าง รหัสผ่านชั่วคราวใหม่ </span>
                                            
                                          </div>
                                            
                                            
                                           <div class="col-lg-12">
                                            <button type="button" class="btn btn-info generate-label-absolute">สร้างรหัสผ่านชั่วคราว</button>
                                            </div>
                                            
			                            </div>
                                    
											
                                            
                                            	
                                            
										</div>

									
                                    
                                    
                                    <div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-3"><strong>ระบุชื่อ บริษัท </strong></label>
										<div class="col-lg-9  has has-feedback">
											<input type="text" name="tk_company" id="tk_company" class="form-control" placeholder="Enter Company Name" value="" required="required">
                                            
                                             <span class="help-block label  text-info-600">*ชื่อนี้จะปรากฎและคู่ค้าสามารถแก้ไขได้ เมื่อเข้าสู่ระบบครั้งแรก</span>
										</div>
									</div>
                                    
                                    
                                     <div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-3"><strong>ระบุ Email ในการติดต่อ </strong></label>
										<div class="col-lg-9  has has-feedback">
											<input type="email" required="required" class="form-control" name="tk_email" id="tk_email" placeholder="Enter email for contact" value="">
                                            
                                             <span class="help-block label  text-danger-600">*Email นี้จะปรากฎและคู่ค้า <strong>ไม่</strong> สามารถแก้ไขได้ เมื่อเข้าสู่ระบบ</span>
										</div>
									</div>
                                    
                                    
                                 
                                  
                              
                              </fieldset>
                              
                              
            
            <div class="alert alert-info alert-styled-left">
										
										<span class="text-semibold">ไม่ต้องห่วง !</span>  เราจะส่งรหัสผ่านชั่วคราวไปยัง Email ของท่าน และ Email ของ คู่ค้าที่ท่านได้ทำการลงทะเบียนไว้
                                       
                                        			    </div>
                                        
                                  
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
                                       
										
                                        <button type="submit" class="btn btn-primary">ดำเนินการสร้างรหัสผ่านชั่วคราว
                                    
                                     </button>
                                        
                                        
										  
                                        
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->

 
<script type="text/javascript">

// Custom tag class

    $('.tokenfield').tokenfield();
    $('.tagsinput-custom-tag-class').tagsinput({
        tagClass: function(item){
            return 'label bg-success';
        }
    });
	
	    // Display values
    $('.tags-input, [data-role="tagsinput"], .tagsinput-max-tags, .tagsinput-custom-tag-class').on('change', function(event) {
        var $element = $(event.target),
            $container = $element.parent().parent('.content-group');

        if (!$element.data('tagsinput'))
        return;

        var val = $element.val();
        if (val === null)
        val = "null";
    
        $('pre.val > code', $container).html( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
        $('pre.items > code', $container).html(JSON.stringify($element.tagsinput('items')));
        Prism.highlightAll();
    }).trigger('change');




function noti(val) {
	
	 new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
	
    //alert("The input value has changed. The new value is: " + val);
}

	 $('#pnotify-successx').on('click', function () {
        new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
    });

 // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();




    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });


	

    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



 var validatorst1 = $("#add_editflow").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
			tk_company:{
                minlength: 10
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 1
            }, 
			flow_approve_acc: {
                minlength: 1
            },
			
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "คุณต้องการบันทึก รหัสผ่านชั่วคราว ลงระบบหรือไม่ ? ",
            text: "ทันทีที่คุณกดบันทึก ระบบจะทำการส่ง Email หาคู่ค้าทันที ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
				
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='../gentoken/';
  }  , 1000 );
				
				
				}
            }
        });
				
				$.ajax({
					
			url: "../toa_gentoken",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					tk_flow_id	: 	$('#tk_flow_id').val(),
					tk_q_id	: 	$('#tk_q_id').val(),
					tk_token	: 	$('#tk_token').val(),
					tk_company	: 	$('#tk_company').val(),
					tk_email	: 	$('#tk_email').val(),
					tk_user_create : '<?php echo $getuser->username;?>',
					tk_mode	: 	'gentoken',
					//flow_approve_acc	: 	$('#flow_approve_acc').val(),
					
					//flow_approve_acc	: 	$('[name="flow_approve_acc_m"]:checked').val(),
					
					
					
					
					//role	: 	val,
					//value	: 	newval,
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
				
				
				
			}
		});		
				
               
				
					//alert(data);
				 swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
					
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "คุณได้กดยกเลิกการบันทึกข้อมูล",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		/*	
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		*/
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
    });




 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


	
   // With validation
   
	
 $(window).on('load',function(){
	 
	 
	 /*swal({
		 
		  title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบระหว่างนี้ท่านจะไม่สามารถแก้ไข ข้อมูลใดๆ ได้ จนกว่าทางเจ้าหน้าที่ของ TOA GROUP ตรวจสอบข้อมูลเสร็จ",
            confirmButtonColor: "#66BB6A",
            type: "success",
			confirmButtonText: "ตกลง",
		 
			
			   title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบ",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        });*/
	 
	// $('#modal_theme_primary').modal('show');
	 //validation-next
	/* $('#validation-next').click(function(){
		 $('#modal_theme_primary').modal('show');
		 });*/
       // $('#modal_theme_primary').modal('show');
	  
	 /* bootbox.dialog({
                title: "กรุณาระบุรหัสผ่าน",
                message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                        '<form class="form-horizontal">' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">Name</label>' +
                                '<div class="col-md-8">' +
                                    '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                                    '<span class="help-block">Here goes your name</span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">How awesome is this?</label>' +
                                '<div class="col-md-8">' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked">' +
                                            'Really awesomeness' +
                                        '</label>' +
                                    '</div>' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                                            'Super awesome' +
                                        '</label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '</div>',
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            bootbox.alert("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        ); */
    });
	
	 
	/*
	 $('#validation-next').on('click', function() {
        
		  swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
    });*/
	
	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });

 // Custom bootbox dialog with form
   

</script>


</body>




</html>