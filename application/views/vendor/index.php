<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php echo $title;?></title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
<?php $this->load->view('main/allcss');?>
<?php $this->load->view('main/alljs3');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>uploadify/uploadifive.css">
</head>

<body >
<?php


  $thislang = $this->lang->load('vendorregister',$this->session->userdata('lang'));
  
  
?>
<?php $this->load->view('mainvendor/navbar');?>

<!-- Page container -->
<div class="page-container"> 
  
  <!-- Page content -->
  <div class="page-content">
    <?php $this->load->view('mainvendor/navigation');?>
    
    <!-- Main content -->
    <div class="content-wrapper"> 
      
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4><i class="icon-arrow-right15 position-left"></i>
              <?php
							//echo base_url();
							
							



							

                            echo $this->lang->line('heading_page1', FALSE);
							
							?>
            </h4>
          </div>
          <div class="heading-elements">
            <div class="heading-btn-group"> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-cube4 text-primary "></i><span>Product</span></a> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-file-text text-primary"></i> <span>Doccument</span></a> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-users4 text-primary"></i> <span>Contact</span></a> </div>
          </div>
        </div>
      </div>
      <!-- /page header -->
      
      <div class="content" style="padding-bottom:20px;">
        <div class="alert alert-info alert-styled-left">
        <span class="text-semibold">ยินดีด้วย เราได้รับข้อมูลของท่านแล้ว  ซึ่งอยู่ระหว่างดำเนินการ ตรวจสอบข้อมูลของท่าน จากเจ้าหน้าที่ หาก สินค้าของท่านผ่านการอนุมัติ ระบบจะแจ้ง ไปที่ Email ที่ท่านได้ ลงทะเบียนไว้กับเรา ทางเราหวังเป็นอย่างยิ่งว่าเราจะมีโอกาสได้ทำธุรกิจร่วมกับคุณ  </span>
          <?php
                         //   echo $this->lang->line('heading_notification', FALSE);
							
							?>
        </div>
      </div>
      <?php //$this->load->view('dashboard/toa/noti');?>
      
      <!-- Content area -->
      <div class="content"> 
        
        <!-- Input group buttons -->
        <div class="panel panel-flat">
          <div class="panel-body">
            
          </div>
        </div>
        <!-- /input group buttons --> 
        
        <!-- Primary modal -->
        <div id="modal_theme_primary" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">ข้อความจากระบบ</h6>
              </div>
              <div class="modal-body">
                <h6 class="text-semibold">แจ้งผู้ค้ารายใหม่</h6>
                <p> ขอความร่วมมือคู่ค้าทุกท่าน กรอกข้อมูล ตามเงื่อนไขของทาง ทีโอเอ เพื่อ ความสะดวกรวดเร็วในการสั่งซื้อสินค้า และ ส่งข้อมูล จากเรา </p>
                <hr>
                <p>หากตรวจพบว่าข้อมูลของท่านไม่ถูกต้อง บริษัทฯ ขอสงวนสิทธิ์ ในการสั่งซื้อสินค้าจาก ท่าน หรือ หากท่านไม่ได้รับความเป็นธรรมในการให้บริการจาก ฝ่ายจัดซื้อโปรติดต่อ Tel. 02 345 6789 หน่วยงาน ตรวจสอบ </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">รับทราบ</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /primary modal -->
        
        <?php $this->load->view('main/footer');?>
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container --> 
          
                    
       







</body>
</html>