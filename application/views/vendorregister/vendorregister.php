<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php echo $title;?></title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
<?php $this->load->view('main/allcss');?>
<?php $this->load->view('main/alljs3');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>uploadify/uploadifive.css">
</head>

<body >
<?php


  $thislang = $this->lang->load('vendorregister',$this->session->userdata('lang'));
  
  
?>
<?php $this->load->view('mainvendor/navbar');?>

<!-- Page container -->
<div class="page-container"> 
  
  <!-- Page content -->
  <div class="page-content">
    <?php $this->load->view('mainvendor/navigation');?>
    
    <!-- Main content -->
    <div class="content-wrapper"> 
      
      <!-- Page header -->
      <div class="page-header">
        <div class="page-header-content">
          <div class="page-title">
            <h4><i class="icon-arrow-right15 position-left"></i>
              <?php
							//echo base_url();
							
							



							

                            echo $this->lang->line('heading_page1', FALSE);
							
							?>
            </h4>
          </div>
          <div class="heading-elements">
            <div class="heading-btn-group"> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-cube4 text-primary "></i><span>Product</span></a> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-file-text text-primary"></i> <span>Doccument</span></a> <a href="#" class="btn btn-link btn-float text-size-small has-text disabled"><i class="icon-users4 text-primary"></i> <span>Contact</span></a> </div>
          </div>
        </div>
      </div>
      <!-- /page header -->
      
      <div class="content" style="padding-bottom:20px;">
        <div class="alert alert-warning alert-styled-left">
          <?php
                            echo $this->lang->line('heading_notification', FALSE);
							
							?>
        </div>
      </div>
      <?php //$this->load->view('dashboard/toa/noti');?>
      
      <!-- Content area -->
      <div class="content"> 
        
        <!-- Input group buttons -->
        <div class="panel panel-flat">
          <div class="panel-body">
            <form class="form-validation form-validate-jquery" id="regis_st1" action="#"  method="post" enctype="multipart/form-data" >
              <h6 class="form-wizard-title text-semibold"> <span class="form-wizard-count">1</span> <?php echo $this->lang->line('topic_1', FALSE);?> <small class="display-block"> <?php echo $this->lang->line('s_topic_1', TRUE);?></small> </h6>
              
              <!-- Top component -->
              <div class="content-group border-top-lg border-top-primary">
                <div class="page-header page-header-default pt-20">
                  <div class="breadcrumb-line breadcrumb-line-component">
                    <ul class="breadcrumb">
                      <li><i class="icon-copy position-left"></i> เรียนคู่ค้ารายใหม่ เอกสารที่อัพโหลดเข้าระบบ กรุณา เซ็นต์รับรอง สำเนาถูกต้องทุกฉบับ ทางบริษัทขอสงวนสิทธิ์ <code> ไม่พิจารณาคู่ค้าที่เอกสารไม่สมบูรณ์ </code> และเพื่อความสะดวกในการค้นข้อมูลทางบริษัทขอความร่วมมือในการบันทึกเอกสารที่เป็น <code> ฉบับล่าสุด </code> และหากข้อมูลของท่านมีมากเท่าไหร่ ยิ่งมีผลในการพิจารณาสั่งซื้อสินค้าของท่านจากเรา </li>
                    </ul>
                  </div>
                  <div class="page-header-content">
                    <div class="page-title">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>ชื่อผู้ค้า : <span class="text-danger">*</span></label>
                            <input type="text" name="vp1_cname" id="vp1_cname" class="form-control required" placeholder="Company Name " value="<?php echo $vendor_vp1->vp1_cname;?>" readonly >
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Email address: <span class="text-danger">*</span></label>
                            <input type="email" name="email" class="form-control required" placeholder="your@email.com" value="<?php echo $vendor_vp1->vp1_email;?>" readonly>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>เบอร์โทรติดต่อ : <span class="text-danger">*</span> </label>
                            <input type="text" name="vp1_tel" id="vp1_tel" class="form-control required" placeholder="02-345-6789 ext 5678"  onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_tel;?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>เบอร์ติดต่อ กรณีฉุกเฉิน: <span class="text-danger">*</span></label>
                            <input type="text" name="vp1_etel" id="vp1_etel" class="form-control required" placeholder="02-345-6789 ext 5678"  onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_etel;?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>เบอร์โทรสาร : <span class="text-danger">*</span> </label>
                            <input type="text" name="vp1_fax" id="vp1_fax" class="form-control required" placeholder="02-345-6789"  onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_fax;?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Website : </label>
                            <input type="text" name="vp1_web" id="vp1_web" class="form-control" placeholder="eg : www.toagroup.com" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_web;?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>ที่อยู่ตาม (หนังสือรับรองบริษัท) : <span class="text-danger">*</span></label>
                            <input type="text" name="vp1_add1" id="vp1_add1" class="form-control required" placeholder="ระบุที่อยู่ตามหนังสือรับรอง" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add1;?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <label> &nbsp;</label>
                          <div class="row">
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add1_province" id="vp1_add1_province" class="form-control required" placeholder="จังหวัด" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add1_province;?>">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add1_amphur" id="vp1_add1_amphur" class="form-control required" placeholder="อำเภอ" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add1_amphur;?>">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add1_thumbol" id="vp1_add1_thumbol" class="form-control required" placeholder="ตำบล" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add1_thumbol;?>">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add1_post" id="vp1_add1_post" class="form-control required" placeholder="รหัสไปรษณีย์" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add1_post;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group">
                          <label class="control-label col-lg-3">ที่อยู่ที่ติดต่อได้  : <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" name="vp1_same_add1" id="vp1_same_add1" class="styled"   <?php if($vendor_vp1->vp1_same_add1 == 1){
												echo 'checked';		
														} ?>    onchange="checkbox(this.name,this.value)">
                                ใช้ที่อยู่เดียวกับ ที่อยู่ตามหนังสือรับรอง </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" name="vp1_add2" id="vp1_add2" class="form-control required" placeholder="ระบุ ที่อยู่ที่ติดต่อได้" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add2;?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add2_province" id="vp1_add2_province" class="form-control required" placeholder="จังหวัด" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add2_province;?>">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add2_amphur" id="vp1_add2_amphur" class="form-control required" placeholder="อำเภอ" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add2_amphur;?>">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add2_thumbol" id="vp1_add2_thumbol" class="form-control required" placeholder="ตำบล" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add2_thumbol;?>">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group">
                                <input type="text" name="vp1_add2_post" id="vp1_add2_post" class="form-control required" placeholder="รหัสไปรษณีย์" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_add2_post;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6"> 
                          
                          <!-- Styled checkbox group -->
                          <div class="form-group">
                            <label class="control-label col-lg-6">ประเภทกิจการ <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" name="vp1_croptype" id="vp1_croptype1" class="styled" required="required" value="1" onchange="checkbox2(this.id,this.value)"  <?php if($vendor_vp1->vp1_croptype1 == 1){
												echo 'checked';		
														} ?>    >
                                  โรงงาน </label>
                              </div>
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" name="vp1_croptype" id="vp1_croptype2" class="styled"  value="1" onchange="checkbox2(this.id,this.value)" <?php if($vendor_vp1->vp1_croptype2 == 1){
												echo 'checked';		
														} ?>  >
                                  ซื้อมาขายไป </label>
                              </div>
                              <div class="input-group"> <span class="input-group-addon">
                                <input type="checkbox" name="vp1_croptype" id="vp1_croptype3" class="styled" value="1" onchange="checkbox2(this.id,this.value)" <?php if($vendor_vp1->vp1_croptype3 == 1){
												echo 'checked';		
														} ?> >
                                </span>
                                <input type="text" name="vp1_croptype_re" id="vp1_croptype_re" class="form-control" placeholder="อื่นๆ โปรดระบุ " onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_croptype_re;?>">
                              </div>
                              
                              <!--	<div class="checkbox">
												<label class="checkbox-inline">
													<input type="checkbox" name="styled_checkbox" class="styled"> 
                                                    </label>
                                                    <label class="checkbox-inline" style="    margin-left: -20px;    padding-left: 18px;">
													<input type="text" name="tel" class="form-control" placeholder="อื่นๆ โปรดระบุ  " >
												</label>
											</div>--> 
                            </div>
                          </div>
                          <!-- /styled checkbox group --> 
                          
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>เริ่มประกอบกิจการตั้งแต่ : </label>
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <select name="vp1_cropstart_y" id="vp1_cropstart_y" data-placeholder="Year" class="select required" onchange="autosave(this.name,this.value)">
                                    <option></option>
                                    <?php
                                                $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910)); 
												
												foreach($years as $key => $val){
													?>
                                    <option value="<?php echo $key;?>"  <?php  if($vendor_vp1->vp1_cropstart_y==$key){ echo 'selected';
														}
													
													?>><?php echo $val;?></option>
                                    <?php
													}
												//print_r($years);
												?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <input type="text" name="vp1_cropstart_cost" id="vp1_cropstart_cost" class="form-control required" placeholder="เงินทุนประมาณ" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_cropstart_cost;?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <input type="text" name="vp1_cropstart_sale" id="vp1_cropstart_sale" class="form-control required" placeholder="ยอดจำหน่ายต่อปี" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_cropstart_sale;?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <input type="text" name="vp1_cropstart_emp" id="vp1_cropstart_emp" class="form-control required" placeholder="จำนวนพนักงาน" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_cropstart_emp;?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <input type="text" name="vp1_cropstart_ceo" id="vp1_cropstart_ceo" class="form-control required" placeholder="ชื่อผู้จัดการใหญ่ / เจ้าของกิจการ" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_cropstart_ceo;?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row" id="div_vp_allfile">
                      
                      
                      <div class="col-md-6">
                          <div class="form-group" id="vp_upfile1"   <?php if($vp1_file_crop1){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                            <label>เอกสารการจด ทะเบียนพาณิชย์ / ทะเบียนการค้า <span class="text-danger">*</span> : </label>
                            <br>
                            <input id="vp_file1" name="vp_file1" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file1"   <?php if($vp1_file_crop1){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>เอกสารการจด ทะเบียนพาณิชย์ / ทะเบียนการค้า <span class="text-danger">*</span> : </label>
                            <?php
if($vp1_file_crop1){
	foreach($vp1_file_crop1 as $vp1_file_crop1s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile1"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile1('<?php echo  $vp1_file_crop1s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop1s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile1" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop1s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                        </div>
                      
                      
                      <div class="col-md-6">
                          <div class="form-group" id="vp_upfile2"   <?php if($vp1_file_crop2){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                            <label>เอกสารทะเบียนภาษีมูลค่าเพิ่ม (ภพ.20) <span class="text-danger">*</span> : </label>
                            <br>
                            <input id="vp_file2" name="vp_file2" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file2"   <?php if($vp1_file_crop2){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>เอกสารทะเบียนภาษีมูลค่าเพิ่ม (ภพ.20) <span class="text-danger">*</span> : </label>
                            <?php
if($vp1_file_crop2){
	foreach($vp1_file_crop2 as $vp1_file_crop2s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile2"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile2('<?php echo  $vp1_file_crop2s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop2s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile2" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop2s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /top component -->
              
              <div class="row">
                <div class="col-md-12">
                  <div class="content-group border-top-lg border-top-primary">
                    <div class="page-header page-header-default pt-20">
                      <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                          <li><i class="icon-copy position-left"></i> ข้อมูลบัญชี / เช็ค ในการชำระเงินให้กับบริษัทท่าน โปรดระบุ ข้อมูลให้ถูกต้อง และยืนยันสำเนาถูกต้องให้เรียบร้อย เพื่อให้ง่ายต่อการตรวจสอบข้อมูลของเจ้าหน้าที่ หากมีข้อสงสัยโปรดติดต่อ คุณ xxx Tel 023456789 </li>
                        </ul>
                      </div>
                      <div class="page-header-content">
                        <div class="page-title">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-3">
                                <label>ชื่อบัญชี</label>
                                  <input type="text" name="vp1_acc_name" id="vp1_acc_name" class="form-control required" placeholder="ระบุชื่อบัญชี" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_acc_name;?>">
                                
                              </div>
                              <div class="col-sm-3">
                                <label>เลขที่บัญชี</label>
                              
                                 <input type="text" name="vp1_acc_no" id="vp1_acc_no" class="form-control required" placeholder="ระบุเลขที่บัญชี" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_acc_no;?>">
                                
                              </div>
                              <div class="col-sm-3">
                                <label>ชื่อธนาคาร</label>
                                
                                 <input type="text" name="vp1_acc_bname" id="vp1_acc_bname" class="form-control required" placeholder="ระบุชื่อธนาคาร" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_acc_bname;?>">
                                
                              </div>
                              <div class="col-sm-3">
                                <label>สาขา</label>
                              
                                 <input type="text" name="vp1_acc_bbname" id="vp1_acc_bbname" class="form-control required" placeholder="ระบุสาขา" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_acc_bbname;?>">
                                
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-3">
                                <label>ชื่อผู้ดูแลบัญชี</label>
                              
                                 <input type="text" name="vp1_acc_sname" id="vp1_acc_sname" class="form-control required" placeholder="ระบุชื่อผู้ดูแลบัญชี" onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_acc_sname;?>">
                                
                              </div>
                              <div class="col-sm-3">
                                <label>เบอร์โทรติดต่อ</label>
                              
                                                            <input type="text" name="vp1_acc_tsname" id="vp1_acc_tsname" class="form-control required" placeholder="ระบุเบอร์โทรติดต่อ"  onchange="autosave(this.name,this.value)" value="<?php echo $vendor_vp1->vp1_acc_tsname;?>">

                                
                                
                              </div>
                              <div class="col-sm-3">
                                <label>หมายเหตุ / รายละเอียดเพิ่มเติม : </label>
                                <textarea name="vp1_acc_remark" id="vp1_acc_remark" rows="1" cols="4" placeholder="" class="form-control" onchange="autosave(this.name,this.value)"> <?php echo $vendor_vp1->vp1_acc_remark;?> </textarea>
                                
                                
                              
                                
                                
                              </div>
                              
                              <div class="col-sm-3">
                          <div class="form-group" id="vp_upfile3"   <?php if($vp1_file_crop3){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                            <label>สำเนาบัญชีธนาคาร<span class="text-danger">*</span> : </label>
                            <br>
                            <input id="vp_file3" name="vp_file3" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file3"   <?php if($vp1_file_crop3){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>สำเนาบัญชีธนาคาร <span class="text-danger">*</span> : </label> <br>
                            <?php
if($vp1_file_crop3){
	foreach($vp1_file_crop3 as $vp1_file_crop3s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile3"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile3('<?php echo  $vp1_file_crop3s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop3s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile3" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop3s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                        </div>
                              
                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h6 class="form-wizard-title text-semibold"> <span class="form-wizard-count">2</span> ข้อมูลสินค้า และบริการที่ จำหน่ายให้กับบริษัท TOA PAINT <small class="display-block">กรุณากรอกข้อมูลให้ครบถ้วน ตามที่แบบฟอร์มกำหนด</small> </h6>
              
              <!-- Top component -->
              <div class="content-group border-top-lg border-top-primary">
                <div class="page-header page-header-default pt-20">
                  <div class="breadcrumb-line breadcrumb-line-component">
                    <ul class="breadcrumb">
                      <li><i class="icon-copy position-left"></i> รายชื่อลูกค้าที่มีการซื้อขายกับบริษัทของท่าน โดยท่านจะต้องระบุ อย่างน้อย 3 รายชื่อ โดย ในแต่ละรายชื่อเมื่อท่าน ระบุเสร็จแล้วให้กด <code>Enter</code> เพื่อให้ระบบบันทึกข้อมูลของท่าน หากต้องการยกเลิกให้กด ที่เครื่องหมาย <code> X</code> (กากบาท) </li>
                    </ul>
                  </div>
                  <div class="page-header-content">
                    <div class="page-title">
                      <div class="form-group">
                        <div class="input-group ">
                          <div class="input-group-addon"><i class=" icon-users4"></i></div>
                          <input type="text" name="vp2_vendor_customer" id="vp2_vendor_customer" style="min-width: 300px;" placeholder="ระบุ รายชื่อลูกค้า/บริษัท" value="<?php echo $vendor_vp2->vp2_vendor_customer;?>" class="tagsinput-custom-tag-class required" onchange="vp2autosave(this.name,this.value)"   >
                            
                             
                                

                          
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /top component -->
              <div class="row" id="section2">
                <div class="col-md-12">
                  <div class="content-group border-top-lg border-top-primary">
                    <div class="page-header page-header-default pt-20">
                      <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                          <li><i class="icon-copy position-left"></i> ข้อมูลสถานที่เก็บสินค้า โปรดระบุอย่างน้อย 1 รายการเพื่อให้เรามั่นใจว่าสินค้าของท่านมีอยู่จริง และเพื่อให้เราค้นหาสินค้าของท่านอย่างรวดเร็วเมื่อมีการสั่งซื้อ หรืออยากได้ ตัวอย่างจากท่าน</li>
                        </ul>
                      </div>
                      <div class="page-header-content">
                        <div class="page-title">
                          <a href="<?php echo base_url(); ?>vendorregister/locator?mode=addlocator"  class="btn btn-primary btn-sm" >เพิ่มข้อมูลสถานที่เก็บสินค้า <i class="icon-plus3 position-right"></i></a>
                          <table class="table datatable-basic">
                            <thead>
                              <tr>
                                <th>ชื่อสถานที่จัดเก็บ</th>
                                <th>ชื่อผู้ติดต่อ</th>
                                <th>เบอร์โทร</th>
                                <th>ที่อยู่</th>
                                
                                <th>หมายเหตุ</th>
                                
                               
                                
                                <th class="text-center">Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
							

if($vendor_vp2store){
	foreach($vendor_vp2store as $vendor_vp2stores){
		?>
        <tr>
                                <td><?php echo $vendor_vp2stores->vp2_vstorename;?></td>
                                <td><?php echo $vendor_vp2stores->vp2_vcontact;?></td>
                                <td><?php echo $vendor_vp2stores->vp2_vtelno;?></td>
                               
                                <td><?php echo $vendor_vp2stores->vp2_vaddress;?></td>
                                <td><?php echo $vendor_vp2stores->vp2_vremark;?></td>
                                
                                <td class="text-center">
                                
                                                                        
                                        <a   href="<?php echo base_url(); ?>vendorregister/locator?mode=editlocator&id=<?php echo $vendor_vp2stores->vp2_id;?>" title="Edit"> <i class="icon-pencil5"></i></a> |
                                        <a href="#nogo" onclick="vp_deletelocator('<?php echo  $vendor_vp2stores->vp2_id;?>')"  title="Delete"><i class="icon-bin"></i></a>
                                        

                                
                               </td>
                              </tr>
        <?php
		
		}
	}


							
							?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div class="row" id="vendorproduct">
                <div class="col-md-12">
                  <div class="content-group border-top-lg border-top-primary">
                    <div class="page-header page-header-default pt-20">
                      <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                          <li><i class="icon-copy position-left"></i> ข้อมูลสินค้าของบริษัท โปรดตรวจสอบข้อมูลสินค้าของท่านให้เรียบร้อยก่อนทำการบันทึกเข้าระบบ เพื่อให้สินค้าของท่านค้นหาได้ง่าย จากเรา </li>
                        </ul>
                      </div>
                      <div class="page-header-content">
                        <div class="page-title">
                        
                          
                          
                                                                                                      <a href="<?php echo base_url(); ?>vendorregister/vendorproduct?mode=add"  class="btn btn-primary btn-sm" >เพิ่มสินค้าที่จำหน่าย <i class="icon-plus3 position-right"></i></a>

                          
                          
                          <table class="table datatable-basic">
                            <thead>
                              <tr>
                                <th>ชื่อผู้ผลิต</th>
                                <th>สินค้าที่จำหน่าย</th>
                                <th>ประเทศที่ผลิต</th>
                                <th>Minimum Order</th>
                                <th>Contact</th>
                              
                                <th class="text-center">Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
							function randomName() {
   
   
   $names = array(
   'Major',
'Serve',
'Think',
'PageOne',
'Activate',
'CountryClub',
'Benefits',
'Connect',
'Fund',
'Pirates',

   );
   
   /* $names = array(



'Endeavor Co.,ltd',
'Wiz Co.,ltd',
'Crawler Co.,ltd',
'Guru Co.,ltd',
'Trick Co.,ltd',
'Universe Co.,ltd',
'Trilogy Co.,ltd',
'Insta Co.,ltd',
'Wisdom Co.,ltd',
'Welcome Co.,ltd',
'SweetWater Co.,ltd',
'Resource Co.,ltd',
'RedSky Co.,ltd',
'Leaf Co.,ltd',
'Bumblebee Co.,ltd',
'Glass Co.,ltd',



    );*/
    return $names[rand ( 0 , count($names) -1)];
}

function randomName2() {
   
   
   $names = array(
   'Jonh Crop',
'Mocrosoft',
'Adobe',
'Autodesk',


   );
   
 
    return $names[rand ( 0 , count($names) -1)];
}


if($vendor_vp2product){
	foreach($vendor_vp2product as $vendor_vp2products){
		?>
        <tr>
                                <td><?php echo $vendor_vp2products->vp2_vp_owner;?></td>
                                <td><?php echo $vendor_vp2products->vp2_vp_name;?></td>
                                <td><?php echo $vendor_vp2products->vp2_vp_country;?></td>
                                <td><?php echo $vendor_vp2products->vp2_vp_min;?></td>
                                <td><?php echo $vendor_vp2products->vp2_vp_info;?></td>
                                
                                <td class="text-center">
                                
                                     <a   href="<?php echo base_url(); ?>vendorregister/vendorproduct?mode=edit&id=<?php echo $vendor_vp2products->vp2_id;?>" title="Edit"> <i class="icon-pencil5"></i></a> |
                                        <a href="#nogo" onclick="vp_deleteproduct('<?php echo  $vendor_vp2products->vp2_id;?>')"  title="Delete"><i class="icon-bin"></i></a>
                                        

                                
                                
                                </td>
                              </tr>
        <?php
		
		}
	
	}


							
							?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-body border-top-danger">
                    <h6 class="no-margin text-semibold">รายการสินค้าของท่าน  ( Catalog)<span class="text-danger">*</span></h6>
                    <p class="content-group-sm text-muted"> สำหรับสินค้าเพิ่มเติม และ สินค้าใน Catalog ของท่าน</p>
                    <hr>
                    
                    
                    
                    
                      <div class="form-group" id="vp_upfile4"   <?php if($vp1_file_crop4){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                           
                          
                            <input id="vp_file4" name="vp_file4" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file4"   <?php if($vp1_file_crop4){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                           
                            <?php
if($vp1_file_crop4){
	foreach($vp1_file_crop4 as $vp1_file_crop4s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile4"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile4('<?php echo  $vp1_file_crop4s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop4s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile4" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop4s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                    
                    
                    
                    
                    
                    
                    
                    
                  </div>
                </div>
                
                
                
                
                <div class="col-md-6">
                  <div class="panel panel-body border-top-danger">
                    <h6 class="no-margin text-semibold">รายงาน ข้อมูล Heavy Metal <span class="text-danger">*</span></h6>
                    <p class="content-group-sm text-muted"> รายงานข้อมูลเกี่ยวกับสารโลหะหนักในสินค้าของท่าน</p>
                    <hr>
                    
                     
                      <div class="form-group" id="vp_upfile5"   <?php if($vp1_file_crop5){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                           
                          
                            <input id="vp_file5" name="vp_file5" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file5"   <?php if($vp1_file_crop5){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                           
                            <?php
if($vp1_file_crop5){
	foreach($vp1_file_crop5 as $vp1_file_crop5s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile5"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile4('<?php echo  $vp1_file_crop5s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop5s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile5" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop5s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                    
                    
                    
                  </div>
                </div>
              </div>
              <div class="row" id="seller">
                <div class="col-md-12">
                  <div class="content-group border-top-lg border-top-primary">
                    <div class="page-header page-header-default pt-20">
                      <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                          <li><i class="icon-copy position-left"></i> ข้อมูลฝ่ายขายของบริษัท โปรดตรวจสอบข้อมูลฝ่ายขายของท่านให้เรียบร้อยก่อนทำการบันทึกเข้าระบบ เพื่อให้เราค้นหาได้ง่ายเมื่อต้องการสั่งซื้อสินค้าจากท่าน </li>
                        </ul>
                      </div>
                      <div class="page-header-content">
                        <div class="page-title">
                        
                                          
                                                  
                                                                            <a href="<?php echo base_url(); ?>vendorregister/seller?mode=addlocator"  class="btn btn-primary btn-sm" >เพิ่มข้อมูลฝ่ายขาย <i class="icon-plus3 position-right"></i></a>



                         
                          <table class="table datatable-basic">
                            <thead>
                              <tr>
                                <th>Full Name</th>
                                <th>Tel.</th>
                                <th>Email</th>
                                <th>Position</th>
                                <th>Language</th>
                                <th class="text-center">Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($vendor_vp2sale){
								foreach($vendor_vp2sale as $vendor_vp2sales){
									?>
                                    <tr>
                                <td><?php echo $vendor_vp2sales->vp2_sname;?> </td>
                                <td><?php echo $vendor_vp2sales->vp2_stel;?></td>
                                <td><?php echo $vendor_vp2sales->vp2_semail;?></td>
                                <td><?php echo $vendor_vp2sales->vp2_sposition;?></td>
                                <td><?php echo $vendor_vp2sales->vp2_slang;?></td>
                                <td class="text-center">
                                
                                
                                <a   href="<?php echo base_url(); ?>vendorregister/seller?mode=editlocator&id=<?php echo $vendor_vp2sales->vp2_id;?>" title="Edit"> <i class="icon-pencil5"></i></a> |
                                        <a href="#nogo" onclick="vp_deleteseller('<?php echo  $vendor_vp2sales->vp2_id;?>')"  title="Delete"><i class="icon-bin"></i></a>

                                
                                
                                </td>
                              </tr>
                                    <?php
									}
								}
							?>
                            
                              
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h6 class="form-wizard-title text-semibold"> <span class="form-wizard-count">3</span> ข้อมูลอื่นๆ <small class="display-block">กรุณากรอกข้อมูลให้ครบถ้วน ตามที่แบบฟอร์มกำหนด</small> </h6>
              
              <!-- Top component -->
              <div class="content-group border-top-lg border-top-primary">
                <div class="page-header page-header-default pt-20">
                  <div class="page-header-content">
                    <div class="page-title">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="panel panel-body border-top-danger">
                            <h6 class="no-margin text-semibold">บริษัทของท่านมีการจัดกิจกรรม CSR ให้กับลูกค้าของท่านหรือไม่ <span class="text-danger">*</span></h6>
                            <p class="content-group-sm text-muted">กิจกรรมเพื่อสังคมของบริษัท ที่มีกับผู้ประกอบการ</p>
                            <hr>
                            <div class="col-lg-6">
                              <div class="input-group"> <span class="input-group-addon" style="padding-right:10px !important;">
                                <input type="radio" name="vp3_csr" class="styled" required="required" value="1" <?php if($vendor_vp3->vp3_csr == 1){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                
                                
                                </span>
                                <input type="text" name="vp3_csr_comment" id="vp3_csr_comment"  class="form-control" placeholder="มีการจัดกิจกรรมโปรดระบุ"  onchange="vp3autosave(this.name,this.value)" value="<?php if($vendor_vp3->vp3_csr == 1){ echo $vendor_vp3->vp3_csr_comment; } ?>">
                                
                                
                                 
                                 
                                 
                                
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_csr" class="styled"  required="required" value="2" <?php if($vendor_vp3->vp3_csr == 2){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)" >
                                  อยู่ระหว่างการจัดตั้งโครงการ </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_csr" class="styled"  required="required" value="3" <?php if($vendor_vp3->vp3_csr == 3){ echo 'checked'; }?> onchange="vp3autosave(this.name,this.value)">
                                  ยังไม่ได้เริ่มดำเนินการ </label>
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label>หมายเหตุ / รายละเอียดเพิ่มเติมเกี่ยวกับกิจกรรม CSR (ถ้ามี) : </label>
                             
                                  <textarea name="vp3_csr_remark" id="vp3_csr_remark" rows="2" cols="4" placeholder="" class="form-control" onchange="vp3autosave(this.name,this.value)"><?php echo $vendor_vp3->vp3_csr_remark;?></textarea>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="panel panel-body border-top-danger">
                            <h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO-9000 หรือไม่ <span class="text-danger">*</span></h6>
                            <p class="content-group-sm text-muted"> โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
                            <hr>
                            
                            <!-- Styled checkbox group -->
                            <div class="form-group">
                              <div class="radio">
                                <label>
                                
                                  <input type="radio" name="vp3_iso9000" class="styled" required="required" value="1" <?php if($vendor_vp3->vp3_iso9000 == 1){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">

                                  
                                  
                                  ได้รับการรับรอง </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="vp3_iso9000" class="styled" required="required" value="2" <?php if($vendor_vp3->vp3_iso9000 == 2){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                    
                                  อยู่ระหว่างดำเนินการขอ </label>
                              </div>
                              <div class="radio">
                                <label>
                                
                                    <input type="radio" name="vp3_iso9000" class="styled" required="required" value="3" <?php if($vendor_vp3->vp3_iso9000 == 3){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                    
                                  ยังไม่ได้เริ่มดำเนินการ </label>
                              </div>
                            </div>
                            <!-- /styled checkbox group -->
                            
                            <div class="form-group">
                              <label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
                                <textarea name="vp3_iso9000_re" id="vp3_iso9000_re" rows="2" cols="4" placeholder="" class="form-control" onchange="vp3autosave(this.name,this.value)"><?php echo $vendor_vp3->vp3_iso9000_re;?></textarea>
                                
                            </div>
                            
                            
                             <div class="form-group" id="vp_upfile6"   <?php if($vp1_file_crop6){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                             <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
                            <br>
                            <input id="vp_file6" name="vp_file6" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file6"   <?php if($vp1_file_crop6){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label> <br>
                            <?php
if($vp1_file_crop6){
	foreach($vp1_file_crop6 as $vp1_file_crop6s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile6"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile6('<?php echo  $vp1_file_crop6s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop6s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile6" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop6s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                            
                            
                            
                            
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="panel panel-body border-top-danger">
                            <h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO-14000 หรือไม่ <span class="text-danger">*</span></h6>
                            <p class="content-group-sm text-muted"> โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
                            <hr>
                            
                            <!-- Styled checkbox group -->
                            <div class="form-group">
                              <div class="radio">
                                <label>
                                    <input type="radio" name="vp3_iso14000" class="styled" required="required" value="1" <?php if($vendor_vp3->vp3_iso14000 == 1){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                    
                                    
                                  ได้รับการรับรอง </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso14000" class="styled" required="required" value="2" <?php if($vendor_vp3->vp3_iso14000 == 2){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                  อยู่ระหว่างดำเนินการขอ </label>
                              </div>
                              <div class="radio">
                                <label>
                                    <input type="radio" name="vp3_iso14000" class="styled" required="required" value="3" <?php if($vendor_vp3->vp3_iso14000 == 3){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                  ยังไม่ได้เริ่มดำเนินการ </label>
                              </div>
                            </div>
                            <!-- /styled checkbox group -->
                            
                            <div class="form-group">
                              <label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
                               <textarea name="vp3_iso14000_re" id="vp3_iso14000_re" rows="2" cols="4" placeholder="" class="form-control" onchange="vp3autosave(this.name,this.value)"><?php echo $vendor_vp3->vp3_iso14000_re;?></textarea>
                            </div>
                                                         
                                                         
                                                         <div class="form-group" id="vp_upfile7"   <?php if($vp1_file_crop7){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                             <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
                            <br>
                            <input id="vp_file7" name="vp_file7" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file7"   <?php if($vp1_file_crop7){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label> <br>
                            <?php
if($vp1_file_crop7){
	foreach($vp1_file_crop7 as $vp1_file_crop7s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile7"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile7('<?php echo  $vp1_file_crop7s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop7s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile7" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop7s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>

                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="panel panel-body border-top-danger">
                            <h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO-18000 หรือไม่ <span class="text-danger">*</span></h6>
                            <p class="content-group-sm text-muted"> โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
                            <hr>
                            
                            <!-- Styled checkbox group -->
                            <div class="form-group">
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso18000" class="styled" required="required" value="1" <?php if($vendor_vp3->vp3_iso18000 == 1){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                  
                                  
                                  ได้รับการรับรอง </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso18000" class="styled" required="required" value="2" <?php if($vendor_vp3->vp3_iso18000 == 2){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                  อยู่ระหว่างดำเนินการขอ </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso18000" class="styled" required="required" value="3" <?php if($vendor_vp3->vp3_iso18000 == 3){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                  ยังไม่ได้เริ่มดำเนินการ </label>
                              </div>
                            </div>
                            <!-- /styled checkbox group -->
                            
                            <div class="form-group">
                              <label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
                              <textarea name="vp3_iso18000_re" id="vp3_iso18000_re" rows="2" cols="4" placeholder="" class="form-control" onchange="vp3autosave(this.name,this.value)"><?php echo $vendor_vp3->vp3_iso18000_re;?></textarea>
                            </div>
                            
                            
                            <div class="form-group" id="vp_upfile8"   <?php if($vp1_file_crop8){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                             <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
                            <br>
                            <input id="vp_file8" name="vp_file8" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file8"   <?php if($vp1_file_crop8){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label> <br>
                            <?php
if($vp1_file_crop8){
	foreach($vp1_file_crop8 as $vp1_file_crop8s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile8"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile8('<?php echo  $vp1_file_crop8s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop8s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile8" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop8s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                            
                            
                            
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="panel panel-body border-top-danger">
                            <h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO TSI16949 หรือไม่ <span class="text-danger">*</span></h6>
                            <p class="content-group-sm text-muted"> โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
                            <hr>
                            
                            <!-- Styled checkbox group -->
                            <div class="form-group">
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso16949" class="styled" required="required" value="1" <?php if($vendor_vp3->vp3_iso16949 == 1){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">
                                  ได้รับการรับรอง </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso16949" class="styled" required="required" value="2" <?php if($vendor_vp3->vp3_iso16949 == 2){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">

                                  อยู่ระหว่างดำเนินการขอ </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="vp3_iso16949" class="styled" required="required" value="3" <?php if($vendor_vp3->vp3_iso16949 == 3){ echo 'checked'; }?>  onchange="vp3autosave(this.name,this.value)">

                                  ยังไม่ได้เริ่มดำเนินการ </label>
                              </div>
                            </div>
                            <!-- /styled checkbox group -->
                            
                            <div class="form-group">
                              <label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
                               <textarea name="vp3_iso16949_re" id="vp3_iso16949_re" rows="2" cols="4" placeholder="" class="form-control" onchange="vp3autosave(this.name,this.value)"><?php echo $vendor_vp3->vp3_iso16949_re;?></textarea>
                            </div>
                            
                            
                                                        						<div class="form-group" id="vp_upfile9"   <?php if($vp1_file_crop9){
											echo 'style="display: none;"';
											}else {echo 'style="display: block;"';}?>>
                             <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
                            <br>
                            <input id="vp_file9" name="vp_file9" type="file" class="upload_file ">
                            <span class="help-block">Accepted formats: .pdf .jpg only Max file size 10Mb </span> </div>
                          <div class="form-group" id="vp_show_file9"   <?php if($vp1_file_crop9){
											echo 'style="display: block;"';
											}else {echo 'style="display: none;"';}?> >
                            <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label> <br>
                            <?php
if($vp1_file_crop9){
	foreach($vp1_file_crop9 as $vp1_file_crop9s){
		?>
                            <a  data-toggle="modal" href="#vp_viewfile9"   class="btn btn-link legitRipple"><i class="icon-file-eye position-left"></i> View </a> / <a href="#nogo" onclick="vp_deletefile9('<?php echo  $vp1_file_crop9s->vp1_filecrop_id;?>','<?php echo  $vp1_file_crop9s->vp1_filecrop_name;?>')" class="btn btn-link  legitRipple"><i class="icon-trash position-left"></i> Delete </a> 
                            
                            <!-- Vertical form modal -->
                            <div id="vp_viewfile9" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> View File </h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <div class="row col-md-12">
                                        <embed src="<?php echo base_url(); ?>vendorfile/<?php echo $vendor_vp1->vp1_path;?>/<?php echo  $vp1_file_crop9s->vp1_filecrop_name;?>"  class="img-responsive"  style="width:100%;height:500px;"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /vertical form modal -->
                            
                            <?php
		
		}
	}
?>
                            <div class="file-actions">
                              <div class="file-footer-buttons" style="margin-top:10px;"> </div>
                            </div>
                            <br>
                            <br>
                          </div>
                            

                            
                            
                            
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>ความคิดเห็นอื่นๆ เพิ่มเติม : </label>
                           
                            
                            <textarea name="vp3_remark" id="vp3_remark" rows="2" cols="4" placeholder="" class="form-control" onchange="vp3autosave(this.name,this.value)"><?php echo $vendor_vp3->vp3_remark;?></textarea>
                            
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12 text-right">
                          <label class="checkbox-inline">
                            <input type="checkbox" name="agreement" id="agreement" class="styled" required="required">
                            
                                                            
                            
                            
                            ข้าพเจ้าขอรับรองว่าข้อมูลดังกล่าวถูกต้อง </label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3 col-md-offset-9  text-right">
                         
                             <input type="text" name="vp3_upby" id="vp3_upby" class="form-control required"   onchange="vp3autosave(this.name,this.value)" value="<?php echo $vendor_vp3->vp3_upby;?>">
                            
                           
                          
                          <span class="help-block text-right">ลงชื่อผู้บันทึกข้อมูล</span> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /top component -->
              
              <div class="text-right"> 
                
                <!--<button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>-->
                
                <button type="submit" class="btn btn-primary">ส่งข้อมูลให้กับ TOA PAINT <i class="icon-arrow-right14 position-right"></i></button>
              </div>
            </form>
          </div>
        </div>
        <!-- /input group buttons --> 
        
        <!-- Primary modal -->
        <div id="modal_theme_primary" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">ข้อความจากระบบ</h6>
              </div>
              <div class="modal-body">
                <h6 class="text-semibold">แจ้งผู้ค้ารายใหม่</h6>
                <p> ขอความร่วมมือคู่ค้าทุกท่าน กรอกข้อมูล ตามเงื่อนไขของทาง ทีโอเอ เพื่อ ความสะดวกรวดเร็วในการสั่งซื้อสินค้า และ ส่งข้อมูล จากเรา </p>
                <hr>
                <p>หากตรวจพบว่าข้อมูลของท่านไม่ถูกต้อง บริษัทฯ ขอสงวนสิทธิ์ ในการสั่งซื้อสินค้าจาก ท่าน หรือ หากท่านไม่ได้รับความเป็นธรรมในการให้บริการจาก ฝ่ายจัดซื้อโปรติดต่อ Tel. 02 345 6789 หน่วยงาน ตรวจสอบ </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">รับทราบ</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /primary modal -->
        
        <?php $this->load->view('main/footer');?>
      </div>
      <!-- /content area --> 
      
    </div>
    <!-- /main content --> 
    
  </div>
  <!-- /page content --> 
  
</div>
<!-- /page container --> 

<!-- Vertical form modal -->
<div id="modal_form_vertical" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> เพิ่มสินค้า</h5>
      </div>
      <form action="#" method="post">
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>ชื่อผู้ผลิต</label>
                <input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
              </div>
              <div class="col-sm-6">
                <label>สินค้าที่จำหน่าย</label>
                <input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>ประเทศที่ผลิต</label>
                <input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
              </div>
              <div class="col-sm-6">
                <label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
                <input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label>Lead Time</label>
                <input type="text" placeholder="ระบุ Lead Time" class="form-control">
              </div>
              <div class="col-sm-4">
                <label>Minimum Order</label>
                <input type="text" placeholder="ระบุ Minimum Order" class="form-control">
              </div>
              <div class="col-sm-4">
                <label>Packing Size </label>
                <input type="text" placeholder="ระบุ Packing Size" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
                <input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
                <span class="help-block">eg: k.หนูแดง T 023456789</span> </div>
                <div class="col-sm-6">
                <label>สถานที่เก็บสินค้า</label>
                <input type="text" placeholder="ระบุสถานที่เก็บสินค้า" class="form-control">
                <span class="help-block">eg: โกดังบางนา สมุทรปาการ กม 19 </span> </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>SDS(ไทย/อังกฤษ) </label>
                <input type="file" name="styled_file" class="file-styled" required="required">
                <span class="help-block">Accepted formats: pdf. Max file size 2Mb </span> </div>
                <div class="col-sm-6">
                <label>TDS(ไทย/อังกฤษ) </label>
                <input type="file" name="styled_file" class="file-styled" required="required">
                <span class="help-block">Accepted formats: pdf. Max file size 2Mb </span> </div>
              
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
            <label>หมายเหตุ : </label>
            <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
          </div>
              </div>
              <div class="col-sm-6">
                <label>สถานะซื้อขายกับ TOA </label>
                <label class="checkbox-inline checkbox-switchery switchery-xs">
                  <input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
                  เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่ </label>
              </div>
            </div>
          </div>
          
          
          <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        --> 
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
          <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- /vertical form modal --> 

		         
                    
                   
                    
                    
                    
       





<script type="text/javascript">



	


// Custom tag class

    $('.tokenfield').tokenfield();
    $('.tagsinput-custom-tag-class').tagsinput({
        tagClass: function(item){
            return 'label bg-success';
        }
    });
	
	    // Display values
    $('.tags-input, [data-role="tagsinput"], .tagsinput-max-tags, .tagsinput-custom-tag-class').on('change', function(event) {
        var $element = $(event.target),
            $container = $element.parent().parent('.content-group');

        if (!$element.data('tagsinput'))
        return;

        var val = $element.val();
        if (val === null)
        val = "null";
    
        $('pre.val > code', $container).html( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
        $('pre.items > code', $container).html(JSON.stringify($element.tagsinput('items')));
        Prism.highlightAll();
    }).trigger('change');

checkreadOnly();

function uploadfilex(name,val){
	//e.preventDefault();  
	// document.forms[0].submit();
	
	//   var formData = new FormData($("#regis_st1")[0]);
	    var formData = new FormData(document.getElementById(name).files[0]);
	   
	   
	   
	   
	  // var fd = new FormData();
		//formData.append(name, val);
	   
	   
	   console.log(formData);
      
	  
	  

	  
	
	
	
	//document.getElementById('uniform-'+name).style.display = "none";
	
	
	
	//alert (val);
	}

function uploadfile(name,val){
	//e.preventDefault();  
	// document.forms[0].submit();
	
	   var formData = new FormData($("#regis_st1")[0]);
	//    var formData = new FormData(document.getElementById(name).files[0]);
	   
	   
	   
	   
	  // var fd = new FormData();
	//	formData.append("file1", file, file.name);
	   
	   
	 //  console.log(formData);
        $.ajax({ 
					 url:'<?php echo base_url(); ?>vendorregister/vendor_upload/',    
                     //url:"<?php //echo base_url(); ?>main/ajax_upload",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data: formData,  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
					// alert (data);
					// var resultAjax = JSON.parse(data);
				//	console.log(data);
					 
						
		document.getElementById('div_vp1_filecrop_name').style.display = "none";
		document.getElementById('h-default-basic').style.display = "block"; 				
		var $pb = $('#h-default-basic .progress-bar');
        $pb.attr('data-transitiongoal', $pb.attr('data-transitiongoal-backup'));
        $pb.progressbar({display_text: 'fill'});
		//$('#uploadst').html("Upload Success"); 
		
		 window.setTimeout(function () {
            //$(dark_2).unblock();
			document.getElementById('h-default-basic').style.display = "none";
			document.getElementById('div_vp1_filecrop_name_showfile').style.display = "block";

        }, 3000);
		
						 
                     }  
                });  
	  
	
	}

function checkreadOnly(){
	if(document.getElementById('vp1_same_add1').checked == true){
	//alert ('checked');
	document.getElementById("vp1_add2").readOnly = true;
	document.getElementById("vp1_add2_province").readOnly = true;
	document.getElementById("vp1_add2_amphur").readOnly = true;
	document.getElementById("vp1_add2_thumbol").readOnly = true;
	document.getElementById("vp1_add2_post").readOnly = true;
	//readonly
	}else {
	document.getElementById("vp1_add2").readOnly = false;
	document.getElementById("vp1_add2_province").readOnly = false;
	document.getElementById("vp1_add2_amphur").readOnly = false;
	document.getElementById("vp1_add2_thumbol").readOnly = false;
	document.getElementById("vp1_add2_post").readOnly = false;
		}
	}

function checkbox2(name,val){
	
	if(document.getElementById(name).checked == true){
		autosave(name,'1');
		}else {
			autosave(name,'0');
			}

	}
	
function checkbox(name,val){
	
	
	
	if(document.getElementById(name).checked == true){
		
		

document.getElementById("vp1_add2").value = document.getElementById("vp1_add1").value;
document.getElementById("vp1_add2_province").value = document.getElementById("vp1_add1_province").value;
document.getElementById("vp1_add2_amphur").value = document.getElementById("vp1_add1_amphur").value;
document.getElementById("vp1_add2_thumbol").value = document.getElementById("vp1_add1_thumbol").value;
document.getElementById("vp1_add2_post").value = document.getElementById("vp1_add1_post").value;

		
			 
		
		 // autosave('vp1_add2',document.getElementById("vp1_add1").value);
		  
		autosave2('vp1_add2',document.getElementById("vp1_add2").value);  
		autosave2('vp1_add2_province',document.getElementById("vp1_add2_province").value);
		autosave2('vp1_add2_amphur',document.getElementById("vp1_add2_amphur").value);  
		autosave2('vp1_add2_thumbol',document.getElementById("vp1_add2_thumbol").value);  
		autosave2('vp1_add2_post',document.getElementById("vp1_add2_post").value); 
		autosave(name,'1');
		checkreadOnly();
		
		}else if(document.getElementById(name).checked == false) {
			
			


			autosave(name,'0');
			checkreadOnly();
			}
	
	
	}

function autosave(name,val) {
	//alert(name + val);
	$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vendor_profile1/",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: "<?php echo $vendor_vp1->vp1_id;?>",
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//var resultAjax = JSON.parse(data);
				//alert (resultAjax.user_st);
				
				//console.log(data);
				//exit;
				new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
			delay: 1500,
            icon: 'icon-checkmark3',
            type: 'success'
        });
			
				
			}
		});	
	
	
		
//noti2(this.name,this.value)
	
	
    //alert("The input value has changed. The new value is: " + val);
} 

function vp2autosave(name,val) {
	//alert(name + val);
	$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vendor_profile2/",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: "<?php echo $vendor_vp1->vp1_id;?>",
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//var resultAjax = JSON.parse(data);
				//alert (resultAjax.user_st);
				
				//console.log(data);
				//exit;
			/*	new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
			delay: 1500,
            icon: 'icon-checkmark3',
            type: 'success'
        });*/
			
				
			}
		});	
	
	
		
//noti2(this.name,this.value)
	
	
    //alert("The input value has changed. The new value is: " + val);
} 

function vp3autosave(name,val) {
	//alert(name + val);
	$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vendor_profile3/",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: "<?php echo $vendor_vp1->vp1_id;?>",
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//var resultAjax = JSON.parse(data);
				//alert (resultAjax.user_st);
				
				//console.log(data);
				//exit;
				new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
			delay: 1500,
            icon: 'icon-checkmark3',
            type: 'success'
        });
			
				
			}
		});	
	
	
		
//noti2(this.name,this.value)
	
	
    //alert("The input value has changed. The new value is: " + val);
}

function autosave2(name,val) {
	//alert(name + val);
	$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vendor_profile1/",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: "<?php echo $vendor_vp1->vp1_id;?>",
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//var resultAjax = JSON.parse(data);
				//alert (resultAjax.user_st);
				
				//console.log(data);
				//exit;
				
			
				
			}
		});	
	
	
		
//noti2(this.name,this.value)
	
	
    //alert("The input value has changed. The new value is: " + val);
} 


  function vp_deletefile1(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงไฟล์ดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vp1_fdelete1",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}
function vp_deletefile2(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงไฟล์ดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vp1_fdelete2",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}
function vp_deletefile3(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงไฟล์ดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vp1_fdelete3",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}

function vp_deletefile4(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงไฟล์ดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vp1_fdelete4",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}


function vp_deletelocator(thisid){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/deletelocator",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//alert(data);
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}

function vp_deleteproduct(thisid){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/deleteproduct",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//alert(data);
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}



function vp_deleteseller(thisid){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/sdeletelocator",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//alert(data);
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}
 

	

 // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();




    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });


	

    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });


 var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
			
			/*vp1_tel: {
                number: true
            },*/
            numbers: {
                number: true
            },
			
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
				
				
				
		
				swal({
                    title: "เราได้รับข้อมูลของท่านเรียบร้อยแล้ว | Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
				$.ajax({
					
			url: "<?php echo base_url(); ?>vendorregister/vendorsend",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
			 
			 		username : "<?php echo $this->session->userdata('vendor_username');?>",
										
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				//alert(data);
			
			
			
			 var resultAjax = JSON.parse(data);
			 
			 if(resultAjax.st == 'true'){
				 
				 

				 
				  $.blockUI({ 
           
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				  setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='<?php echo base_url(); ?>vendorregister';
  }  , 1000 );
				
				
				
				
				}
            }
        });
		
			
				
				
				
				 
				 } else {
					 
					 //alert('fail');
					 swal({
            title: "Oop.. File type  incorrect...",
            text: "ขออภัยมีข้อมูลบางอย่างไม่ถูกต้องโปรดตรวจสอบข้อมูลให้เรียบร้อยอีกครั้ง หากไม่แน่ใจโปรดติดต่อเจ้าหน้าที่เพื่อ ตรวจสอบปัญหา | Sorry This file type incorrect.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					 }
			
  
  	
				
			
				
			}
		});	
				
               
				$.blockUI({ 
           
           timeout: 10000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 
				
				
				
				
				}
            }
        });
				 
				
				
					
					
				

				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		
		
            }
    });
	


 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


   

	


	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });





		


</script>
<?php $timestamp = time();?>
<script src="<?php echo base_url(); ?>uploadify/jquery.uploadifive.min.js" type="text/javascript"></script> 
<script type="text/javascript">


 $(function() {
			$('#file_upload').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file1').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload1',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file2').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload2',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file3').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload3',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file4').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload4',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file5').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload5',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file6').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload6',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file7').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload7',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file8').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload8',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
			$('#vp_file9').uploadifive({
				
				'multi'    : false,
				 
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					
				},
				'uploadScript' : '<?php echo base_url(); ?>vendorregister/vp1_fileupload9',
			'uploadLimit' : 1,
		 'onUploadComplete' : function(file) {
						  
						setTimeout(function(){
   location.reload();
  },1000)
						
					
		
        }
		 
				
			});
		});

 
 </script>
</body>
</html>