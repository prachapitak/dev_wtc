<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title;?></title>
<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	
    
    
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/visualization/d3/d3.min.js"></script>
    
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/visualization/echarts/echarts.js"></script>
    

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->
    
    
    
    <script type="text/javascript">
	
    $(function () {

    // Set paths
    // ------------------------------
	
	
	
	

    require.config({
        paths: {
            echarts: '<?php echo base_url(); ?>assets/js/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec, limitless) {


        
            var stacked_clustered_bars = ec.init(document.getElementById('stacked_clustered_bars'), limitless);
           

         
var weatherIcons = {
    'Sunny': './data/asset/img/weather/sunny_128.png',
    'Cloudy': './data/asset/img/weather/cloudy_128.png',
    'Showers': './data/asset/img/weather/showers_128.png'
};

var seriesLabel = {
    normal: {
        show: true,
        textBorderColor: '#333',
        textBorderWidth: 2
    }
}

stacked_clustered_bars_options = {
  
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: ['Goal', 'Actual', 'Diff']
    },
    grid: {
        left: 100
    },
    xAxis: {
        type: 'value',
        name: '',
        axisLabel: {
            formatter: '{value}'
        }
    },
    yAxis: {
        type: 'category',
        inverse: true,
        data: ['Line1', 'Line2', 'Demand'],
        
    },
    series: [
        {
            name: 'Goal',
            type: 'bar',
            data: [20000, 20000, 40000],
            label: seriesLabel,
			itemStyle: {
                            normal: {
                                color: '#64B5F6',
                                label: {
                                    show: true,
                                   // formatter: function(p) {return p.value > 0 ? (p.value +'+'):'';}
                                }
                            },
                            emphasis: {
                                color: '#64B5F6',
                                label: {
                                    show: false
                                }
                            }
                        },
            /*markPoint: {
                symbolSize: 1,
                symbolOffset: [0, '50%'],
                label: {
                   normal: {
                        formatter: '{a|{a}\n}{b|{b} }{c|{c}}',
                        backgroundColor: 'rgb(242,242,242)',
                        borderColor: '#aaa',
                        borderWidth: 1,
                        borderRadius: 4,
                        padding: [4, 10],
                        lineHeight: 26,
                        // shadowBlur: 5,
                        // shadowColor: '#000',
                        // shadowOffsetX: 0,
                        // shadowOffsetY: 1,
                        position: 'right',
                        distance: 20,
                        rich: {
                            a: {
                                align: 'center',
                                color: '#fff',
                                fontSize: 18,
                                textShadowBlur: 2,
                                textShadowColor: '#000',
                                textShadowOffsetX: 0,
                                textShadowOffsetY: 1,
                                textBorderColor: '#333',
                                textBorderWidth: 2
                            },
                            b: {
                                 color: '#333'
                            },
                            c: {
                                color: '#ff8811',
                                textBorderColor: '#000',
                                textBorderWidth: 1,
                                fontSize: 22
                            }
                        }
                   }
                },
                data: [
                    {type: 'max', name: 'max days: '},
                    {type: 'min', name: 'min days: '}
                ]
            }*/
        },
        {
            name: 'Actual',
            type: 'bar',
            label: seriesLabel,
			itemStyle: {
                            normal: {
                                color: 'green',
                                label: {
                                    show: true,
                                  //  formatter: function(p) {return p.value > 0 ? (p.value +'+'):'';}
                                }
                            },
                            emphasis: {
                                color: 'green',
                                label: {
                                    show: false
                                }
                            }
                        },
            data: [18000, 17872, 33822]
        },
        {
            name: 'Diff',
            type: 'bar',
            label: seriesLabel,
			itemStyle: {
                            normal: {
                                color: 'red',
                                label: {
                                    show: true,
                                  //  formatter: function(p) {return p.value > 0 ? (p.value +'+'):'';}
                                }
                            },
                            emphasis: {
                                color: 'red',
                                label: {
                                    show: false
                                }
                            }
                        },
            data: [2000, 2128, 6038]
        }
    ]
};



 stacked_clustered_bars_optionsx = {

                // Setup grid
                grid: {
                    x: 45,
                    x2: 45,
                    y: 65,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                // Add legends
                legend: {
                    data: [
                        'Version 1.7 - 2k data','Version 1.7 - 2w data','Version 1.7 - 20w data','',
                        'Version 2.0 - 2k data','Version 2.0 - 2w data','Version 2.0 - 20w data'
                    ]
                },

                // Enable drag recalculate
              //  calculable: true,

                // Vertical axis
                yAxis: [
                    {
                        type: 'category',
                        data: ['Line','Bar','Scatter']
                    },
                    {
                        type: 'category',
                        axisLine: {show: false},
                        axisTick: {show: false},
                        axisLabel: {show: false},
                        splitArea: {show: false},
                        splitLine: {show: false},
                        data: ['Line','Bar','Scatter']
                    }
                ],

                // Horizontal axis
                xAxis: [{
                    type: 'value',
                    axisLabel: {formatter: '{value} ms'}
                }],

                // Add series
                series: [
                    {
                        name: 'Version 2.0 - 2k data',
                        type: 'bar',
                        yAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#F44336',
                                label: {
                                    show: true,
                                    textStyle:{
                                        color: '#fff'
                                    }
                                }
                            },
                            emphasis: {
                                color: '#F44336',
                                label: {
                                    show: true
                                }
                            }
                        },
                        data: [247, 187, 95, 175]
                    },
                    {
                        name: 'Version 2.0 - 2w data',
                        type: 'bar',
                        yAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#4CAF50',
                                label: {
                                    show: true,
                                    textStyle: {
                                        color: '#fff'
                                    }
                                }
                            },
                            emphasis: {
                                color: '#4CAF50',
                                label: {
                                    show: true
                                }
                            }
                        },
                        data: [488, 415, 405, 340]
                    },
                    {
                        name: 'Version 2.0 - 20w data',
                        type: 'bar',
                        yAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#2196F3',
                                label: {
                                    show: true,
                                    textStyle: {
                                        color: '#fff'
                                    }
                                }
                            },
                            emphasis: {
                                color: '#2196F3',
                                label: {
                                    show: true
                                }
                            }
                        },
                        data: [906, 911, 908, 778]
                    },

                    {
                        name: 'Version 1.7 - 2k data',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#E57373',
                                label: {
                                    show: true
                                }
                            },
                            emphasis: {
                                color: '#E57373',
                                label: {
                                    show: true
                                }
                            }
                        },
                        data: [680, 819, 564, 724]
                    },
                    {
                        name: 'Version 1.7 - 2w data',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#81C784',
                                label: {
                                    show: true
                                }
                            },
                            emphasis: {
                                color: '#81C784',
                                label: {
                                    show: true
                                }
                            }
                        },
                        data: [1212, 2035, 1620, 955]
                    },
                    {
                        name: 'Version 1.7 - 20w data',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#64B5F6',
                                label: {
                                    show: true,
                                    formatter: function(p) {return p.value > 0 ? (p.value +'+'):'';}
                                }
                            },
                            emphasis: {
                                color: '#64B5F6',
                                label: {
                                    show: false
                                }
                            }
                        },
                        data: [2200, 3000, 2500, 3000]
                    }
                ]
            };



            stacked_clustered_bars.setOption(stacked_clustered_bars_options);
         

            window.onresize = function () {
                setTimeout(function (){
                  
                    stacked_clustered_bars.resize();
           
                }, 200);
            }
        }
    );
	
	
	
	
	//generateBarChart("#goal-bars", 24, 40, true, "elastic", 1200, 50, "#5C6BC0", "goal");
   
   
    function generateBarChart(element, barQty, height, animate, easing, duration, delay, color, tooltip) {


        // Basic setup
        // ------------------------------

        // Add data set
        var bardata = [];
        for (var i=0; i < barQty; i++) {
            bardata.push(Math.round(Math.random()*10) + 10)
        }

        // Main variables
        var d3Container = d3.select(element),
            width = d3Container.node().getBoundingClientRect().width;
        


        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.scale.ordinal()
            .rangeBands([0, width], 0.3)

        // Vertical
        var y = d3.scale.linear()
            .range([0, height]);



        // Set input domains
        // ------------------------------

        // Horizontal
        x.domain(d3.range(0, bardata.length))

        // Vertical
        y.domain([0, d3.max(bardata)])



        // Create chart
        // ------------------------------

        // Add svg element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
            .attr('width', width)
            .attr('height', height)
            .append('g');



        //
        // Append chart elements
        //

        // Bars
        var bars = svg.selectAll('rect')
            .data(bardata)
            .enter()
            .append('rect')
                .attr('class', 'd3-random-bars')
                .attr('width', x.rangeBand())
                .attr('x', function(d,i) {
                    return x(i);
                })
                .style('fill', color);



        // Tooltip
        // ------------------------------

        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0]);

        // Show and hide
        if(tooltip == "hours" || tooltip == "goal" || tooltip == "members") {
            bars.call(tip)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);
        }

        // Daily meetings tooltip content
        if(tooltip == "hours") {
            tip.html(function (d, i) {
                return "<div class='text-center'>" +
                        "<h6 class='no-margin'>" + d + "</h6>" +
                        "<span class='text-size-small'>meetings</span>" +
                        "<div class='text-size-small'>" + i + ":00" + "</div>" +
                    "</div>"
            });
        }

        // Statements tooltip content
        if(tooltip == "goal") {
            tip.html(function (d, i) {
                return "<div class='text-center'>" +
                        "<h6 class='no-margin'>" + d + "</h6>" +
                        "<span class='text-size-small'>statements</span>" +
                        "<div class='text-size-small'>" + i + ":00" + "</div>" +
                    "</div>"
            });
        }

        // Online members tooltip content
        if(tooltip == "members") {
            tip.html(function (d, i) {
                return "<div class='text-center'>" +
                        "<h6 class='no-margin'>" + d + "0" + "</h6>" +
                        "<span class='text-size-small'>members</span>" +
                        "<div class='text-size-small'>" + i + ":00" + "</div>" +
                    "</div>"
            });
        }



        // Bar loading animation
        // ------------------------------

        // Choose between animated or static
        if(animate) {
            withAnimation();
        } else {
            withoutAnimation();
        }

        // Animate on load
        function withAnimation() {
            bars
                .attr('height', 0)
                .attr('y', height)
                .transition()
                    .attr('height', function(d) {
                        return y(d);
                    })
                    .attr('y', function(d) {
                        return height - y(d);
                    })
                    .delay(function(d, i) {
                        return i * delay;
                    })
                    .duration(duration)
                    .ease(easing);
        }

        // Load without animateion
        function withoutAnimation() {
            bars
                .attr('height', function(d) {
                    return y(d);
                })
                .attr('y', function(d) {
                    return height - y(d);
                })
        }



        // Resize chart
        // ------------------------------

        // Call function on window resize
        $(window).on('resize', barsResize);

        // Call function on sidebar width change
        $(document).on('click', '.sidebar-control', barsResize);

        // Resize function
        // 
        // Since D3 doesn't support SVG resize by default,
        // we need to manually specify parts of the graph that need to 
        // be updated on window resize
        function barsResize() {

            // Layout variables
            width = d3Container.node().getBoundingClientRect().width;


            // Layout
            // -------------------------

            // Main svg width
            container.attr("width", width);

            // Width of appended group
            svg.attr("width", width);

            // Horizontal range
            x.rangeBands([0, width], 0.3);


            // Chart elements
            // -------------------------

            // Bars
            svg.selectAll('.d3-random-bars')
                .attr('width', x.rangeBand())
                .attr('x', function(d,i) {
                    return x(i);
                });
        }
    }



   
    progressCounter('#goal-progress', 38, 8, "#008000", 0.87, "icon-trophy3 text-green-800", 'Rank #1', '')
	
	progressCounter('#goal-progress2', 38, 8, "#008000", 0.87, "icon-trophy3 text-green-800", 'Rank #1', '')
	
	progressCounter('#goal-progress3', 38, 8, "#008000", 0.87, "icon-trophy3 text-green-800", 'Rank #1', '')
	
	progressCounter('#goal-progress4', 38, 8, "#008000", 0.87, "icon-trophy3 text-green-800", 'Rank #1', '')

    // Chart setup
    function progressCounter(element, radius, border, color, end, iconClass, textTitle, textAverage) {


        // Basic setup
        // ------------------------------

        // Main variables
        var d3Container = d3.select(element),
            startPercent = 0,
            iconSize = 32,
            endPercent = end,
            twoPi = Math.PI * 2,
            formatPercent = d3.format('.0%'),
            boxSize = radius * 2;

        // Values count
        var count = Math.abs((endPercent - startPercent) / 0.01);

        // Values step
        var step = endPercent < startPercent ? -0.01 : 0.01;



        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
            .attr('width', boxSize)
            .attr('height', boxSize)
            .append('g')
                .attr('transform', 'translate(' + (boxSize / 2) + ',' + (boxSize / 2) + ')');



        // Construct chart layout
        // ------------------------------

        // Arc
        var arc = d3.svg.arc()
            .startAngle(0)
            .innerRadius(radius)
            .outerRadius(radius - border);



        //
        // Append chart elements
        //

        // Paths
        // ------------------------------

        // Background path
        svg.append('path')
            .attr('class', 'd3-progress-background')
            .attr('d', arc.endAngle(twoPi))
            .style('fill', '#eee');

        // Foreground path
        var foreground = svg.append('path')
            .attr('class', 'd3-progress-foreground')
            .attr('filter', 'url(#blur)')
            .style('fill', color)
            .style('stroke', color);

        // Front path
        var front = svg.append('path')
            .attr('class', 'd3-progress-front')
            .style('fill', color)
            .style('fill-opacity', 1);



        // Text
        // ------------------------------

        // Percentage text value
        var numberText = d3.select(element)
             
			.append('h1')
			.attr('style', 'color: green;font-weight: 800;margin-top: 0px;margin-bottom: 0px;');
               // .attr('class', 'mt-15 mb-5')

        // Icon
        d3.select(element)
            .append("i")
                .attr("class", iconClass + " counter-icon")
                .attr('style', 'top: ' + ((boxSize - iconSize) / 2) + 'px');

        // Title
        d3.select(element)
		.append('h3')
			 .attr('style', 'color: green;font-weight: 800;margin-top: 0px;margin-bottom: 0px;')
			//.attr('class', 'text-size-small')
            .append('div')
                .text(textTitle);

        // Subtitle
        d3.select(element)
            .append('div')
                .attr('class', 'text-size-small text-muted')
                .text(textAverage);



        // Animation
        // ------------------------------

        // Animate path
        function updateProgress(progress) {
            foreground.attr('d', arc.endAngle(twoPi * progress));
            front.attr('d', arc.endAngle(twoPi * progress));
            numberText.text(formatPercent(progress));
        }

        // Animate text
        var progress = startPercent;
        (function loops() {
            updateProgress(progress);
            if (count > 0) {
                count--;
                progress += step;
                setTimeout(loops, 10);
            }
        })();
    }

	
	
	
	
});
    
    </script>
	
    
 
</head>

<body>



	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

		

			<!-- Main content -->
			<div class="content-wrapper">

				

				
                
                <!-- Content area -->
				<div class="content">

					

					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-8">

					<!-- Stacked clustered bar chart -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Productivity Content  : <?php echo date("d-m-Y H:i:s")?></h5>
							
						</div>

						<div class="panel-body">
							<div class="chart-container">
								<div class="chart has-fixed-height" id="stacked_clustered_bars" style="height: 500px"></div>
							</div>
						</div>
					</div>
					<!-- /stacked clustered bar chart -->
					

						
						</div>

						<div class="col-lg-4">
                        
                      <div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Shift 1 Information  : <?php echo date("d-m-Y H:i:s")?></h5>
							
						</div>

						<div class="panel-body">
						
                    <div class="row">
                       <div class="col-md-6">

									<!-- Productivity goal -->
									<div class="panel text-center ">
										<div class="panel-body">
											

											<!-- Progress counter -->
											<div class="content-group-sm svg-center position-relative" id="goal-progress"></div>
											<!-- /progress counter -->

											<!-- Bars -->
											<!--<div id="goal-bars"></div>-->
											<!-- /bars -->

										</div>
									</div>
									<!-- /productivity goal -->

								</div>
                                
                                <div class="col-md-6">

									<!-- Available hours -->
									<div class="panel text-center">
										<div class="panel-body">
											

						            
                                        
                                        <span class="text-success-600" style="font-size:32px; font-weight:800; color:green !important;"><i class="icon-stats-growth2 position-left" style="font-size:40px; font-weight:800; color:green;"></i> 2.43%</span>
                                           <h1 class="text-success-600" style=" font-weight:800; color:green !important;">Faster
                                           <div>Greate</div>
                                           </h1>

										</div>
									</div>
									<!-- /available hours -->

								</div>
                    </div>
                    
                    <hr>
                     <div class="row">
                       <div class="col-md-6">

									<!-- Available hours -->
									<div class="panel text-center">
										<div class="panel-body">
											
<div class="content-group-sm svg-center position-relative" id="goal-progress3"></div>
						                

										</div>
									</div>
									<!-- /available hours -->

								</div>
                                
                                <div class="col-md-6">

									<!-- Available hours -->
									<div class="panel text-center">
										<div class="panel-body">
											
<div class="content-group-sm svg-center position-relative" id="goal-progress4"></div>
						                

										</div>
									</div>
									<!-- /available hours -->

								</div>
                    </div>
                                
                             
                        
                        
                        
						</div>
					</div>
							


						</div>
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2018. <a href="#">WTC</a> 
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
