<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?>
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_extension_fixed_columns.js"></script>
    
    
   



	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/daterangepicker.js"></script>


    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
	
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	


	
	
   
	
    
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>


	<!-- /theme JS files -->



</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">View PO Detail   </span> -  WTC  <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php //$this->load->view('dashboard/wtc/headnoti');?>

						
					</div>

					
                    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                            
							<li><a href="<?php echo base_url(); ?>dashboard/pomanage/">PO Management </a></li>
                            
							<li class="active">View PO  </li>
						</ul>

						
					</div>
                    
                    
                    
				</div>
				<!-- /page header -->

<?php $this->load->view('dashboard/wtc/noti');?>
					
				
				
                                    

				<!-- Content area -->
				<div class="content">
                
            
						
                        
                          <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">PO Detail : รายละเอียด PO </h5>
                            
                            <div class="heading-elements">
								<ul class="icons-list">
			                		
			                		<li><a data-action="reload"></a></li>
			                		
			                	</ul>
		                	</div>
                       
                            
						</div>
                        
                        
                        
    
                        
                        
                        
                        
                                        <div style="color:#F00; font-size:16px;" id="displayerror" > </div> 
               
                

					<!-- Form with validation -->
					<form action="#" id="form-agreememt">
                    
                    <div class="panel-body">
                                
                               <fieldset class="content-group">
								



									<div class="form-group"> 
                                   
									<div class="col-md-6">
                                    <label class="control-label col-lg-2"><strong>Customer </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="CustomerId" id="CustomerId" class="form-control" placeholder=""  required="required" value="<?php echo $get_customer->Company_en?>" readonly >
                                            
                                            
										</div>
                                    </div>
                                    
                                    	<div class="col-md-6">
                                    <label class="control-label col-lg-2"><strong>PO No. </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="PONumber" id="PONumber" class="form-control" placeholder=""  required="required" value="<?php echo $get_podetail->PONumber;?>" readonly >
                                            
                                            
										</div>
                                    </div>
                                        
									</div>
                                    
                                 
                                 <div class="form-group"> 
                                   
                                   <div class="col-md-6">
                                    <label class="control-label col-lg-2">&nbsp;</label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="customer_enduser_id" id="customer_enduser_id" class="form-control" placeholder="" onchange="noti2(this.name,this.value)" required="required" value="<?php echo $get_customer->Company_th?>" readonly >
                                            
                                            
										</div>
                                    </div>
                                   
									
                                    
                                    	<div class="col-md-6">
                                     <label class="control-label col-lg-2"><strong> Issue date. </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="customer_enduser_id" id="customer_enduser_id" class="form-control" placeholder="" onchange="noti2(this.name,this.value)" required="required" value="<?php echo $get_podetail->IssuedDate;?>" readonly >
                                            
                                            
										</div>
                                    </div>
                                        
									</div>
                              
                              </fieldset>
							
                            
                            
                      
                            
                            <table class="table datatable-fixed-left" width="100%">
							<thead>
						        <tr>
                                 				
                                                <th >SlNUMBER</th>
                                                <th >CATEGOTY</th>
												
												<th >BRAND/MODEL</th>
                                             
												<th >Part Number</th>
                                                <th >Serial Number</th>
                                                <th >Service Tag</th>
                                                <th >Cost</th>
                                                <th >Warranty Expire</th>
                                                <th >Invoice </th>
                                                <th >Do Number</th>
                                                 
											 
						            <th class="text-center">Actions</th>
						        </tr>
						    </thead>
						    <tbody>
                            
                            <?php
                           // print_r($get_customer);
							
							if($get_po_item){
								foreach($get_po_item as $get_po_items){
									
									//Status
									//echo $get_customers->Status;
									?>
                                    
                                  <tr>
						            <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class=""><a href="#" data-toggle="modal" data-target="#m_edit_item<?php echo $get_po_items->po_item_id?>"><?php echo $get_po_items->sl_number;?></a></div>
														
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
                                               
												
                                                 
                                                
													
													<div class="media-left">
														<div class="media-left">
														<div class=""><?php echo $get_po_items->category_en;?> <br><?php echo $get_po_items->category_th;?></div>
													</div>
														
													</div>
												</td>
												<td>
                                                
                                                
                                              
                                                
                                                
                                                <div class="media-left">
														
														<div class=""><?php echo $get_po_items->brand_en;?> | <?php echo $get_po_items->model;?> <br> <?php echo $get_po_items->brand_th;?> | <?php echo $get_po_items->model;?></div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                               
                                               
                                              
                                               
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->part_number;?> </div>
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                
                                                
                                                
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->serial_number;?></div>
													</div>
                                                
                                            </td>
                                            
                                            
                                            
                                            
												
                                               
											<td>
                                                
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->service_tag;?></div>
														
                                                        
														
													</div>
                                               </td>
                                            
                                             
                                            
                                            
                                            <td>
                                                
                                                
                                                
                                             
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->cost;?></div>
														
                                                        
														
													</div>
                                               </td>
                                               
                                               <td>
                                                
                                                
                                                
                                             
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->warranty_end;?></div>
														
                                                        
														
													</div>
                                               </td>
                                               <td>
                                                
                                                
                                                
                                             
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->invoice_number;?> <br> <?php echo $get_po_items->invoice_issue_date;?></div>
														
                                                        
														
													</div>
                                               </td>
                                            
                                            
                                            
                                            <td>
                                                
                                                
                                                
                                             
                                                
                                                <div class="media-left">
														<div class=""><?php echo $get_po_items->do_number;?> <br> <?php echo $get_po_items->do_issue_date;?></div>
														
                                                        
														
													</div>
                                               </td>
                                            
						            <td class="text-center">
                                    
                                    <ul class="icons-list">
												<li>
                                               
                                                
                                                <a href="#" data-toggle="modal" data-target="#m_edit_item<?php echo $get_po_items->po_item_id?>"><i class="icon-eye"></i></a></li>
												
                                                
                           
                       
												
											</ul>
                                    
                                    
													
												</td>
						        </tr>  
                                    <?php
									
									}
								
								}
							
							?>
                            
                            
                            
                            
                          
                             
                                
                             
                            
                            
                              
                            </tbody>
                            
						</table>
                            
                            		  
								</div>
                    
                  
                    
                    
						
					</form>
					<!-- /form with validation -->

						
                        
                        
                        
                        
                        
					</div>
					<!-- /basic datatable -->




                    
                    
			
                    

					
                    
		        

		         



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
    <!-- Vertical form modal -->
					<div id="m_add_item" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> Add New Item</h5>
								</div>

								   <form class="form-validation form-validate-jquery" id="regis_st1" action="#"  method="post" enctype="multipart/form-data" >
									<div class="modal-body">
                                    
                                    
                                


									<div class="form-group">
                                    
                                    <div class="row">
												

												<div class="col-sm-6">
                                                <label>SlNUMBER</label>
										<input type="text" placeholder="SlNUMBER" class="form-control required" id="SLNumber" name="SLNumber" readonly>
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                
                                                
                                                 <label>PO No.</label>
                                        
                                          <input type="text"  name="PONumber" id="PONumber" class="form-control" placeholder=""  value="<?php echo $get_podetail->PONumber;?>" readonly >
                                          
                                            <input type="hidden"   name="POID" id="POID" class="form-control" placeholder=""  value="<?php echo $get_podetail->ID;?>" readonly >
                                                
                                                </div>
                                                
                                                </div>
                                                
                                    
										
                                        
                                        
                                      
                                        
                                        
									</div>
                                    
                                    
                                    
										<div class="form-group">
											<div class="row">
												

												<div class="col-sm-6">
													<label>CATEGOTY</label>
													<select class="bootstrap-select required" data-width="100%" id="ProductCategoryId" name="ProductCategoryId" readonly>
                                                    <?php
                                                    if($get_categoty){
														foreach($get_categoty as $get_categotys){
															?>
                                                            <option value="<?php echo $get_categotys->id;?>"><?php echo $get_categotys->category_en;?> | <?php echo $get_categotys->category_th;?></option>
                                                            
                                                            <?php
															
															}
														
														}
													?>
                                                    
											
											
										</select>
												</div>
                                                
                                                <div class="col-sm-6">
													  <label>Brand | Model</label>
										<select class="bootstrap-select required" data-live-search="true" data-width="100%" id="ModelID" name="ModelID" readonly>
                                        
                                        
                                        
                                        <?php
										
										//print_r($brand);
                                        if($get_brand){
											foreach($get_brand as $get_brands){
												
												
												?>
                                                
                                                
                                                <optgroup label="<?php echo $get_brands->brand_en;?>">
                                                <?php
                                                $model = $this->Query_Db->result($this->db->dbprefix.'product_model','brand_id ='.$get_brands->id.'');
												if($model ){
													foreach($model as $models){
														
														echo '<option value="'.$models->id.'">'.$get_brands->brand_en.' | '.$models->model.'</option>';
														
														}
													
													
													}
												
												?>
                                                
                                                
                                                </optgroup>
                                                <?php
												
												}
											}
										?>
											
										</select>
												</div>
                                                
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Part Number</label>
													<input type="text" placeholder="Part Number"  class="form-control required" id="PartNumber" name="PartNumber" readonly>
												</div>

												<div class="col-sm-6">
													<label>Serial Number</label>
													<input type="text" placeholder="Serial Number" class="form-control required" id="SerialNumber" name="SerialNumber" readonly >
												</div>
											</div>
										</div>

										
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Service Tag</label>
													<input type="text" placeholder="Service Tag" class="form-control required" id="Tag" name="Tag" readonly>
												</div>

												<div class="col-sm-6">
													<label>Cost</label>
													<input type="text" placeholder="Cost" class="form-control required" id="Cost" name="Cost" readonly>
												</div>
											</div>
										</div>
                                        
                                        
                                        
                                           <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Warranty Start</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="WarrantyStart" name="WarrantyStart" readonly>
										</div>
												</div>

												<div class="col-sm-6">
													<label>Warranty Month</label>
                                                    
                                                    <input type="text" placeholder="Warranty Month" class="form-control required" id="WarrantyMonth" name="WarrantyMonth" readonly>

																						</div>
											</div>
										</div>

										
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Invoice No.</label>
													<input type="text" placeholder="Invoice No." class="form-control required"  id="INVNumber" name="INVNumber" readonly>
												</div>

												<div class="col-sm-6">
													<label>Invoice Date</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="INVDate" name="INVDate" readonly>
										</div>
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Do No.</label>
													<input type="text" placeholder="Do No. " class="form-control required" id="DONumber" name="DONumber" readonly>
												</div>

												<div class="col-sm-6">
													<label>Do Date</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="DODate" name="DODate" readonly>
										</div>
												</div>
											</div>
										</div>
                                        
                                    
                                      
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->

<?php


if($get_po_item){
								foreach($get_po_item as $get_po_items){
								
								
								$get_t_po_item = $this->Query_Db->record($this->db->dbprefix.'po_item','id ='.$get_po_items->po_item_id.'');
								?>
                                 <!-- Vertical form modal -->
					<div id="m_edit_item<?php echo $get_po_items->po_item_id?>" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> Edit PO Item</h5>
								</div>

								   <form class="form-validation form-validate-jquery" id="f_edit_item<?php echo $get_po_items->po_item_id;?>" action="#"  method="post" enctype="multipart/form-data" >
									<div class="modal-body">
                                    
                                    
                                

									<div class="form-group">
                                    
                                    <div class="row">
												

												<div class="col-sm-6">
                                                <label>SlNUMBER</label>
										<input type="text" placeholder="SlNUMBER" class="form-control required" id="SLNumber" name="SLNumber" value="<?php echo $get_po_items->sl_number;?>" readonly>
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                
                                                
                                                 <label>PO No.</label>
                                        
                                          <input type="text"  name="PONumber" id="PONumber" class="form-control" placeholder=""  value="<?php echo $get_podetail->PONumber;?>" readonly >
                                          
                                            <input type="hidden"   name="POID" id="POID" class="form-control" placeholder=""  value="<?php echo $get_podetail->ID;?>" readonly >
                                            
                                             <input type="hidden"   name="ID" id="ID" class="form-control" placeholder=""  value="<?php echo $get_po_items->po_item_id?>" readonly >
                                                
                                                </div>
                                                
                                                </div>
                                                
                                    
										
                                        
                                        
                                      
                                        
                                        
									</div>
                                    
                                    
                                    
										<div class="form-group">
											<div class="row">
												

												<div class="col-sm-6">
													<label>CATEGOTY</label>
													<select class="bootstrap-select required" data-width="100%" id="ProductCategoryId" name="ProductCategoryId" disabled >
                                                    <?php
                                                    if($get_categoty){
														foreach($get_categoty as $get_categotys){
															?>
                                                            <option value="<?php echo $get_categotys->id;?>" <?php if($get_po_items->category_en==$get_categotys->category_en){ echo 'selected';}?> ><?php echo $get_categotys->category_en;?> | <?php echo $get_categotys->category_th;?></option>
                                                            
                                                            <?php
															
															}
														
														}
													?>
                                                    
											
											
										</select>
												</div>
                                                
                                                <div class="col-sm-6">
													  <label>Brand | Model</label>
										<select class="bootstrap-select required" data-live-search="true" data-width="100%" id="ModelID" name="ModelID" disabled>
                                        
                                        
                                        
                                        <?php
										
										//print_r($brand);
                                        if($get_brand){
											foreach($get_brand as $get_brands){
												
												
												?>
                                                
                                                
                                                <optgroup label="<?php echo $get_brands->brand_en;?>">
                                                <?php
                                                $model = $this->Query_Db->result($this->db->dbprefix.'product_model','brand_id ='.$get_brands->id.'');
												if($model ){
													foreach($model as $models){
														
														echo '<option value="'.$models->id.'"';
														if($get_po_items->model==$models->model){ echo 'selected';}
														
														echo '>'.$get_brands->brand_en.' | '.$models->model.'</option>';
														
														}
													
													
													}
												
												?>
                                                
                                               
                                                </optgroup>
                                                <?php
												
												}
											}
										?>
											
										</select>
												</div>
                                                
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Part Number</label>
													<input type="text" placeholder="Part Number"  class="form-control required" id="PartNumber" name="PartNumber" value="<?php echo $get_po_items->part_number;?>" readonly>
												</div>

												<div class="col-sm-6">
													<label>Serial Number</label>
													<input type="text" placeholder="Serial Number" class="form-control required" id="SerialNumber" name="SerialNumber" value="<?php echo $get_po_items->serial_number;?>" readonly>
												</div>
											</div>
										</div>

										
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Service Tag</label>
													<input type="text" placeholder="Service Tag" class="form-control required" id="Tag" name="Tag" value="<?php echo $get_po_items->service_tag;?>" readonly>
												</div>

												<div class="col-sm-6">
													<label>Cost</label>
													<input type="text" placeholder="Cost" class="form-control required" id="Cost" name="Cost" value="<?php echo $get_po_items->cost;?>" readonly>
												</div>
											</div>
										</div>
                                        
                                        
                                        
                                           <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Warranty Start</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="WarrantyStart" name="WarrantyStart" value="<?php echo $get_po_items->warranty_start;?>" disabled >
										</div>
												</div>

												<div class="col-sm-6">
													<label>Warranty Month</label>
                                                    
                                                    <input type="text" placeholder="Warranty Month" class="form-control required" id="WarrantyMonth" name="WarrantyMonth" value="<?php echo $get_t_po_item->WarrantyMonth;?>" readonly>

																						</div>
											</div>
										</div>

										
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Invoice No.</label>
													<input type="text" placeholder="Invoice No." class="form-control required"  id="INVNumber" name="INVNumber" value="<?php echo $get_po_items->invoice_number;?>" readonly>
												</div>

												<div class="col-sm-6">
													<label>Invoice Date</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="INVDate" name="INVDate" value="<?php echo $get_po_items->invoice_issue_date;?>" disabled>
										</div>
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>Do No.</label>
													<input type="text" placeholder="Do No. " class="form-control required" id="DONumber" name="DONumber" value="<?php echo $get_po_items->do_number;?>" readonly>
												</div>

												<div class="col-sm-6">
													<label>Do Date</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="DODate" name="DODate" value="<?php echo $get_po_items->do_issue_date;?>" disabled>
										</div>
												</div>
											</div>
										</div>
                                        
                                      
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->
                                <?php
								
								}
								
}



?>



 
    
 <script type="text/javascript">
  
 // Accessibility labels
    $('.pickadate-accessibility').pickadate({
        labelMonthNext: 'Go to the next month',
        labelMonthPrev: 'Go to the previous month',
        labelMonthSelect: 'Pick a month from the dropdown',
        labelYearSelect: 'Pick a year from the dropdown',
        selectMonths: true,
        selectYears: true,
		format: 'yyyy-mm-dd',
		formatSubmit: 'yyyy-mm-dd'
    });
	 $('.bootstrap-select').selectpicker();

function noti2(name,val) {
	
	
	
	$.ajax({
						
			url: "<?php echo base_url(); ?>dashboard/updatepodetail",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: "<?php echo $get_podetail->ID;?>",
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			//	alert (data);
				new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
			
				
			}
		});	
		

	
	
    
} 

 

 
 
   // Styled file input
   
 function delete_po(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
				
				
				url: '<?php echo base_url(); ?>dashboard/deletepoitem',	
			//url: "<?php echo base_url(); ?>vendorregister/vp1_fdelete2",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}  




 
  var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
      
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
           // text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                
				
				
		
		 var formData = new FormData($("#regis_st1")[0]);
		  $.ajax({ 
		  			 url: '<?php echo base_url(); ?>dashboard/createpoitem',
					
                     //url:"<?php //echo base_url(); ?>main/ajax_upload",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data: formData,  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
			//alert (data);
			
			 var resultAjax = JSON.parse(data);
			 
			 console.log(resultAjax);
			 
			 if(resultAjax.upload_st == 'true'){
				 
				 swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				 
				  $.blockUI({ 
           
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	//window.location='<?php echo base_url(); ?>dashboard/';
	location.reload();
  }  , 1000 );
				
				
				}
            }
        });
				 
				 } else {
					 
					 //alert('fail');
					 swal({
            title: "Oop.. File type  incorrect...",
            text: "ขออภัยข้อมูลไม่ถูกต้อง | Sorry This file type incorrect.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					 }
				//console.log(data);
					 
		
						 
                     }  
                });  	
					
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		
		
            }
    });


<?php


if($get_po_item){
								foreach($get_po_item as $get_po_items){
								
								?>
								
								var fvalidatorst<?php echo $get_po_items->po_item_id?> = $("#f_edit_item<?php echo $get_po_items->po_item_id?>").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
      
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
           // text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                
				
				
		
		 var formData = new FormData($("#f_edit_item<?php echo $get_po_items->po_item_id?>")[0]);
		  $.ajax({ 
		  			 url: '<?php echo base_url(); ?>dashboard/editpoitem',
					
                     //url:"<?php //echo base_url(); ?>main/ajax_upload",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data: formData,  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
			//alert (data);
			
			 var resultAjax = JSON.parse(data);
			 
			 console.log(resultAjax);
			 
			 if(resultAjax.upload_st == 'true'){
				 
				 swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				 
				  $.blockUI({ 
           
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	//window.location='<?php echo base_url(); ?>dashboard/';
	location.reload();
  }  , 1000 );
				
				
				}
            }
        });
				 
				 } else {
					 
					 //alert('fail');
					 swal({
            title: "Oop.. File type  incorrect...",
            text: "ขออภัยข้อมูลไม่ถูกต้อง | Sorry This file type incorrect.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					 }
				//console.log(data);
					 
		
						 
                     }  
                });  	
					
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		
		
            }
    });
								
								<?php
								
								}
								
}

								
								?>

    
    </script>  


</body>




</html>