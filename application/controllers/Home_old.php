<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Home extends CI_Controller {
	class Home extends CI_Controller {
	
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Lang_Menu');
		$this->load->model('Lang_Model');
		
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		
		
		if(!empty($this->session->userdata('username')) && ($this->session->userdata('type_st') == "T01")){
			
			 redirect('dashboard/toa/', 'refresh');
			
			}else if(!empty($this->session->userdata('username')) && ($this->session->userdata('type_st') == "T02")){
			
			 redirect('dashboard/toa_register/', 'refresh');
			
			}else if(!empty($this->session->userdata('vendor_username')) && ($this->session->userdata('vendor_st') == "S1")){
			
			 redirect('vendorregister/vendorregister/', 'refresh');
			
			}
				
		
		
		//print_r($_REQUEST);
		
		$data['response'] = '';
			
		//	!empty($_REQUEST)
		if(!empty($_REQUEST)){
			
			//echo 'true<br/>';
			
			
			

$curl = curl_init();

curl_setopt_array($curl, array(
 // CURLOPT_URL => "http://10.1.102.7/webservice/auth/login",
  CURLOPT_URL => "http://10.1.102.7/webapp.toagroup.com/public_html/webservice/index.php/auth/login",
  
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => 'username='.$this->input->post('username').'&password='.$this->input->post('password').'',
  CURLOPT_HTTPHEADER => array(
    "auth-key: simplerestapi",
    "cache-control: no-cache",
    "client-service: frontend-client",
    "content-type: application/x-www-form-urlencoded",
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
  $data['response'] = $err;
} else {
	
	//$data['response'] = $response;
 // echo $response;
}

		$resultapi = json_decode($response, true);

	
			//echo $this->input->post('username');
		//print_r($resultapi['type_id']);
		
		//echo $resultapi['type_id'];
		
		 //$resultapi;
		
		
		//echo '<div id="resultajax" style="display:none">'.$response.'</div>';
		//	$resultapi['status'];
			if($resultapi['type_st'] == "T01"){
				
				$sessionuserdata = array(
				'type_st' => $resultapi['type_st'],
				'token' => $resultapi['token'],
				'role' => $resultapi['ad_group'],
				'username' => $resultapi['username'],
				'name' => $resultapi['name'],
				'email' => $resultapi['email']
				
				
				
				);
				
				
				
				
				$this->session->set_userdata($sessionuserdata);
				
				
				$newrole = explode(",",$this->session->userdata('role'));
				if($newrole){
										foreach($newrole as $newroles => $val){
											
											if($val != NULL){
												$this->session->set_userdata(array('role_'.$val => $val));
												}
											
											
											}
										
										
				
										
										
										}
				
				
				} 
			else if($resultapi['type_st'] == "T02") { 
				
				
				$sessionuserdata = array(
				'type_st' => $resultapi['type_st'],
				'token' => $resultapi['token'],
				'role' => $resultapi['ad_group'],
				'username' => $resultapi['username'],
				'name' => $resultapi['name'],
				'email' => $resultapi['email']
				
				
				
				);
				
				$this->session->set_userdata($sessionuserdata);
				
				
				 }
			else if($resultapi['type_st'] == "S1") { 
				
				
				$sessionuserdata = array(
				'vendor_st' => $resultapi['type_st'],
				'token' => $resultapi['token'],
				'role' => $resultapi['ad_group'],
				'vendor_username' => $resultapi['username'],
				'vendor_comname' => $resultapi['name'],
				'vendor_cemail' => $resultapi['email']
				
				
				
				);
				
				$this->session->set_userdata($sessionuserdata);
				
				
				 }	
				 else if($resultapi['type_st'] == "S2") { 
				
				
				$sessionuserdata = array(
				'vendor_st' => $resultapi['type_st'],
				'token' => $resultapi['token'],
				'role' => $resultapi['ad_group'],
				'vendor_username' => $resultapi['username'],
				'vendor_comname' => $resultapi['name'],
				'vendor_cemail' => $resultapi['email']
				
				
				
				);
				
				$this->session->set_userdata($sessionuserdata);
				
				
				 }	
				
				
				
			//echo 'test';	  
			
			echo  $response;
			
			
			
			
			} else {
				
				$data['title']  = 'Register Vendor Information purchase Online';
				$this->load->view('main/index',$data);	
				
				
				}
		
		
	//	username
	


		
		$data['title']  = 'Register Vendor Information purchase Online';
		
		
		
	//	if
	//	$this->load->view('welcome_message',$data);	
		
	//	echo print_r($resultapi);
		
	/*
			if($resultapi['type_id'] == '1'){
				
				echo 'login success';
				} else echo 'login fail';
		
	*/	
		
		//$this->load->view('welcome_message');
	}
	
	

	
	public function checklogin()
	{
		
		$something = $this->input->post('username');
		$data['title']  = $something;
		$data['vendorlogin'] = $something;
		$this->load->view('main/index',$data);	
		//$this->load->view('welcome_message');
	}
}
