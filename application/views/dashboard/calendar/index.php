<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs2');?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>

	


	
</head>


<body class="">




	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
     


			<!-- Main content -->
			<div class="content-wrapper">


                                    

				
                
                
                 <!-- Content area -->
				<div class="content">

					<!-- Basic view -->
					<div class="panel panel-flat">
						
						
						<div class="panel-body">
                        
                        
                        
                                    
                                    <div class="col-md-3">
                                    
                                                                <div class="form-group">
										<select class="select" >
											<optgroup label="CP ALL TOWER">
												<option value="hall1" >Location : CP All HALL 1</option>
												
											</optgroup>
											
										</select>
									</div>
                                    
                                    <h1>To Day Meeting</h1>
                                    <div class="content-detached">

							<div class="panel border-left-lg border-left-danger invoice-grid timeline-content">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-sm-12">
																			<h6 class="text-semibold no-margin-top">Period  : 09.00 - 11.00</h6>
																			<ul class="list list-unstyled">
																				<li> <span class="text-semibold"> Topic : TRUE Business Service with N.E.X.T </span></li>
																				<li><span class="text-semibold"> Contact : K.Siwaponr(SIW) | Tel.023456789</span></li>
																			</ul>
																		</div>

																		
																	</div>
																</div>

																
															</div>
                                                            
                                                            <div class="panel border-left-lg border-left-info invoice-grid timeline-content">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-sm-12">
																			<h6 class="text-semibold no-margin-top">Period  : 11.30 - 14.00</h6>
																			<ul class="list list-unstyled">
																				<li> <span class="text-semibold"> Topic : TRUE Business Service with N.E.X.T </span></li>
																				<li><span class="text-semibold"> Contact : K.Siwaponr(SIW) | Tel.023456789</span></li>
																			</ul>
																		</div>

																		
																	</div>
																</div>

																
															</div>
                                                            
                                                            <div class="panel border-left-lg border-left-success invoice-grid timeline-content">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-sm-12">
																			<h6 class="text-semibold no-margin-top">Period  : 15.00 - 16.00</h6>
																			<ul class="list list-unstyled">
																				<li> <span class="text-semibold"> Topic : TRUE Business Service with N.E.X.T </span></li>
																				<li><span class="text-semibold"> Contact : K.Siwaponr(SIW) | Tel.023456789</span></li>
																			</ul>
																		</div>

																		
																	</div>
																</div>

																
															</div>
                                                            
                                                            <div class="panel border-left-lg border-left-success invoice-grid timeline-content">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-sm-12">
																			<h6 class="text-semibold no-margin-top">Period  : 16.30 - 18.00</h6>
																			<ul class="list list-unstyled">
																				<li> <span class="text-semibold"> Topic : TRUE Business Service with N.E.X.T </span></li>
																				<li><span class="text-semibold"> Contact : K.Siwaponr(SIW) | Tel.023456789</span></li>
																			</ul>
																		</div>

																		
																	</div>
																</div>

																
															</div>
                                                            
                                                            <div class="panel border-left-lg border-left-success invoice-grid timeline-content">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-sm-12">
																			<h6 class="text-semibold no-margin-top">Period  : 19.00 - 20.00</h6>
																			<ul class="list list-unstyled">
																				<li> <span class="text-semibold"> Topic : TRUE Business Service with N.E.X.T </span></li>
																				<li><span class="text-semibold"> Contact : K.Siwaponr(SIW) | Tel.023456789</span></li>
																			</ul>
																		</div>

																		
																	</div>
																</div>

																
															</div>
                                                            
                                                            <div class="panel border-left-lg border-left-success invoice-grid timeline-content">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-sm-12">
																			<h6 class="text-semibold no-margin-top">Period  : 21.00 - 23.00</h6>
																			<ul class="list list-unstyled">
																				<li> <span class="text-semibold"> Topic : TRUE Business Service with N.E.X.T </span></li>
																				<li><span class="text-semibold"> Contact : K.Siwaponr(SIW) | Tel.023456789</span></li>
																			</ul>
																		</div>

																		
																	</div>
																</div>

																
															</div>

							

<!--<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
								
								<h6 class="alert-heading text-semibold">10.30 - 12.00</h6>
								TRUE Business Service with N.E.X.T<br>
 								K.Siwaponr(SIW)
						    </div>-->

						</div>
                                       
                                      

                                    </div>
                                    
                                      <div class="col-md-9">
                                      <div class="fullcalendar-basic"></div>
                                      
                                      </div>
                        
                        
							
										
                                        
                            
                            
                                    
                                    
<!--<a href="<?php echo base_url(); ?>dashboard/addwallpaper" class="btn btn-success btn-sm legitRipple" >Upload Wallpaper<i class="icon-plus3 position-right"></i><span class="legitRipple-ripple"></span></a>-->
							
						</div>
					</div>
					<!-- /basic view -->


					<!-- Agenda view -->
					
					<!-- agenda view -->


					<!-- Footer -->
					
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
    <div id="fullCalModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> Meeting Detail  </h5>
                    
					<p id="modalTitle" class="modal-title"></p>
				</div>
				<div id="modalBody" class="modal-body">
                
                
                </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					
				</div>
			</div>
		</div>
	</div>
   
    <!-- Vertical form modal -->
					<div id="modal_form_vertical" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> Meeting Detail</h5>
								</div>

								<form action="#" method="post">
									<div class="modal-body">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ชื่อผู้ผลิต</label>
													<input type="text" placeholder="ระบุชื่อผู้ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>สินค้าที่จำหน่าย</label>
													<input type="text" placeholder="ระบุชื่อสินค้าที่จำหน่าย" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ประเทศที่ผลิต</label>
													<input type="text" placeholder="ระบุประเทศที่ผลิต" class="form-control">
												</div>

												<div class="col-sm-6">
													<label>พิกัดภาษีนำเข้า / อัตราภาษี</label>
													<input type="text" placeholder="ระบุพิกัดภาษีนำเข้า / อัตราภาษี " class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<label>Load Time</label>
													<input type="text" placeholder="ระบุ Load Time" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Minimum Order</label>
													<input type="text" placeholder="ระบุ Minimum Order" class="form-control">
												</div>

												<div class="col-sm-4">
													<label>Packing Size </label>
													<input type="text" placeholder="ระบุ Packing Size" class="form-control">
												</div>
											</div>
										</div>

										
		
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>ข้อมูลติดต่อเกี่ยวกับสินค้า</label>
													<input type="text" placeholder="ระบุข้อมูลติดต่อสินค้า" class="form-control">
													<span class="help-block">eg: k.หนูแดง T 023456789</span>
												</div>

												<div class="col-sm-6">
                                                
                                            

                                                
													<label>MSDS(ไทย/อังกฤษ) </label>
													<input type="file" name="styled_file" class="file-styled" required="required">
													<span class="help-block">Accepted formats: pdf. Max file size 2Mb </span>
												</div>
											</div>
										</div>
                                        
                                        
                                        <div class="form-group">
											<div class="row">
												<div class="col-sm-6">
													<label>เงือนไขในการชำระเงิน</label>
													<input type="text" placeholder="ระบุเงือนไขในการชำระเงิน" class="form-control">
													<span class="help-block">eg: 30 วัน , 90 วัน </span>
												</div>

												<div class="col-sm-6">
                                                
                                              
                                              
                                              <label>สถานะซื้อขายกับ TOA </label> 
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
              					                 
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>

                                                
												
													
												</div>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติมที่เกี่ยวกับตัวสินค้า : </label>
		                                    <textarea name="experience-description" rows="4" cols="4" placeholder="โปรดระบุเฉพาะข้อมูลที่เกี่ยวข้องกับตัวสินค้า" class="form-control"></textarea>
		                                </div>
                                        
                                        
                                       <!--       <div class="form-group">
                                
                                <div class="checkbox checkbox-switch">
                                
                                <label class="checkbox-inline checkbox-switchery switchery-xs">
												<input type="checkbox" name="inline_switchery_group" class="switchery" required="required">
												เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
											</label>
												
                                          <label>
													<input type="checkbox" name="switch_single" data-on-text="Yes" data-off-text="No" class="switch" required="required">
													เป็นสินค้าที่ทาง TOA PAINT กำลังจะซื้อ / ซื้ออยู่ ใช่หรือไม่
												</label> 
                                                
											</div>
                                
											
		                                </div>
                                        -->
                                        
                                        
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


  <!-- Vertical form modal -->
					<div id="fullCalModalxx" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
                            
                            
                            
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> Meeting Detail  </h5>
								</div>
                                
                               	


								
                                <div class="modal-body">
										
                                        </div>
                                        
                                        	<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
									</div>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


 
<script type="text/javascript">

$(function() {


    // Add events
    // ------------------------------

    // Default events
	
	
    var events = [ {
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-03T09:00:00',
            end: '2018-05-03T11:00:00'
        },
       {
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#00BCD4',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-03T11:30:00',
            end: '2018-05-03T14:00:00'
	 
  }, {
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-03T15:00:00',
            end: '2018-05-03T16:00:00'
	 
  },{
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-03T16:30:00',
            end: '2018-05-03T18:00:00'
	 
  },{
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-03T19:00:00',
            end: '2018-05-03T20:00:00'
	 
  },{
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-03T21:00:00',
            end: '2018-05-03T23:00:00'
	 
  },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-04T08:00:00',
            end: '2018-05-04T10:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-04T10:00:00',
            end: '2018-05-04T14:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-04T14:00:00',
            end: '2018-05-04T19:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-04T19:00:00',
            end: '2018-05-04T23:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-02T08:00:00',
            end: '2018-05-02T10:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-02T10:00:00',
            end: '2018-05-02T14:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-02T14:00:00',
            end: '2018-05-02T19:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-02T19:00:00',
            end: '2018-05-02T23:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-01T09:00:00',
            end: '2018-05-01T11:00:00'
        },
       {
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#00BCD4',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-01T11:30:00',
            end: '2018-05-01T14:00:00'
	 
  }, {
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-01T15:00:00',
            end: '2018-05-01T16:00:00'
	 
  },{
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-01T16:30:00',
            end: '2018-05-01T18:00:00'
	 
  },{
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-01T19:00:00',
            end: '2018-05-01T20:00:00'
	 
  },{
     		title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-05-01T21:00:00',
            end: '2018-05-01T23:00:00'
	 
  },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-04-30T08:00:00',
            end: '2018-04-30T10:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#F44336',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-04-30T10:00:00',
            end: '2018-04-30T14:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-04-30T14:00:00',
            end: '2018-04-30T19:00:00'
        },{
            title: 'TRUE Business Service with N.E.X.T',
			color: '#4CAF50',
			description: "K.Siwaponr(SIW) | Tel.023456789",
            start: '2018-04-30T19:00:00',
            end: '2018-04-30T23:00:00'
        },];


   

    // Event background colors
    var eventBackgroundColors = [
        {
            title: 'All Day Event',
            start: '2014-11-01'
        },
        {
            title: 'Long Event',
            start: '2014-11-07',
            end: '2014-11-10',
            color: '#DCEDC8',
            rendering: 'background'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-06T16:00:00'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-16T16:00:00'
        },
        {
            title: 'Conference',
            start: '2014-11-11',
            end: '2014-11-13'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T10:30:00',
            end: '2014-11-12T12:30:00'
        },
        {
            title: 'Lunch',
            start: '2014-11-12T12:00:00'
        },
        {
            title: 'Happy Hour',
            start: '2014-11-12T17:30:00'
        },
        {
            title: 'Dinner',
            start: '2014-11-24T20:00:00'
        },
        {
            title: 'Meeting',
            start: '2014-11-03T10:00:00'
        },
        {
            title: 'Birthday Party',
            start: '2014-11-13T07:00:00'
        },
        {
            title: 'Vacation',
            start: '2014-11-27',
            end: '2014-11-30',
            color: '#FFCCBC',
            rendering: 'background'
        }
    ];



    // Initialization
    // ------------------------------

    // Basic view
    $('.fullcalendar-basic').fullCalendar({
       /* header: {
          //  left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
       // defaultDate: '2014-11-12',
        editable: false,
        events: events*/
		   header: {
            left: 'prev,next today',
            center: 'title',
			text : 'xxxx',
            right: 'month,agendaWeek,agendaDay'
        },
       // defaultDate: '2014-11-12',
        defaultView: 'agendaWeek',
		nowIndicator: true,
		now: '2018-05-03T11:46:00',
        editable: false,
        events: events,
		allDaySlot: false,
		minTime: "08:00:00",
		maxTime: "23:00:00",
		timeFormat: 'HH:mm',
		axisFormat: 'HH:mm',
		//  resources: 'https://fullcalendar.io/demo-resources.json?with-nesting',
    //events: 'https://fullcalendar.io/demo-events.json?with-resources',
		eventClick: function(event, jsEvent, view) {
				$('#modalTitle').html(event.title);
				$('#modalBody').html(event.description);
				$('#eventUrl').attr('href',event.url);
				$('#fullCalModal').modal();
				return false;
			}
    });




});
   

</script>


</body>




</html>