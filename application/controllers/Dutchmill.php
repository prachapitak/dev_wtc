<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Home extends CI_Controller {
	class Dutchmill extends CI_Controller {
	
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Lang_Menu');
		$this->load->model('Lang_Model');
		$this->load->model('Query_Db');
		
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function index()
	{
		//$this->load->view('welcome_message');
		$data['title']  = 'DUTCH MILL PRODUCTIVITY DASHBOARD';
		//$this->load->view('main/index',$data);	
		//print_r($data);
		//exit;
		$this->load->view('dutchmill/index',$data);	
		//$this->load->view('dashboard/calendar/index',$data);	
		}
		
		
		 public function line1()
	{
		//$this->load->view('welcome_message');
		$data['title']  = 'DUTCH MILL PRODUCTIVITY DASHBOARD';
		//$this->load->view('main/index',$data);	
		//print_r($data);
		//exit;
		$this->load->view('dutchmill/line1',$data);	
		//$this->load->view('dashboard/calendar/index',$data);	
		}
	 
	public function index2()
	{
		
		
		
		if(!empty($this->session->userdata('username')) && ($this->session->userdata('type_st') == "T01")){
			
			 redirect('dashboard/wtc/', 'refresh');
			
			}
			
			
			
			
				
		 
		
		//print_r($_REQUEST);
		
		$data['response'] = '';
			
		//	!empty($_REQUEST)
		if(!empty($_REQUEST)){
			
			
	$username = $_REQUEST['username'];
	$password = $_REQUEST['password'];
	$newuser = $username.DOMAIN1;
	$email = $username.DOMAIN2;
	$Adconnect = ldap_connect(SERVER_AD);
	
	
	ldap_set_option($Adconnect, LDAP_OPT_REFERRALS, 0);
	ldap_set_option($Adconnect, LDAP_OPT_PROTOCOL_VERSION, 3);
	//$b = @ldap_bind($ad,'toa\prachapitak','prachazamak.s1');
	$ldap_bind = @ldap_bind($Adconnect,$newuser,$password);
	if(!$ldap_bind){
							
			$response = '{"login_st":"fail"}';	
			echo  $response;			
				
								
								
								}else{
								
								
								
									
		$search_filter = "(userprincipalname=".$newuser.")";
		$result = ldap_search($Adconnect,'DC=TOA,DC=CO,DC=TH', $search_filter);
					
		if($result){
			//$entries = ldap_get_entries($Adconnect, $result);	
			//$ADgroup = array();
			//print_r($entries[0]);
			
			$getuser = $this->Query_Db->record($this->db->dbprefix.'user','user_username ="'.$_REQUEST['username'].'" and user_pass = "'.$_REQUEST['password'].'"');
			
			
		
		if($getuser){
					//echo $getuser['username'];
					
					$sessionuserdata = array(
				
				'username' => $getuser->user_username,
				'pass' => $getuser->user_pass,
				'type_st' => "T01"
				
				
			);
					$this->session->set_userdata($sessionuserdata);
			
			$response = json_encode($sessionuserdata);	
			echo  $response;	
			
			
					}else {
						
						
					$sessionuserdata = array(
				
				'username' => $_REQUEST['username'],
				'pass' => $_REQUEST['password'],
				'type_st' => "T01"
				
				
			);	
			
			$data = array(
				
				'user_username' => $_REQUEST['username'],
				'user_pass' => $_REQUEST['password'],
				'name' => $_REQUEST['username'],
				'email' => $email,
				
				
			);	
			
			//$insertwallpaper  = $this->Query_Db->insert($this->db->dbprefix.'wallpaper',$data );

						
			$insertuser = $this->Query_Db->insert($this->db->dbprefix.'user',$data);
			
				
					$this->session->set_userdata($sessionuserdata);
						
					
			$response = json_encode($sessionuserdata);	
			echo  $response;		
						
				
						
						}
						
						
						
					
						}else {
							
					$response = '{"login_st":"fail"}';	
			echo  $response;	
							
							}
				
					
		}
	
//	exit();
	
			
			

		
	
			
			
			
			
			
			
			} else {
				
				//echo  '1';	
				$data['title']  = 'TOA WALLPAPER';
				
			
				$this->load->view('main/index',$data);	
				
				
				}
				
				
				//echo 'testxx';
		
		
	//	username
	


		
	//	$data['title']  = 'Register Vendor Information purchase Online';
		
		
		
	//	if
	//	$this->load->view('welcome_message',$data);	
		
	//	echo print_r($resultapi);
		
	/*
			if($resultapi['type_id'] == '1'){
				
				echo 'login success';
				} else echo 'login fail';
		
	*/	
		
		//$this->load->view('welcome_message');
	}
	
	
	public function deleteimg($imgname = NULL)
	{
		

		if($imgname){

			$path = './assets/wallpaperapprove/'.$imgname.'.jpg';
		//	$this->load->helper("file");
			unlink($path);
			echo $path;
			echo 'Image'.$imgname.'delete';

		}
		
		

	$this->load->view('main/deleteimage');
		
	}
	
	public function checklogin()
	{
		
		$something = $this->input->post('username');
		$data['title']  = $something;
		$data['vendorlogin'] = $something;
		$this->load->view('main/index',$data);	
		//$this->load->view('welcome_message');
	}
}
