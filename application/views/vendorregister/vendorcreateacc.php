<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?>

    
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>

    
   <!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/prism.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/extension_blockui.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->

</head>


<body class="login login-cover">

 <?php $this->load->view('main/allheader');?>
<?php //echo $this->session->userdata('lang');
//print_r($title);
//print_r($response);
?>

<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
        
				

			<!-- Main content -->
			<div class="content-wrapper">
				
                
                
                
				<!-- Content area -->
				<div class="content pb-20 " style="padding-top:100px;">
                
                
                
                <div style="color:#F00; font-size:16px;" id="displayerror" > </div> 


   
                    	<!-- Registration form -->
                        
                        <form id="form-validate"  class="form-validate " enctype="multipart/form-data">
					
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										
                                        
                                        <div class="text-center">
								<div class="border-slate-300 text-slate-300"><img src="<?php echo base_url(); ?>assets/images/logo_dark.png" alt="" style="max-height:45px;"></div>
								<h5 class="content-group">Create account <small class="display-block">All field  Request</small></h5>
                                
                                <?php //print_r($this->session->userdata());?>
							</div>

										<div class="form-group has-feedback">
											<input type="text" class="form-control" required="required" name="username" id="username" placeholder="ระบุ username เพื่อ Login เข้าระบบ | Please input your username" value="" onchange="checkuser(this.value)" >
											<div class="form-control-feedback">
												<i class="icon-user-plus text-muted"></i>
											</div>
										</div>

										<div class="form-group has-feedback">
											<input type="text" class="form-control" required="required" name="comname" id="comname" placeholder="ระบุชื่อบริษัทของท่าน เช่น  ' บริษัท ค้าขายร่ำรวย จำกัด '| Please input your company name " value="<?php echo $this->session->userdata('tk_company'); ?>"  >
											<div class="form-control-feedback">
												<i class="icon-user-check text-muted"></i>
											</div>
										</div>
                                        
										
                                        	
			<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
                                                
                                                <input type="password" name="password" id="password" class="form-control" required="required" placeholder="ระบุอย่างน้อย 5 ตัวอักษร | Minimum 5 characters allowed" value="">
													
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
                                                
                                                <input type="password" id="repeat_password" name="repeat_password" class="form-control" required="required" placeholder="ยืนยันรหัสผ่านอีกครั้ง | Try different password" value="">
                                                
													
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>
										</div>
            
									
                                        

										

										<div class="row">
											<div class="col-md-12">
												<div class="form-group has-feedback">
													<input type="email" disabled="disabled" class="form-control" placeholder="" readonly value="<?php echo $this->session->userdata('tk_email'); ?>" name="vendor_cemail" id="vendor_cemail">
													<div class="form-control-feedback">
														<i class="icon-mention text-muted"></i>
													</div>
												</div>
											</div>

											
										</div>

										

										<div class="text-right">
											
											<button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<!-- /registration form -->
                    
						
					
					

				</div>
				<!-- /content area -->
                
                
                
  
    

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->


              


	</div>
	<!-- /page container -->
    
	


	
        <?php
    if(ENVIRONMENT == 'development'){
		
		$this->load->view('main/pagerender');
		
		}
	?>

    
    
    
 <script type="text/javascript">
    

		
		function checkuser(val) {
   // alert("The input value has changed. The new value is: " + val);
	var username = val;
	
	if(username.length >= 5 ){
		
		$.ajax({
					
			url: "../checkuser?",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					newusername	: 	username,
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
				
				//JSON.parse(data);
				//$("#resultajax").html(data)
				//alert(data);
				//console.log(label);
				var resultAjax = JSON.parse(data);
				//alert(resultAjax.user_st);
				if(resultAjax.user_st == 'fail'){
					
					swal({
            title: "Oop.. Username incorrect...",
            text: "ขออภัยระบบไม่อนุญาติให้ใช้ ชื่อผู้ใช้นี้  โปรดใช้ชื่อใหม่ | Sorry This username denie. Please try new username.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					$('#username').val("");
					$('#username').focus();
					 //document.getElementById("mytext").focus();
					} else {
						
						 swal({
            title: "Username correct .",
            text: "ชื่อผู้ใช้นี้สามารถใช้งานได้ค่ะ | You can use this username.",
            confirmButtonColor: "#66BB6A",
            type: "success"
        });
						
						}
					
					
				
			},error: function(){
				alert('Error Send Data ');
				}
		});	
		
		}
	
		
	
	
	//alert(username);
	
}
	
    
$(function() {
	



	// Style checkboxes and radios
	$('.styled').uniform();
	

    // Setup validation
    $("#form-validate").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
			
            label.addClass("validation-valid-label").text("Successfully")
        },
        rules: {
			
			 password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
			 username: {
                minlength: 5
            },
			
           
        },
        messages: {
            username: {
				
				required: "Enter your username",
            	minlength: jQuery.validator.format("ระบุอย่างน้อย {0} ตัวอักษร | At least {0} characters required")
				
				},
            password: {
            	required: "Enter your password",
            	minlength: jQuery.validator.format("ระบุอย่างน้อย {0} ตัวอักษร At least {0} characters required")
            }
        },
		submitHandler: function (form) {
	
			
			
        swal({
            title: "ยืนยันข้อมูลถูกต้อง | Are you sure ? ",
            text: "หากข้อมูลของท่านถูกต้องแล้ว โปรดกด ตกลง | If you sure your data please click OK button ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "ตกลง | OK",
			cancelButtonText: "ยังไม่บันทึก | Cancel ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
        },
        function(isConfirm){
            if (isConfirm) {
				
				
				$.ajax({
					
			url: "../createvendor?",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
	
					vendor_username	: 	$('#username').val(),
					vendor_comname		: 	$('#comname').val(),
					vendor_pass	: 	$('#password').val(),
					vendor_cemail	: 	$('#vendor_cemail').val(),
					

					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
				
				//JSON.parse(data);
				//$("#resultajax").html(data)
				//alert(data);
				//console.log(label);
				var resultAjax = JSON.parse(data);
				//alert(resultAjax.user_st);
				if(resultAjax.create_st == 'true'){
					
					 swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป | Your Account Created.",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='../';
  }  , 1000 );
				
				
				}
            }
        });
					
					} else {
						
						
						 swal({
                    title: "ข้อมูลถูกยกเลิก | Cancelled ",
                    text: "คุณได้ระบุข้อมูลไม่ถูกต้อง | Wrong Data",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
						$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			  
			   
			   		$.blockUI({ 
            message: '<i class="icon-shield-notice " style="font-size: 60px;color: red;"></i> <p style="font-size: 17px;"> Sorry !!  Username incorrect!!  </p>',
           timeout: 3000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
            }
        });
				
			 
			 
			 		
		 setTimeout( function(){ 
    // Do something after 1 second 
	//window.location='./';
  }  , 3000 );
			   
			   
            } 
        });
						
						}
						
						
						
				
				
				
					
					
				
			},error: function(){
				alert('Error Send Data ');
				}
		});	
				
               
				
				
				
               
				
					//alert(data);
				
				
					
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก | Cancelled ",
                    text: "คุณได้กดยกเลิกการบันทึกข้อมูล | Your are click cancel",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    
			
			
			   
			
			
			
			
			
		
					
					
			
		
            }
		

		
		
    });
	

	
	$("#form-createacc").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
			
            label.addClass("validation-valid-label").text("Successfully")
        },
        rules: {
            password: {
                minlength: 5
            }
        },
        messages: {
            username: "Enter your username",
            password: {
            	required: "Enter your password",
            	minlength: jQuery.validator.format("At least {0} characters required")
            }
        },
		submitHandler: function (form) {
	
			
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		
			
			
			
			
			
		
			
			
				
		
				
			
			
					
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
		

		
		
    });
	
	


});

    
    </script>   
    
    
</body>


</html>