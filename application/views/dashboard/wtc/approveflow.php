<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?>
      
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_extension_fixed_columns.js"></script>



</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">- Generate Approve Flow   </span> - สร้าง Flow สำหรับ Approve Vendor  <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php $this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->

<?php  $this->load->view('dashboard/toa/noti');?>
					
				
		
                                    		
                                    

				<!-- Content area -->
				<div class="content">
                
                <!-- Quick stats boxes -->
							
                        

                        
                        
                        <div class="panel panel-flat">
                        <div class="panel-heading">
                        
									
                                   
                                    	
									
								</div>
                                
                        
								  
						
                                

								<div class="panel-body">
                                 <div class="col-md-12">
                                 
                                 
                                 <div class="alert alert-warning alert-styled-left">
										
										<span class="text-semibold">คำเตือน !</span>  หาก Flow ของท่านมีการใช้งานในระบบท่านจะไม่สามารถแก้ไข / ลบ Flow  ดังกล่าว  ได้ เนื่องจาก ระบบอื่นได้มีการนำ Flow ของท่าน ไปอ้างอิง  หากท่านไม่แน่ใจ โปรดตรวจสอบ Flow ของท่านให้เรียบร้อยว่าถูกต้องหรือไม่ก่อนนำ Flow ดังกล่าวไปใช้งาน 
                                       
                                        			    </div>
                                    
                                
                                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal_form_acc">Generate Approve Flow <i class="icon-plus3 position-right"></i></button>
                                 <hr>
                                 </div>
                               
                               
                               <?php
							  function check_flow_type_job($typejob){
								  
								  switch ($typejob){
									
									case "GP":
									return $newtypejob = 'กลุ่มงานจัดซื้อทั่วไป (General Purchase)'; 
									break;
									case "RMPK":
									return $newtypejob = 'กลุ่มงานจัดซื้อ วัตถุดิบ และ Packaging (PK & RM)'; 
									break;
									default:
										return $newtypejob = 'Data Incorrect Please Contact Administrator';
									  
									  }
								  //case
								  
								  } 
								  
								   function check_flow_type_vendor($typejob){
								  
								  switch ($typejob){
									
									case "CM":
									return $newtypejob = 'Commercial'; 
									break;
									case "OP":
									return $newtypejob = 'Operation'; 
									break;
									case "RM":
									return $newtypejob = 'Raw Material'; 
									break;
									case "PK":
									return $newtypejob = 'Packaging'; 
									break;
									default:
										return $newtypejob = 'Data Incorrect Please Contact Administrator';
									  
									  }
								  //case
								  
								  } 
								  
								   function check_use_flow($val,$val2){
								  
								  switch ($val){
									
									case 1:
									$newval = '	<li class="disabled"><a href="#nogo" ><i class="icon-pencil7"></i> Edit / แก้ไข</a></li>
																
                                            
												
												<li class="divider"></li>
                                                
                                                <li class="disabled"><a href="#nogo"><i class="icon-bin"></i> Delete Flow / ลบข้อมูล</a></li>';
									
									return $newval ; 
									break;
									case 0:
									
									$newval = '	<li><a data-toggle="modal" data-target="#modal_form_acc_'.$val2.'"><i class="icon-pencil7"></i> Edit / แก้ไข</a></li>
																
                                            
												
												<li class="divider"></li>
                                                
                                                <li class=""><a href="#nogo" onclick="deleteflow('.$val2.')"><i class="icon-bin"></i> Delete Flow / ลบข้อมูล</a></li>
												';
									
									return $newval ;
									break;
									 
									default:
										return $newval = '<li><a data-toggle="modal" data-target="#modal_form_acc"><i class="icon-pencil7"></i> Edit / แก้ไข</a></li>
																
                                            
												
												<li class="divider"></li>
                                                
                                                <li class=""><a href="#"><i class="icon-bin"></i> Delete Flow / ลบข้อมูล</a></li>';
									  
									  }
								  //case
								  
								  } 
								  
								  
								  function check_role_avp_type($val){
								  
								  switch ($val){
									
									case "CM":
									return $newval = 'GP'; 
									break;
									case "OP":
									return $newval = 'GP'; 
									break;
									case "RM":
									return $newval = 'RMPK'; 
									break;
									case "PK":
									return $newval = 'RMPK'; 
									break;
									default:
										return $newval = 'FALSE';
									  
									  }
								  //case
								  
								  } 
								  
								  
								  
								if(!empty($getuser)){
									
									//$queryrole  = $this->Query_Db->record($this->db->dbprefix.'users','username =  "'.$showflows->flow_avp_id.'"');
									}  
								  
								  
								  
								//  print_r($permission);
												
												
												
												if(!empty($permission)){
													
													
													
													foreach($permission as $permissions){
														
														
														
														$newselectshow[check_role_avp_type($permissions)]= check_role_avp_type($permissions);
														
														$selectmpr[$permissions]  = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role = "MPR" AND role_permission =  "'.$permissions.'" AND (role_st = 1)');
														
														
														$mpr_role_ref_id = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role = "MPR" AND role_permission =  "'.$permissions.'" AND (role_st = 1)');
														
														
														$avp_role_ref_id = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role = "AVP" AND role_permission =  "'.check_role_avp_type($permissions).'" AND (role_st = 1)');
														
														$acc_role_ref_id = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role = "ACC" AND role_permission =  "'.check_role_avp_type($permissions).'" AND (role_st = 1)');
														
														$vendor_type = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role =  "PR" AND role_permission =  "'.$permissions.'" AND (role_st = 1)');
														
														
														$job_type = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role =  "PR" AND role_permission =  "'.$permissions.'" AND (role_st = 1)');
														
														
														//print_r($job_type);
														//$job_type = $this->Query_Db->result($this->db->dbprefix.'users_role','role_role = "ACC" AND role_permission =  "'.check_role_avp_type($permissions).'" AND (role_st = 1)');
														
														
														
												
												
												if($vendor_type){
															foreach($vendor_type as $vendor_types){
																
																//$new_mpr_role_ref_ids[$vendor_types->role_permission] = $vendor_types->role_permission;
																
																$newselectshow_vendortype[$vendor_types->role_permission]= $vendor_types->role_permission;
																
																
																
																}
																
																
																
															}
															
															
												
														
														if($mpr_role_ref_id){
															foreach($mpr_role_ref_id as $mpr_role_ref_ids){
																
																$new_mpr_role_ref_ids[$mpr_role_ref_ids->role_ref_id] = $mpr_role_ref_ids->role_ref_id;
																
																
																
																}
															}
															
															
															if($avp_role_ref_id){
															foreach($avp_role_ref_id as $avp_role_ref_ids){
																
																$new_avp_role_ref_ids[$avp_role_ref_ids->role_ref_id] = $avp_role_ref_ids->role_ref_id;
																
																}
															}
															
																if($acc_role_ref_id){
															foreach($acc_role_ref_id as $acc_role_ref_ids){
																
																$new_acc_role_ref_ids[$acc_role_ref_ids->role_ref_id] = $acc_role_ref_ids->role_ref_id;
																
																}
															}
															
															
															
														
														}
														
													
													}
													
								  
							   
							  if(!empty($flowshow)) {
								   // print_r($flowshow);
									foreach($flowshow as $flowshows){
										
										if(!empty($flowshows)){
											
											foreach($flowshows as $showflows){
												
												$findavp  = $this->Query_Db->record($this->db->dbprefix.'users','username =  "'.$showflows->flow_avp_id.'"');
												
												$findmpr  = $this->Query_Db->record($this->db->dbprefix.'users','username =  "'.$showflows->flow_mpr_id.'"');
												
												$findacc  = $this->Query_Db->record($this->db->dbprefix.'users','username =  "'.$showflows->flow_acc_id.'"');
												$findpr  = $this->Query_Db->record($this->db->dbprefix.'users','username =  "'.$showflows->flow_pr_id.'"');
												
												
												
												
												$newselectshow[$showflows->flow_type_job]= $showflows->flow_type_job;
												//$newselectshow_vendortype[$showflows->flow_type_vendor]= $showflows->flow_type_vendor;
												
													
												
											//	print_r($new_acc_role_ref_ids);
												
												
												?>
                                                
                                                <div class="col-md-6">
                            <!-- Segmented button -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h6 class="panel-title"><?php echo $showflows->flow_name;?></h6>
									<div class="heading-elements">
										<div class="btn-group heading-btn">
				                            <button class="btn btn-info">Action</button>
				                            <button class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
											<ul class="dropdown-menu dropdown-menu-right">
                                            
                                            <?php
                                            echo check_use_flow($showflows->flow_use,$showflows->flow_id);
											?>
                                            
                                            
                                            	
												
											</ul>
				                        </div>
			                        </div>
								</div>
								
								<div class="panel-body">
									<ul class="media-list media-list-bordered">
										<span class="label border-left-primary label-striped ">	
<?php echo check_flow_type_job($showflows->flow_type_job);?></span>  &nbsp; <span class="label border-left-primary label-striped "> 	
  <?php echo check_flow_type_vendor($showflows->flow_type_vendor);?></span>




										<?php
                                        if(!empty($findavp)){
													
													//print_r($findavp);
													
													?>
                                                    
                                                    	<li class="media">
                                        
                                       
											<div class="media-left">
												<a href="#"><img src="http://toa/PhoneTOA/IMG/emp/<?php echo $findavp->empid;?>.jpg" class="img-circle" alt=""></a>
											</div>

											<div class="media-body">
												<h6 class="media-heading"><span class="label label-flat  border-danger text-danger-600"> ผู้อนุมัติระดับ AVP (AVP)</span><br>คุณ <?php echo $findavp->fullname_th;?> ( <?php echo $findavp->fullname_en;?> )  </h6>
                                               
												IP Phone : <?php echo $findavp->ipphone;?> , Email : <?php echo $findavp->email;?>
											</div>
										</li>

                                                    <?php
													
													}
										
										
										?>

									
										
							<?php
                                        if(!empty($findmpr)){ 
										
										?>
                                        <li class="media">
											<div class="media-left">
												<a href="#"><img src="http://toa/PhoneTOA/IMG/emp/<?php echo $findmpr->empid;?>.jpg" class="img-circle" alt=""></a>
											</div>

											<div class="media-body">
												<h6 class="media-heading"><span class="label label-flat  border-danger text-danger-600">ผู้อนุมัติระดับหัวหน้าจัดซื้อ (MPR)</span> <br>คุณ <?php echo $findmpr->fullname_th;?> ( <?php echo $findmpr->fullname_en;?>  )  </h6>
                                               
												IP Phone : <?php echo $findmpr->ipphone;?> , Email : <?php echo $findmpr->email;?>
											</div>
										</li>
                                        <?php
										
										}
										
										?>
										
										
                                        	<?php
                                        if(!empty($findacc)){ 
										
										?>
                                        
                                        <li class="media">
											<div class="media-left">
												<a href="#"><img src="http://toa/PhoneTOA/IMG/emp/<?php echo $findacc->empid;?>.jpg" class="img-circle" alt=""></a>
											</div>

											<div class="media-body">
												<h6 class="media-heading">
                                                <?php
												//echo $showflows->flow_acc_st;
												if($showflows->flow_acc_st == 0){
													
													echo '<span class="label label-flat  border-info text-info-600">บัญชีผู้ตรวจสอบ  (ACC)</span>';
													}else { echo  '<span class="label label-flat  border-danger text-danger-600">บัญชีผู้อนุมัติ  (ACC)  </span>'; }
												 ?>
                                                
                                                	<br>
                                                คุณ <?php echo $findacc->fullname_th;?> ( <?php echo $findacc->fullname_en;?> )  </h6>
                                               
												IP Phone : <?php echo $findacc->ipphone;?> , Email : <?php echo $findacc->email;?>
											</div>
										</li>
                                        <?php
										
										}
										?>
										
                                        
                                        
                                        
										
                                        <?php
                                        if(!empty($findpr)){ 
										
										?>
                                        <li class="media">
											<div class="media-left">
												<a href="#"><img src="http://toa/PhoneTOA/IMG/emp/<?php echo $findpr->empid;?>.jpg" class="img-circle" alt=""></a>
											</div>

											<div class="media-body">
												<h6 class="media-heading">
                                                <span class="label label-flat  border-danger text-danger-600">ผู้ดำเนินการระดับเจ้าหน้าที่ (PR)</span>	<br>
                                                คุณ <?php echo $findpr->fullname_th;?>  ( <?php echo $findpr->fullname_en;?>  )  </h6>
                                               
												IP Phone : <?php echo $findpr->ipphone;?> , Email : <?php echo $findpr->email;?>
											</div>
										</li>
                                        
                                        <?php
										}
										?>
                                        
										
										
                                        
                                        
                                        
									</ul>
								</div>
							</div>
							<!-- /segmented button -->
                            </div>
                                               
                                               
                                               
                                               	  <!-- Vertical form modal -->
					<div id="modal_form_acc_<?php echo $showflows->flow_id;?>" class="modal fade">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i>  Flow เพื่อใช้ในการอนุมัติ </h5>
								</div>

								
                                
                                  <form class="form-validation form-validate-jquery" id="editflow" action="#"  method="post">
                                  
                                  
                                  <?php
                                  
								  //$showflows->flow_id;
								  
								  $getthisflow = $this->Query_Db->record($this->db->dbprefix.'flow','flow_id =  '.$showflows->flow_id.'');
								  
								  //print_r($getthisflow);
								  
								  ?>
                                  
                                  
                                <!--<form class="form-horizontal" action="#">-->
									<div class="modal-body">
									 
								<fieldset class="content-group">
									<legend class="text-bold"  style="font-size:20px;">สร้าง Flow เพื่อใช้ในการอนุมัติ  (Design Approve Flow)</legend>



									<div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>ระบุชื่อ Flow </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="flow_name" id="flow_name" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)" class="form-control" placeholder="ชื่อ Flow ของท่าน" value="<?php echo $getthisflow->flow_name;?>">
                                            
                                             <span class="help-block label  text-info-600">*ชื่อนี้จะปรากฎและถูกอ้างถึงเมื่อท่านสร้าง รหัสผ่านชั่วคราวให้กับคู่ค้าใหม่ ชื่อ Flow ที่แนะนำ เช่น : Flow 1 Support Team</span>
										</div>
									</div>
                                    
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>โปรดเลือก กลุ่มงาน</strong></label>
			                        	<div class="col-lg-10 ">
                                        
                                      
				                            <select  class="form-control" name="flow_type_job" id="flow_type_job" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)">
                                            <?php
                                           
												
												if(!empty($newselectshow)){
													foreach($newselectshow as $newselectshows){
														
														?>
                                                        <option <?php if($getthisflow->flow_type_job ==$newselectshows ){ echo "selected"; }?>  value="<?php echo $newselectshows?>"><?php echo check_flow_type_job($newselectshows)?></option>
                                                        <?php
														
														}
													
													
													}
												
											?>
				                                
				                             
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น</span>
                                            
			                            </div>
			                        </div>
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>ประเภท ของคู่ค้า</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_type_vendor" id="flow_type_vendor" class="form-control" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)">
                                            <?php
                                            
											if(!empty($newselectshow_vendortype)){
													foreach($newselectshow_vendortype as $newselectshow_vendortypes){
														
														?>
                                                        <option <?php if($getthisflow->flow_type_vendor ==$newselectshow_vendortypes ){ echo "selected"; }?> value="<?php echo $newselectshow_vendortypes?>"><?php echo check_flow_type_vendor($newselectshow_vendortypes)?></option>
                                                        <?php
														
														}
													
													
													}
												
											
											?>
                                            
                                            
				                              
                                                
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น</span>
                                            
			                            </div>
			                        </div>
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>ผู้อนุมัติระดับหัวหน้า (MPR)</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_mpr_id" id="flow_mpr_id" class="form-control" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)" >
				                                    <?php
                                            
											if($new_mpr_role_ref_ids){
																foreach($new_mpr_role_ref_ids as $mprname){
																	
																	$querymprname =  $this->Query_Db->record($this->db->dbprefix.'users','id ='.$mprname.'');
																	
																	?>
                                                                    <option <?php if($getthisflow->flow_mpr_id ==$querymprname->username ){ echo "selected"; }?> value="<?php echo $querymprname->username;?>">คุณ <?php echo $querymprname->fullname_th;?> ( <?php echo $querymprname->fullname_en;?> ) </option>
                                                                    <?php
																	
																	}
																
																}
																
											?>
                                            
				                             
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น ผู้อนุมัติใน Role นี้ จะอยู่ในระดับ หัวหน้าจัดซื้อ เท่านั้น  </span>
                                            
			                            </div>
			                        </div>
                                    
                              <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>ผู้อนุมัติระดับ (AVP)</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_avp_id" id="flow_avp_id" class="form-control" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)">
				                                
                                                   <?php
                                            
											if($new_avp_role_ref_ids){
																foreach($new_avp_role_ref_ids as $avpname){
																	
																	$queryavpname =  $this->Query_Db->record($this->db->dbprefix.'users','id ='.$avpname.'');
																	
																	?>
                                                                    <option <?php if($getthisflow->flow_avp_id ==$queryavpname->username ){ echo "selected"; }?> value="<?php echo $queryavpname->username;?>">คุณ <?php echo $queryavpname->fullname_th;?> ( <?php echo $queryavpname->fullname_en;?> ) </option>
                                                                    <?php
																	
																	}
																
																}
																
											?>
                                            
                                                
                                                
                                              
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น ผู้อนุมัติใน Role นี้ จะอยู่ในระดับ AVP เท่านั้น  </span>
                                            
			                            </div>
			                        </div>
                              
                              </fieldset>
                              
                              <fieldset class="content-group">
									<legend class="text-bold" style="font-size:20px;">ข้อมูลการ ตรวจสอบจากบัญชี</legend>

									      
                                    
                                    <div class="form-group">
                                    
                                    
                                    <label class="control-label col-lg-2"><strong>สถานะการตรวจจากบัญชี</strong></label>
			                        	<div class="col-lg-10 ">
				                            <div class="radio-inline">
											<label>
												 <input type="radio" name="flow_acc_st" <?php if($getthisflow->flow_acc_st ==0 ){ echo "checked"; }?>  value="0" class="styled" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)" >
												แจ้งเพื่อรับทราบ
											</label>
										</div>
                                       
                                        <div class="radio-inline">
											<label>
												 <input type="radio" name="flow_acc_st" <?php if($getthisflow->flow_acc_st ==1 ){ echo "checked"; }?>  value="1" class="styled" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)" >
												แจ้งเพื่ออนุมัติ
											</label>
										</div>
                                            
                                            <br>
                                            
                                            <span class="help-block help-block label  text-warning-600">การ แจ้งเพื่อรับทราบ เป็นการแจ้งเพื่อให้ทางบัญชีทราบว่า คู่ค้ารายใหม่กำลังจะทำการซื้อขายกับ TOA ส่วนการ แจ้งเพื่ออนุมัติ<br> จะเป็นแจ้งเพื่อให้ทำการอนุมัติก่อน ทำการซื้อขายกับ TOA  </span>
                                            
			                            </div>
                                    
                                    	
                                    
			                        	<label class="control-label col-lg-2"><strong>ผู้ตรวจสอบ (ACC)</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_acc_id" id="flow_acc_id" class="form-control" onchange="noti(this.name,this.value,<?php echo $getthisflow->flow_id;?>)">
                                            
                                            
                                            <?php
                                            
											if($new_acc_role_ref_ids){
																foreach($new_acc_role_ref_ids as $accname){
																	
																	$queryaccname =  $this->Query_Db->record($this->db->dbprefix.'users','id ='.$accname.'');
																	
																	?>
                                                                    <option <?php if($getthisflow->flow_acc_id ==$queryaccname->username ){ echo "selected"; }?> value="<?php echo $queryaccname->username;?>">คุณ <?php echo $queryaccname->fullname_th;?> ( <?php echo $queryaccname->fullname_en;?> ) </option>
                                                                    <?php
																	
																	}
																
																}
																
											?>
                                            
				                                
				                             
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น ผู้อนุมัติใน Role นี้ จะอยู่ในระดับ ACC เท่านั้น  </span>
                                            
			                            </div>
			                        </div>
                                    
                                    
                                    
                                  
                                     
          

			
            
            </fieldset>
            
           
                                        
                                  
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										
                                       
                                        
                                         <button type="submit" class="btn btn-primary">บันทึก Flow การ Approve
                                     <i class="icon-arrow-right14 position-right"></i>
                                     </button>
										  
                                        
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->
                                               
                                                <?php
												//echo $showflows->flow_id;
												
												}
											}
										
										
										
										
										}
									
								  }
							
							  
							
								  
                               
							
							   ?>
                               
                                
                              
						
                                
                                
									
								</div>
							</div>
                        
                        
                        
                 

		         




					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
   

  <!-- Vertical form modal -->
					<div id="modal_form_acc" class="modal fade">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i>  Flow เพื่อใช้ในการอนุมัติ </h5>
								</div>

								
                                
                                  <form class="form-validation form-validate-jquery" id="add_editflow" action="#"  method="post">
                                  
                                <!--<form class="form-horizontal" action="#">-->
									<div class="modal-body">
									 
								<fieldset class="content-group">
									<legend class="text-bold"  style="font-size:20px;">สร้าง Flow เพื่อใช้ในการอนุมัติ  (Design Approve Flow)</legend>



									<div class="form-group"> 
                                   
									
                                        <label class="control-label col-lg-2"><strong>ระบุชื่อ Flow </strong></label>
										<div class="col-lg-10  has has-feedback">
											<input type="text"  name="flow_name_m" id="flow_name_m" class="form-control" placeholder="ชื่อ Flow ของท่าน" value="">
                                            
                                             <span class="help-block label  text-info-600">*ชื่อนี้จะปรากฎและถูกอ้างถึงเมื่อท่านสร้าง รหัสผ่านชั่วคราวให้กับคู่ค้าใหม่ ชื่อ Flow ที่แนะนำ เช่น : Flow 1 Support Team</span>
										</div>
									</div>
                                    
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>โปรดเลือก กลุ่มงาน</strong></label>
			                        	<div class="col-lg-10 ">
                                        
                                      
				                            <select  class="form-control" name="flow_type_job_m" id="flow_type_job_m">
                                            <?php
                                           
												
												if(!empty($newselectshow)){
													foreach($newselectshow as $newselectshows){
														
														?>
                                                        <option value="<?php echo $newselectshows?>"><?php echo check_flow_type_job($newselectshows)?></option>
                                                        <?php
														
														}
													
													
													}
												
											?>
				                                
				                             
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น</span>
                                            
			                            </div>
			                        </div>
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>ประเภท ของคู่ค้า</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_type_vendor_m" id="flow_type_vendor_m" class="form-control">
                                            <?php
                                            
											if(!empty($newselectshow_vendortype)){
													foreach($newselectshow_vendortype as $newselectshow_vendortypes){
														
														?>
                                                        <option value="<?php echo $newselectshow_vendortypes?>"><?php echo check_flow_type_vendor($newselectshow_vendortypes)?></option>
                                                        <?php
														
														}
													
													
													}
												
											
											?>
                                            
                                            
				                              
                                                
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น</span>
                                            
			                            </div>
			                        </div>
                                    
                                    <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>ผู้อนุมัติระดับหัวหน้า (MPR)</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_select_mpr_m" id="flow_select_mpr_m" class="form-control">
				                                    <?php
                                            
											if($new_mpr_role_ref_ids){
																foreach($new_mpr_role_ref_ids as $mprname){
																	
																	$querymprname =  $this->Query_Db->record($this->db->dbprefix.'users','id ='.$mprname.'');
																	
																	?>
                                                                    <option value="<?php echo $querymprname->username;?>">คุณ <?php echo $querymprname->fullname_th;?> ( <?php echo $querymprname->fullname_en;?> ) </option>
                                                                    <?php
																	
																	}
																
																}
																
											?>
                                            
				                             
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น ผู้อนุมัติใน Role นี้ จะอยู่ในระดับ หัวหน้าจัดซื้อ เท่านั้น  </span>
                                            
			                            </div>
			                        </div>
                                    
                              <div class="form-group">
			                        	<label class="control-label col-lg-2"><strong>ผู้อนุมัติระดับ (AVP)</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_select_avp_m" id="flow_select_avp_m" class="form-control">
				                                
                                                   <?php
                                            
											if($new_avp_role_ref_ids){
																foreach($new_avp_role_ref_ids as $avpname){
																	
																	$queryavpname =  $this->Query_Db->record($this->db->dbprefix.'users','id ='.$avpname.'');
																	
																	?>
                                                                    <option value="<?php echo $queryavpname->username;?>">คุณ <?php echo $queryavpname->fullname_th;?> ( <?php echo $queryavpname->fullname_en;?> ) </option>
                                                                    <?php
																	
																	}
																
																}
																
											?>
                                            
                                                
                                                
                                              
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น ผู้อนุมัติใน Role นี้ จะอยู่ในระดับ AVP เท่านั้น  </span>
                                            
			                            </div>
			                        </div>
                              
                              </fieldset>
                              
                              <fieldset class="content-group">
									<legend class="text-bold" style="font-size:20px;">ข้อมูลการ ตรวจสอบจากบัญชี</legend>

									      
                                    
                                    <div class="form-group">
                                    
                                    
                                    <label class="control-label col-lg-2"><strong>สถานะการตรวจจากบัญชี</strong></label>
			                        	<div class="col-lg-10 ">
				                            <div class="radio-inline">
											<label>
												 <input type="radio" name="flow_approve_acc_m"   checked   value="0" class="styled" >
												แจ้งเพื่อรับทราบ
											</label>
										</div>
                                       
                                        <div class="radio-inline">
											<label>
												 <input type="radio" name="flow_approve_acc_m"  value="1" class="styled" >
												แจ้งเพื่ออนุมัติ
											</label>
										</div>
                                            
                                            <br>
                                            
                                            <span class="help-block help-block label  text-warning-600">การ แจ้งเพื่อรับทราบ เป็นการแจ้งเพื่อให้ทางบัญชีทราบว่า คู่ค้ารายใหม่กำลังจะทำการซื้อขายกับ TOA ส่วนการ แจ้งเพื่ออนุมัติ<br> จะเป็นแจ้งเพื่อให้ทำการอนุมัติก่อน ทำการซื้อขายกับ TOA  </span>
                                            
			                            </div>
                                    
                                    	
                                    
			                        	<label class="control-label col-lg-2"><strong>ผู้ตรวจสอบ (ACC)</strong></label>
			                        	<div class="col-lg-10 ">
				                            <select name="flow_select_acc_m" id="flow_select_acc_m" class="form-control">
                                            
                                            
                                            <?php
                                            
											if($new_acc_role_ref_ids){
																foreach($new_acc_role_ref_ids as $accname){
																	
																	$queryaccname =  $this->Query_Db->record($this->db->dbprefix.'users','id ='.$accname.'');
																	
																	?>
                                                                    <option value="<?php echo $queryaccname->username;?>">คุณ <?php echo $queryaccname->fullname_th;?> ( <?php echo $queryaccname->fullname_en;?> ) </option>
                                                                    <?php
																	
																	}
																
																}
																
											?>
                                            
				                                
				                             
				                              
				                            </select>
                                            
                                            
                                            
                                            <span class="help-block help-block label  text-info-600">ระบบจะแสดงเฉพาะ Permission ที่ท่านได้รับจากระบบเท่านั้น ผู้อนุมัติใน Role นี้ จะอยู่ในระดับ ACC เท่านั้น  </span>
                                            
			                            </div>
			                        </div>
                                    
                                    
                                    
                                  
                                     
          

			
            
            </fieldset>
            
           
                                        
                                  
                                        
                                        
                                        
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										
                                       
                                        
                                         <button type="submit" class="btn btn-primary">บันทึก Flow การ Approve
                                     <i class="icon-arrow-right14 position-right"></i>
                                     </button>
										  
                                        
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


 
<script type="text/javascript">

// Custom tag class

    $('.tokenfield').tokenfield();
    $('.tagsinput-custom-tag-class').tagsinput({
        tagClass: function(item){
            return 'label bg-success';
        }
    });
	
	    // Display values
    $('.tags-input, [data-role="tagsinput"], .tagsinput-max-tags, .tagsinput-custom-tag-class').on('change', function(event) {
        var $element = $(event.target),
            $container = $element.parent().parent('.content-group');

        if (!$element.data('tagsinput'))
        return;

        var val = $element.val();
        if (val === null)
        val = "null";
    
        $('pre.val > code', $container).html( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
        $('pre.items > code', $container).html(JSON.stringify($element.tagsinput('items')));
        Prism.highlightAll();
    }).trigger('change');





	function deleteflow(thisid){
   //alert(thisid);
   
   
swal({
		  title: "คุณต้องลบ Flow นี้หรือไม่ ? ",
            text: "โปรดตรวจสอบ Flow ของท่านอีกครั้ง เพื่อให้แน่ใจว่าท่านต้องการลบ Flow การทำงาน ",
            type: "error",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "ใช่ ฉันต้องการ ลบ Flow นี้ ออกจากระบบ",
			cancelButtonText: "ไม่!! ฉันแค่กดผิดเข้ามาเฉยๆ ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "Flow ของท่านถูกลบออกจากระบบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
					
				$.ajax({
					
			url: "../toa_editflow",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: thisid,
					flow_name 	: 	'flow_st',
					flow_val	: 	0,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				//alert(data);
				
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='../approveflow/';
  }  , 1000 );
				
				
				}
            }
        });
				
			}
		});		
					
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ Flow",
                    text: "คุณได้กดยกเลิกการลบ Flow",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });

   
   
}


	function noti(name,val,id) {
	
	
	
	$.ajax({
					
			url: "../toa_editflow",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					flow_id		: id,
					flow_name 	: 	name,
					flow_val	: 	val,
					//name	: 	permission,
					//val	: 	val,
					
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				//alert (data);
				new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
			
				
			}
		});	
		

	
	
    //alert("The input value has changed. The new value is: " + val);
} 



//end noti

	 $('#pnotify-successx').on('click', function () {
        new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
    });
	
	

	
	
 // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();




    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });


	

    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



 var validatorst1 = $("#add_editflow").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 1
            }, 
			flow_approve_acc: {
                minlength: 1
            },
			
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "คุณต้องการบันทึก Flow ลงระบบหรือไม่ ? ",
            text: "โปรดตรวจสอบ Flow ของท่านอีกครั้ง เพื่อให้แน่ใจว่า Flow การทำงานถูกต้อง ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
					
				$.ajax({
					
			url: "../toa_updateflow",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					flow_name	: 	$('#flow_name_m').val(),
					flow_type_job	: 	$('#flow_type_job_m').val(),
					flow_type_vendor	: 	$('#flow_type_vendor_m').val(),
					flow_select_mpr	: 	$('#flow_select_mpr_m').val(),
					flow_select_avp	: 	$('#flow_select_avp_m').val(),
					
					flow_select_acc	: 	$('#flow_select_acc_m').val(),
					flow_select_pr : '<?php echo $getuser->username;?>',
					//flow_approve_acc	: 	$('#flow_approve_acc').val(),
					
					flow_approve_acc	: 	$('[name="flow_approve_acc_m"]:checked').val(),
					
					
					flow_mode	: 	'addflow',
					
					//role	: 	val,
					//value	: 	newval,
					
					
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				//alert(data);
				
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='../approveflow/';
  }  , 1000 );
				
				
				}
            }
        });
				
			}
		});		
					
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "คุณได้กดยกเลิกการบันทึกข้อมูล",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		/*	
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		*/
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
    });

 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


	
   // With validation
   
	
 $(window).on('load',function(){
	 
	 
	 /*swal({
		 
		  title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบระหว่างนี้ท่านจะไม่สามารถแก้ไข ข้อมูลใดๆ ได้ จนกว่าทางเจ้าหน้าที่ของ TOA GROUP ตรวจสอบข้อมูลเสร็จ",
            confirmButtonColor: "#66BB6A",
            type: "success",
			confirmButtonText: "ตกลง",
		 
			
			   title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบ",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        });*/
	 
	// $('#modal_theme_primary').modal('show');
	 //validation-next
	/* $('#validation-next').click(function(){
		 $('#modal_theme_primary').modal('show');
		 });*/
       // $('#modal_theme_primary').modal('show');
	  
	 /* bootbox.dialog({
                title: "กรุณาระบุรหัสผ่าน",
                message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                        '<form class="form-horizontal">' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">Name</label>' +
                                '<div class="col-md-8">' +
                                    '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                                    '<span class="help-block">Here goes your name</span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">How awesome is this?</label>' +
                                '<div class="col-md-8">' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked">' +
                                            'Really awesomeness' +
                                        '</label>' +
                                    '</div>' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                                            'Super awesome' +
                                        '</label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '</div>',
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            bootbox.alert("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        ); */
    });
	
	 
	/*
	 $('#validation-next').on('click', function() {
        
		  swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
    });*/
	
	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });

 // Custom bootbox dialog with form
   

</script>


</body>




</html>