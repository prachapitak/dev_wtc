<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs2');?>
      


<!-- /theme JS files -->



   <!-- Theme JS files for moodle -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/notifications/sweet_alert.min.js"></script>
	
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/components_modals.js"></script>


<!-- Theme JS files notification -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/notifications/pnotify.min.js"></script>

	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/components_notifications_pnotify.js"></script>

	

  
    
    <!-- Theme JS files tag input -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/tags/tagsinput.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/tags/tokenfield.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>

	
	<!-- /theme JS files -->
    
    
    
	<!-- /theme JS files -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_basic.js"></script>


	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->
    
    
    
    
    
 
      
      	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/sliders/ion_rangeslider.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment_locales.min.js"></script>

	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/extra_sliders_ion.js"></script>

	

	

	<!-- /theme JS files -->
    
   

 

</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
								<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">Approve Vendor  </span> - ตรวจสอบข้อมูลคู่ค้า </h4>

						</div>
                        
                        <?php //print_r($this->session->userdata());?>
                        <?php print_r($getvendorprofile);?>
                        
                        
                        <?php $this->load->view('dashboard/toa/headnoti');?>

						
					</div>

					
                    
                    
                    
				</div>
				<!-- /page header -->
                
                
                <?php //print_r($this->session->userdata());?>
                
                

<?php //$this->load->view('dashboard/toa/noti');?>
					
				<!-- Cover area -->
				<div class="profile-cover">
					<div class="profile-cover-img" style="background-image: url(<?php echo base_url(); ?>assets/images/cover.jpg)"></div>
					<div class="media">
						<div class="media-left">
							<a href="#" class="profile-thumb">
								<img src="<?php echo base_url(); ?>assets/images/placeholder.jpg" class="img-circle" alt="">
							</a>
						</div>

						<div class="media-body">
				    		<h1>	
บริษัท นิวส์เวนเดอร์01 จำกัด (มหาชน) <small class="display-block">ผลิตและจำหน่ายสารเคมีทำความสะอาดอุปกรณ์ทำความสะอาดทุกชนิด </small> <small class="display-block">โทร 02 3456789 </small></h1>
						</div>

						<div class="media-right media-middle">
							<ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap">
								<li><a href="#" class="btn btn-default"><i class="icon-printer position-left"></i> Print Profile</a></li>
                                
								<li><a href="#" class="btn btn-default"><i class="icon-users4 position-left"></i> Show All Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /cover area -->
				
                       <!-- Toolbar -->
				<div class="navbar navbar-default navbar-xs content-group">
					<ul class="nav navbar-nav visible-xs-block">
						<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
					</ul>

					
				</div>
				<!-- /toolbar -->             

					<!-- Content area -->
				<div class="content">
                <!-- User profile -->
					<div class="row">
						<div class="col-lg-9">
							<div class="tabbable">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="activity">

										<!-- Timeline -->
										<div class="timeline timeline-left content-group">
											<div class="timeline-container">

												
                                                
                                                
                                                
                                                <!-- Sales stats -->
												<div class="timeline-row">
													<div class="timeline-icon">
														<a href="#"><img src="<?php echo base_url(); ?>assets/images/placeholder.jpg" alt=""></a>
													</div>

													<div class="panel panel-flat timeline-content">
														<div class="panel-heading">
															<h6 class="panel-title">ข้อมูลทั่วไปของบริษัท</h6>
															
														</div>

														<div class="panel-body">
															
  <!-- Top component -->
					<div class="content-group border-top-lg border-top-primary">
						<div class="page-header page-header-default pt-20">
							
                            
                          

							<div class="page-header-content">
                            
                            
                            
								
                                <div class="page-title">
								
                                
                                
                                
                                     <div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label><strong>ชื่อผู้ค้า</strong> : <span class="text-danger">*</span></label>
											<input type="text" name="name" class="form-control required" placeholder="Company Name " value="Newvendor (Thailand) Co.,Ltd." readonly >
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label><strong>Email address</strong>: <span class="text-danger">*</span></label>
											<input type="email" name="email" class="form-control required" placeholder="your@email.com" value="supportsale@newvendor.co.th" readonly>
										</div>
									</div>
								</div>
                                <div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label><strong>เบอร์โทรติดต่อ</strong> : <span class="text-danger">*</span> </label>
											<input type="text" name="tel" class="form-control" placeholder="+662-345-6789" data-mask="+999-999-9999" value="02-3456789" readonly>
										</div>
									</div>

									<div class="col-md-6">
                                    
                                    <div class="form-group">
											<label><strong>เบอร์ติดต่อ กรณีฉุกเฉิน:</strong> <span class="text-danger">*</span></label>
											<input type="text" name="tel" class="form-control" placeholder="+662-345-6789" data-mask="+999-999-9999" value="091-2345678" readonly>
										</div>
                                
									</div>
								</div>
                                
                                <div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label><strong>เบอร์โทรสาร :</strong> <span class="text-danger">*</span> </label>
											<input type="tel" name="tel" class="form-control" placeholder="+662-345-6789" value="034-2212233" readonly  >
										</div>
									</div>

									<div class="col-md-6">
                                    
                                    <div class="form-group">
											<label><strong>Website :</strong> </label>
											<input type="text" name="tel" class="form-control" placeholder="eg : www.toagroup.com"  value="www.newvendor.com" readonly>
										</div>
                                
									</div>
								</div>
                                
                                
                                <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label><strong>ที่อยู่ตาม (หนังสือรับรองบริษัท) :</strong> <span class="text-danger">*</span></label>
											<input type="text" name="name" class="form-control required" placeholder="ระบุที่อยู่ตามหนังสือรับรอง" value="
สำนักงาน และศูนย์อุตสาหกรรม ทีโอเอ บางนา-ตราด 
31/2 หมู่ 3 ถนนบางนา-ตราด ตำบลบางเสาธง อ.บางเสาธง 
จ.สมุทรปราการ 10570"  readonly >
										</div>
									</div>

									
								</div>
                                
                                
                   
                                   <div class="row">
                                <div class="form-group">
										<label class="control-label col-lg-12"><strong>ที่อยู่ที่ติดต่อได้</strong>  : <span class="text-danger">*</span></label>
										
									</div>
                                
                                </div>
                        
                                <div class="row">
                                
                                
                             
                                
									<div class="col-md-12">
                                    
										<div class="form-group">
                                     		
											<input type="text" name="name" class="form-control required" placeholder="ระบุ ที่อยู่ที่ติดต่อได้" value="
สำนักงาน และศูนย์อุตสาหกรรม ทีโอเอ บางนา-ตราด 
31/2 หมู่ 3 ถนนบางนา-ตราด ตำบลบางเสาธง อ.บางเสาธง 
จ.สมุทรปราการ 10570 "  readonly >
										</div>
									</div>

									
								</div>
                          
                                
                                <div class="row">
                                
                                
                                <div class="col-md-12">
                                <!-- Styled checkbox group -->
									<div class="form-group">
										<label class="control-label col-lg-12"><strong>ประเภทกิจการ</strong> <span class="text-danger">*</span></label>
										<div class="col-lg-12">
                                        
                                        <div class="col-lg-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="styled_checkbox" class="styled" required="required"   checked disabled >
													โรงงาน
												</label>
											</div>
										</div>
                                        <div class="col-lg-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="styled_checkbox" class="styled" checked disabled >
													ซื้อมาขายไป
												</label>
											</div>
										</div>
                                        <div class="col-lg-4">
                                        	<div class="input-group">
												<span class="input-group-addon">
													<input type="checkbox" name="styled_checkbox" class="styled"  checked  disabled >
												</span>
												<input type="text" class="form-control" placeholder="อื่นๆ โปรดระบุ "  value="บริการ"  readonly>
											</div>
                                        </div>
										<!--	<div class="checkbox">
												<label class="checkbox-inline">
													<input type="checkbox" name="styled_checkbox" class="styled"> 
                                                    </label>
                                                    <label class="checkbox-inline" style="    margin-left: -20px;    padding-left: 18px;">
													<input type="text" name="tel" class="form-control" placeholder="อื่นๆ โปรดระบุ  " >
												</label>
											</div>-->
										</div>
									</div>
									<!-- /styled checkbox group -->
                                    
                                    </div>
                                    
                                       <div class="col-md-12">
                                       <div class="form-group">
											<label><strong>เริ่มประกอบกิจการตั้งแต่</strong> : </label>
                                            
											
                                            
											<div class="row">
                                            
                                            <div class="col-md-2">
												<div class="form-group">
													<input type="text" name="tel" class="form-control" placeholder="เงินทุนประมาณ"   value=" ปี 1990" readonly>
												</div>
											</div>
                                            
                                            
											

                                            <div class="col-md-3">
												<div class="form-group">
                                                
                                                <input type="text" name="tel" class="form-control" placeholder="เงินทุนประมาณ"   value="เงินทุน : 320,000,000 บาท" readonly>
													
												</div>
											</div>
											

											
                                            
                                            <div class="col-md-4">
												<div class="form-group">
                                                
                                                  <input type="text" name="tel" class="form-control" placeholder="ยอดจำหน่ายต่อปี"  value="ยอดจำหน่ายต่อปี 30,000,000,000 บาท"  readonly>
													
												</div>
											</div>
                                            
                                            <div class="col-md-3">
												<div class="form-group">
                                                
                                                <input type="text" name="tel" class="form-control" placeholder="จำนวนพนักงาน"   value="จำนวนพนักงาน : 2000 คน" readonly >
													
												</div>
											</div>
										</div>
                                        
                                        <div class="row">
                                            
                                            <div class="col-md-12">
												<div class="form-group">
                                                
                                                <input type="text" name="tel" class="form-control" placeholder="ชื่อผู้จัดการใหญ่ / เจ้าของกิจการ" value="เจ้าของกิจการ : คุณนิวส์เวนเดอร์ ทรัพย์มั่นคั่งมหาศาล"  readonly >
													
												</div>
											</div>
                                            
                                            
											

											

											
                                            
                                            
										</div>
                                        
										</div>
                                       
                               
                                    
                                    </div>
                                    
                                </div>
                                 
                                 
                                <div class="row">
                                
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>เอกสารการจดทะเบียนพาณิชย์ / ทะเบียนการค้า </strong><span class="text-danger">*</span>  : </label>
											
                                            <br>
                                            
                                            <a href="https://www.dbd.go.th/download/downloads/01_tp/sample_tp_new.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดูสำเนาทะเบียนการค้า</a>
		                                  
                                            
													
                                                   
                                                    
                                                    
		                                </div>
                                </div>
                                
                                  <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>เอกสารทะเบียนภาษีมูลค่าเพิ่ม (ภพ.20)</strong> <span class="text-danger">*</span>  : </label>
											
                                            <br>
                                            
		                                        <a href="https://www.dbd.go.th/download/downloads/01_tp/sample_tp_new.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดูสำเนาเอกสารทะเบียนภาษีมูลค่าเพิ่ม</a>
		                                  
                                            
												
                                                   
                                                    
                                                    
		                                </div>
                                </div>
                                
                                
                                
                                </div>
                                
                                
                                
							</div>

								
							</div>
						</div>
					</div>
					<!-- /top component --> 
                    
                    
                    <div class="row">
                                 <div class="col-md-12">
                                 
                                 
                                 
                                 
                                 
                                 <div class="content-group border-top-lg border-top-primary">
						<div class="page-header page-header-default pt-20">
							

							<div class="page-header-content">
								<div class="page-title">
								
                                <h6>ข้อมูลบัญชีธนาคาร  / เช็ค</h6>
                                
                                
                                 
                                 <table class="table">
							<thead>
								<tr>
                                	<th>ชื่อบัญชี</th>
                                    <th>เลขที่</th>
									<th>ชื่อธนาคาร/สาขา</th>
                                    <th>ผู้ดูแลบัญชี</th>
									<th>Action</th>
									
									
								</tr>
							</thead>
							<tbody>
                            <tr>
									<td>ประสบโชค รักชาติ </td>
									<td>1112223344</td>
									<td>กรุงเทพ/บางบ่อ</td>
                                    
                                    <td>คุณ สมร รักการเงิน</td>
									<td>
                                    <button type="button" class="label label-success" data-toggle="modal" data-target="#modal_form_acc">View <i class="icon-eye position-right"></i></button>
                                    
                                    
                                    </td>
									
									
								</tr>
                                <tr>
									<td>รุ่งเรือง ทรัพย์มากมายมหาศาล </td>
									<td>11245765432</td>
									<td>กสิกร/บางบ่อ</td>
                                    
                                    <td>คุณ สมร รักการเงิน</td>
									<td> <button type="button" class="label label-success" data-toggle="modal" data-target="#modal_form_acc">View <i class="icon-eye position-right"></i></button> </td>
									
									
								</tr>
                            </tbody>
						</table>
                                
                                
                                
							</div>
						</div>
					</div>
                                 
                                
                        </div>
								</div>
                                
                                	
                               </div>
															

															<blockquote>
																<p>เมื่อท่าน ตรวจสอบเสร็จแล้วหน้าจอด้านขวาจะมี Dialog ให้ท่านบันทึกผลคะแนน ท่านสามารถคลิ๊กเลือกผลคะแนน โดยเริ่มจาก 0 - 10 เพื่อส่งให้กับผู้อนุมัติในลำดับถัดไป</p>
																<footer>System, <cite title="Source Title"></cite></footer>
															</blockquote>
														</div>

														
													</div>
												</div>
												<!-- /sales stats -->
                                                
                                                
                                                
                                                
                                                
                                                


												<!-- Blog post -->
												<div class="timeline-row">
													<div class="timeline-icon">
														<img src="<?php echo base_url(); ?>assets/images/placeholder.jpg" alt="">
													</div>

													<div class="panel panel-flat timeline-content">
														<div class="panel-heading">
															<h6 class="panel-title">ข้อมูลสินค้า และบริการ</h6>
															
														</div>

														<div class="panel-body">
															
        
                               
                               	<!-- Top component -->
					<div class="content-group border-top-lg border-top-primary">
						<div class="page-header page-header-default pt-20">
							<div class="breadcrumb-line breadcrumb-line-component">
								<ul class="breadcrumb">
									<li><i class="icon-copy position-left"></i>  รายชื่อลูกค้าที่มีการซื้อขายกับบริษัทของท่าน โดยท่านจะต้องระบุ อย่างน้อย 3 รายชื่อ</li>
									
								</ul>

								
							</div>

							<div class="page-header-content">
								
                                <div class="page-title">
								
                                
                                
                                  <div class="form-group">
										
										<div class="input-group ">
											<div class="input-group-addon"><i class=" icon-users4"></i></div>
											<input type="text"  style="min-width: 300px;" placeholder="" value="Adobe Crop,Microsoft,AUtodesk" class="tagsinput-custom-tag-class"   disabled > 
										</div>
                                        
                                       
									</div>
                                 
                                 
                                
                                
                                
							</div>

								
							</div>
						</div>
					</div>
					<!-- /top component -->  
                               
                                
                                
                                
                                   
                                
                                <div class="row">
                                
                                <div class="col-md-12">
                                 
                                 
                                 
                                 
                                 
                                 <div class="content-group border-top-lg border-top-primary">
						<div class="page-header page-header-default pt-20">
							<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
								<ul class="breadcrumb">
                                
                                
                                <li><i class="icon-copy position-left"></i>   ข้อมูลสินค้าของบริษัท     </li>
                                
                                
									
								</ul>

								
							</div>

							<div class="page-header-content">
								<div class="page-title">
								
                                
                                 
                                 
                                <table class="table datatable-basic">
							<thead>
								<tr>
									<th>ชื่อผู้ผลิต</th>
									<th>สินค้าที่จำหน่าย</th>
									<th>ประเทศที่ผลิต</th>
									<th>Minimum Order</th>
                                    <th>Contact</th>
									<th>Action</th>
									
								</tr>
							</thead>
							<tbody>
                            <?php 
							function randomName() {
   
   
   $names = array(
   'Major',
'Serve',
'Think',
'PageOne',
'Activate',
'CountryClub',
'Benefits',
'Connect',
'Fund',
'Pirates',

   );
   
   /* $names = array(



'Endeavor Co.,ltd',
'Wiz Co.,ltd',
'Crawler Co.,ltd',
'Guru Co.,ltd',
'Trick Co.,ltd',
'Universe Co.,ltd',
'Trilogy Co.,ltd',
'Insta Co.,ltd',
'Wisdom Co.,ltd',
'Welcome Co.,ltd',
'SweetWater Co.,ltd',
'Resource Co.,ltd',
'RedSky Co.,ltd',
'Leaf Co.,ltd',
'Bumblebee Co.,ltd',
'Glass Co.,ltd',



    );*/
    return $names[rand ( 0 , count($names) -1)];
}

function randomName2() {
   
   
   $names = array(
   'Jonh Crop',
'Mocrosoft',
'Adobe',
'Autodesk',


   );
   
 
    return $names[rand ( 0 , count($names) -1)];
}

for($i=0;$i<22;$i++){
	?>
    <tr>
									<td><?php echo randomName2();?></td>
									<td><?php echo randomName();?></td>
									<td>United Stage of America</td>
									<td>10T</td>
                                    <td>สมชาย 	T. 0912345678</td>
									<td>
                                    
                                    <button type="button" class="label label-success" data-toggle="modal" data-target="#modal_form_vertical">View <i class="icon-eye position-right"></i></button>
                                    
                                   </td>
									
								</tr>
    <?php
	}
							
							?>
                            
                            
                              
                            </tbody>
						</table>
                                
                                
							</div>
						</div>
					</div>
                                 
                                
                        </div>
                                
                                 
								</div>
                                
                                
                                </div>
                                
                                
                                <div class="row">
                               
                                <div class="col-md-6">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">รายการสินค้า  ( Catalog)<span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	สำหรับสินค้าเพิ่มเติม และ สินค้าใน Catalog </p>
								
								<div class="form-group">
                                    
											
                                        
                                            
		                                    <a href="https://www.hunterindustries.com/sites/default/files/hunter_catalog_dom.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดูรายการสินค้า ( Catalog) </a>
                                            
													
                                                   
                                                    
                                                    
		                                </div>
                                
                                
                                    
                                     
                                       
                                      
                                        
                                        
                               
                                
                                
			                    
							</div>
                               </div>
                               
                               <div class="col-md-6">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">รายงาน ข้อมูล Heavy Metal <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	รายงานข้อมูลเกี่ยวกับสารโลหะหนักในสินค้าของท่าน</p>
								
								<div class="form-group">
                                    
											
                                           <a href="http://ec.europa.eu/environment/waste/studies/pdf/heavy_metalsreport.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดู Heavy Metal Report</a>
                                                    
                                                    
		                                </div>
                                
                                
			                    
							</div>
                               </div>
                               
                               </div>
                                
                            
                                    <div class="row">
                                 <div class="col-md-12">
                                 
                                 
                                 
                                 
                                 
                                 <div class="content-group border-top-lg border-top-primary">
						<div class="page-header page-header-default pt-20">
							<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
								
                                <ul class="breadcrumb">
                                
                                
                                <li><i class="icon-copy position-left"></i>  ข้อมูลฝ่ายขายของบริษัท    </li>
                                
                                
									
								</ul>

								
							</div>

							<div class="page-header-content">
								<div class="page-title">
								
                                
                                
                        
                                 
                                 
                                 <table class="table datatable-basic">
							<thead>
								<tr>
									<th>Full Name</th>
									<th>Tel.</th>
									<th>Email</th>
									<th>Position</th>
									<th>Language</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
                            <tr>
									<td>Marth Jackelyn </td>
									<td>0812345678</td>
									<td>sale1@newvendor.com</td>
									<td>Sale Directer</td>
									<td>ENGLISH</td>
									<td class="text-center">
                                    
                                    
                                    <button type="button" class="label label-success" data-toggle="modal" data-target="#modal_form_vertical2">View <i class="icon-eye position-right"></i></button>
										
									</td>
								</tr>
                                <tr>
									<td>Jackelyn Marth  </td>
									<td>08112345678</td>
									<td>sale2@newvendor.com</td>
									<td>Senior Sale</td>
									<td>THAILAND</td>
									<td class="text-center">
										 <button type="button" class="label label-success" data-toggle="modal" data-target="#modal_form_vertical2">View <i class="icon-eye position-right"></i></button>
									</td>
								</tr>
                            </tbody>
						</table>
                                
                                
                                
							</div>
						</div>
					</div>
                                 
                                
                        </div>
								</div>
                                
                                	
                               </div> 
                                				

															<blockquote>
																<p>เมื่อท่าน ตรวจสอบเสร็จแล้วหน้าจอด้านขวาจะมี Dialog ให้ท่านบันทึกผลคะแนน ท่านสามารถคลิ๊กเลือกผลคะแนน โดยเริ่มจาก 0 - 10 เพื่อส่งให้กับผู้อนุมัติในลำดับถัดไป</p>
																<footer>System, <cite title="Source Title"></cite></footer>
															</blockquote>
														</div>

														
													</div>
												</div>
												<!-- /blog post -->


												<!-- Blog post -->
												<div class="timeline-row">
													<div class="timeline-icon">
														<img src="<?php echo base_url(); ?>assets/images/placeholder.jpg" alt="">
													</div>
                                                    
                                                    
                                                    
                                                    <div class="panel panel-flat timeline-content">
														<div class="panel-heading">
															<h6 class="panel-title">ข้อมูลอื่นๆ</h6>
															
														</div>

														<div class="panel-body">
															
															 <!-- Top component -->
					<div class="content-group border-top-lg border-top-primary">
						<div class="page-header page-header-default pt-20">
							

							<div class="page-header-content">
								
                                <div class="page-title">
								
                                
                                
                                
                        
                               
                               <div class="row">
                               
                                <div class="col-md-12">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">บริษัทของท่านมีการจัดกิจกรรม CSR ให้กับลูกค้าของท่านหรือไม่ <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">กิจกรรมเพื่อสังคมของบริษัท ที่มีกับผู้ประกอบการ</p>
								
								<hr>
                                
                                <div class="col-lg-6">
                               
                                 <div class="input-group">
												<span class="input-group-addon" style="     padding-right:10px !important;">
													<input type="radio" name="stacked-radio-left1" class="styled" checked disabled>
												</span>
												<input type="text" class="form-control" placeholder="มีการจัดกิจกรรมโปรดระบุ" value="ออกค่ายอาสาเพื่อน้อง" readonly>
											</div>
                                        
                                        
                                      
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled"  disabled >
												อยู่ระหว่างการจัดตั้งโครงการ
											</label>
										</div>

										<div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled>
												ยังไม่ได้เริ่มดำเนินการ
											</label>
										</div>
                               
                               </div>
                               
                               	<div class="col-lg-6">
                                
                                <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม : </label>
		                                    <textarea name="experience-description" rows="3" cols="4" placeholder="" class="form-control" readonly>ออกค่ายอาสาเพื่อพัฒนาชุมชนที่ห่างไกล ได้มีโอกาสทางการศึกษาและก้าวทันเทคโนโลยี เมื่อวันที่ 23/02/2016 สำหรับข้อมูลเพิ่มเติม https://www.im2market.com/2016/08/02/3452 </textarea>
		                                </div>
                                </div>
                               
                                       
                                
                               
			                    
							</div>
                               </div>
                               
                               
                               
                               </div>
                               
                               
                                <div class="row">
                               
                                <div class="col-md-6">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO-9000 หรือไม่ <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
								
								<hr>
                                
                                
                                 <!-- Styled checkbox group -->
									<div class="form-group">
										
										  
                                      
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left2" class="styled" checked disabled >
												ได้รับการรับรอง
											</label>
										</div>
                                       
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled >
												อยู่ระหว่างดำเนินการขอ
											</label>
										</div>

										<div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled>
												ยังไม่ได้เริ่มดำเนินการ
											</label>
										</div>

									</div>
									<!-- /styled checkbox group -->
                                    
                                     
                                       
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control" readonly>Last Update : 10.07.17 Effective 3Years.</textarea>
		                                </div>
                                        
                                        <div class="form-group">
                                    <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
											
                                                <a href="http://www.radiall.com.cn/catalog/ISO9001_Certification.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดู เอกสารใบรับรอง / เอกสารเพิ่มเติม </a>
                                                    
                                                    
		                                </div>
                               
                                
                                
			                    
							</div>
                               </div>
                               
                               <div class="col-md-6">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO-14000 หรือไม่ <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
								
								<hr>
                                
                                
                                
                                 <!-- Styled checkbox group -->
									<div class="form-group">
										
										  
                                      
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left3" class="styled" checked disabled >
												ได้รับการรับรอง
											</label>
										</div>
                                       
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled >
												อยู่ระหว่างดำเนินการขอ
											</label>
										</div>

										<div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled>
												ยังไม่ได้เริ่มดำเนินการ
											</label>
										</div>

									</div>
									<!-- /styled checkbox group -->
                                    
                                     
                                       
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control" readonly>Last Update : 10.07.17 Effective 3Years.</textarea>
		                                </div>
                                        
                                        <div class="form-group">
                                    <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
											
                                                <a href="http://www.radiall.com.cn/catalog/ISO9001_Certification.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดู เอกสารใบรับรอง / เอกสารเพิ่มเติม </a>
                                                    
                                                    
		                                </div>
                                
                                
			                    
							</div>
                               </div>
                               
                               </div>
                               
                               <div class="row">
                               
                                <div class="col-md-6">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO-18000 หรือไม่ <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
								
								<hr>
                                
                                 
                                 <!-- Styled checkbox group -->
									<div class="form-group">
										
										  
                                      
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left4" class="styled" checked disabled >
												ได้รับการรับรอง
											</label>
										</div>
                                       
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled >
												อยู่ระหว่างดำเนินการขอ
											</label>
										</div>

										<div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled>
												ยังไม่ได้เริ่มดำเนินการ
											</label>
										</div>

									</div>
									<!-- /styled checkbox group -->
                                    
                                     
                                       
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control" readonly>Last Update : 10.07.17 Effective 3Years.</textarea>
		                                </div>
                                        
                                        <div class="form-group">
                                    <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
											
                                                <a href="http://www.radiall.com.cn/catalog/ISO9001_Certification.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดู เอกสารใบรับรอง / เอกสารเพิ่มเติม </a>
                                                    
                                                    
		                                </div>
                                
                                
			                    
							</div>
                               </div>
                               
                               <div class="col-md-6">
                               <div class="panel panel-body border-top-danger">
								<h6 class="no-margin text-semibold">บริษัทของท่านได้รับการรับรองมาตรฐาน ISO TSI16949 หรือไม่ <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	โปรดระบุข้อมูลเกี่ยวกับมาตรฐานที่ท่านได้รับ</p>
								
								<hr>
                                
                                
                                
                                 <!-- Styled checkbox group -->
									<div class="form-group">
										
										  
                                      
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left5" class="styled" checked disabled >
												ได้รับการรับรอง
											</label>
										</div>
                                       
                                        <div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled >
												อยู่ระหว่างดำเนินการขอ
											</label>
										</div>

										<div class="radio">
											<label>
												 <input type="radio" name="stacked-radio-left" class="styled" disabled>
												ยังไม่ได้เริ่มดำเนินการ
											</label>
										</div>

									</div>
									<!-- /styled checkbox group -->
                                    
                                     
                                       
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control" readonly>Last Update : 10.07.17 Effective 3Years.</textarea>
		                                </div>
                                        
                                        <div class="form-group">
                                    <label>เอกสารใบรับรอง / เอกสารเพิ่มเติม : </label>
											
                                                <a href="http://www.radiall.com.cn/catalog/ISO9001_Certification.pdf" target="_blank" class="btn btn-default"><i class="icon-stamp position-left"></i> ดู เอกสารใบรับรอง / เอกสารเพิ่มเติม </a>
                                                    
                                                    
		                                </div>
                               
                                
                                
			                    
							</div>
                               </div>
                               
                               </div>
                               
                                
                                
                           
                                      
                                	
                                
                                <div class="row">
                                <div class="col-md-12">
                               
                               <div class="form-group">
											<label>ความคิดเห็นอื่นๆ เพิ่มเติม : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control" readonly>บริษัท เปิดดำเนินกิจการมานาน กว่า 80 ปีมั่นใจได้ถึงมาตรฐานและ ได้รับความไว้วางใจจากลูกค้าชั้นนำทั่วปะเทศ</textarea>
		                                </div>
                               </div>
                               
                           
                               
                               </div>
                                
                                
                                <div class="row">
									



									<div class="col-sm-12 text-right">
										<label class="checkbox-inline">
											<input type="checkbox" name="agreement" id="agreement" class="styled" checked disabled>
											ข้าพเจ้าขอรับรองว่าข้อมูลดังกล่าวถูกต้อง
										</label>
									</div>
								</div>
                                
                                <div class="row">
                                
                               <div class="col-md-3 col-md-offset-9  text-right">
												<strong>	<input type="text" class="form-control" value="John Lennon" readonly></strong>
													<span class="help-block text-right">ลงชื่อผู้บันทึกข้อมูล</span>
												</div>
                                </div>
                       
                       
                                
                              
                                 
                                
                                
                                
							</div>

								
							</div>
						</div>
					</div>
					<!-- /top component -->   
                    
                              
							
							
															

															<blockquote>
																<p>เมื่อท่าน ตรวจสอบเสร็จแล้วหน้าจอด้านขวาจะมี Dialog ให้ท่านบันทึกผลคะแนน ท่านสามารถคลิ๊กเลือกผลคะแนน โดยเริ่มจาก 0 - 10 เพื่อส่งให้กับผู้อนุมัติในลำดับถัดไป</p>
																<footer>System, <cite title="Source Title"></cite></footer>
															</blockquote>
														</div>

														
													</div>

													
												</div>
												<!-- /blog post -->

											

												<!-- Messages -->
												<div class="timeline-row">
													<div class="timeline-icon">
														<div class="bg-info-400">
															<i class="icon-comment-discussion"></i>
														</div>
													</div>

													<div class="panel panel-flat timeline-content">
														<div class="panel-heading">
															<h6 class="panel-title">Results | สรุปผลการตรวจ</h6>
															<div class="heading-elements">
																
										                	</div>
														</div>

														<div class="panel-body">
															
 <form class="form-validation form-validate-jquery" id="regis_st2" action="#"  method="post">
									                    	<textarea name="enter-message" class="form-control content-group" rows="3" cols="1" placeholder="หมายเหตุอื่นๆ เพิ่มเติม..."></textarea>

									                    	<div class="row">
									                    		

									                    		<div class="col-xs-12 text-right">
                                                                	
                                                                 
										                            <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Approve</button>
                                                  or                    <button type="button" class="btn bg-warning-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Reject</button> or
                                                                     
                                                                      <button type="button" class="btn bg-danger-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Cancel</button>
									                    		</div>
									                    	</div>
                                                            </form>
														</div>
													</div>
												</div>
												<!-- /messages -->

											</div>
									    </div>
									    <!-- /timeline -->

									</div>

									
								</div>
							</div>
						</div>

						<div class="col-lg-3">

							<!-- Navigation -->
					    	<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><strong>For Staff</strong></h6>
									
								</div>


								

								<div class="list-group no-border no-padding-top">
                                
                                
                                
                                
                                <div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">1.คุณภาพสินค้าได้รับการอนุมัติให้สามารถใช้งานได้   <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">2.การกำหนดราคาขายเทียบกับราคาที่ซื้อ ณ ปัจจุบัน   <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">3.เงื่อนไขในการชำระเงิน   <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">4.ระยะเวลากำหนดส่งมอบสินค้า หลังจากได้รับใบสั่งซื้อ  <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">5. จำนวนขั้นต่ำในการสั่งซื้อ   <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">6.เอกสาร Specification และ MSDS (กรณีผู้ส่งมอบวัตถุดิบ)   <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">7. หนังสือรับรองบริษัท/ทะเบียนการค้า/ภพ.09 หรือ ภพ.20    <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">8.ความรู้ความสามารถของตัวแทนขายกับตัวสินค้า และจิตสำนึกในการให้บริการ   <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">9.มาตรฐานรับรองที่บริษัทได้รับ    <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								<hr>
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            <div class="list-group-divider"></div>
                            	<div class="list-group-item ">
							
                                	<h6 class="no-margin text-semibold">10.การจัดกิจกรรมเพื่อสังคมของบริษัท    <span class="text-danger">*</span></h6>
								<p class="content-group-sm text-muted">	</p>
								
								
                                
                                
                                
                    <div class="form-group">
                            <div class="ion-custom-strings" ></div>
                         
                    </div>
                                      <div class="form-group">
											<label>หมายเหตุ / รายละเอียดเพิ่มเติม (ถ้ามี) : </label>
		                                    <textarea name="experience-description" rows="2" cols="4" placeholder="" class="form-control"></textarea>
		                                </div>
                                        
                                        
                               
                                
			                    
							</div>
                            	
                                
                          
                            
                            
								</div>
							</div>
							<!-- /navigation -->


							

						</div>
					</div>
					<!-- /user profile -->
                
         
						


                    
                    
			 
                    

					
                    
		        

		         


		                 <!-- Primary modal -->
					<div id="modal_theme_primary" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									
									<h6 class="modal-title">ข้อความจากระบบ</h6>
								</div>

								<div class="modal-body">
									<h6 class="text-semibold">แจ้งผู้ค้ารายใหม่</h6>
									<p> ทางบริษัทได้รับข้อมูลของท่านแล้ว ขณะนี้อยู่ระหว่างการตรวจสอบจากทางเจ้าหน้าที่  </p>

									<hr>

									
									<p>หากตรวจพบว่าข้อมูลของท่านไม่ถูกต้อง บริษัทฯ ขอสงวนสิทธิ์ ในการสั่งซื้อสินค้าจาก ท่าน หรือ หากท่านไม่ได้รับความเป็นธรรมในการให้บริการจาก ฝ่ายจัดซื้อโปรดติดต่อ Tel. 02 345 6789 หน่วยงาน ตรวจสอบ </p>
									
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">รับทราบ</button>
									
								</div>
							</div>
						</div>
					</div>
					<!-- /primary modal -->



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->


				

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   



  
 <script type="text/javascript">

// Custom tag class

    $('.tokenfield').tokenfield();
    $('.tagsinput-custom-tag-class').tagsinput({
        tagClass: function(item){
            return 'label bg-success';
        }
    });
	
	    // Display values
    $('.tags-input, [data-role="tagsinput"], .tagsinput-max-tags, .tagsinput-custom-tag-class').on('change', function(event) {
        var $element = $(event.target),
            $container = $element.parent().parent('.content-group');

        if (!$element.data('tagsinput'))
        return;

        var val = $element.val();
        if (val === null)
        val = "null";
    
        $('pre.val > code', $container).html( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
        $('pre.items > code', $container).html(JSON.stringify($element.tagsinput('items')));
        Prism.highlightAll();
    }).trigger('change');




function noti(val) {
	
	 new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
	
    //alert("The input value has changed. The new value is: " + val);
}

	 $('#pnotify-successx').on('click', function () {
        new PNotify({
            title: 'Auto Save',
            text: 'ระบบได้ทำการบันทึกข้อมูลของท่านโดยอัติโนมัติเรียบร้อยแล้ว',
            icon: 'icon-checkmark3',
            type: 'success'
        });
    });

 // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();




    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });


	

    // Select2 select
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });



 var validatorst1 = $("#regis_st2").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 1
            },
            styled_checkbox: {
                minlength: 1
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะถูกส่งไปยังผู้อนุมัติในลำดับต่อไป ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ระบบได้ส่งข้อมูลไปยังผู้อนุมัติขั้นต่อไปเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				
					
				$.blockUI({ 
           // message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='prapprove.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึก)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		/*	
			$.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i> <p style="font-size: 17px;"> Please Wait System being Processing | กรุณารอซักครู่ระบบกำลังตรวจข้อมูล </p>',
			timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            },
			onUnblock: function() { 
               // alert('Page is now unblocked. FadeOut completed.'); 
			   
			   	
			 
			 		$.ajax({
					
			url: "checklogin.php",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					
					token	: 	$('#token').val(),
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
			
					$.blockUI({ 
            message: '<i class="icon-shield-check " style="font-size: 60px; color: green;"></i> <p style="font-size: 17px;"> บันทึกข้อมูลสำเร็จ   </p>',
           timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='vendorregister.php';
  }  , 1000 );
				
				
				}
            }
        });
				
				
				
				//alert();
				
				
				

			
        	
				
					
		
			
				
		
				
			}
		});	
		
			   
			   
            } 
        });
		
		*/
					
				//	setTimeout($.unblockUI, 2000);
	
			/*	$.ajax({
					
					
					url: "index.php",       
					type: "POST",
                    data: { 
					
					
					
							
							username	: 	$('#username').val(),
							
		
						
					
					},
					success: function(data) { setTimeout($.unblockUI, 2000);} 
		}); */
		
					//alert(username);
		
            }
    });

 //alert('test');
    // Reset form
    $('#reset').on('click', function() {
        //validator.resetForm();
		validatorst1.resetForm();
    });


	
   // With validation
   
	
 $(window).on('load',function(){
	 
	 
	 /*swal({
		 
		  title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบระหว่างนี้ท่านจะไม่สามารถแก้ไข ข้อมูลใดๆ ได้ จนกว่าทางเจ้าหน้าที่ของ TOA GROUP ตรวจสอบข้อมูลเสร็จ",
            confirmButtonColor: "#66BB6A",
            type: "success",
			confirmButtonText: "ตกลง",
		 
			
			   title: "ข้อมูลของท่านอยู่ระหว่างตรวจสอบ",
            text: "ขณะนี้ทาง เราได้รับข้อมูลของท่านเรียบร้อยแล้ว และอยู่ระหว่างตรวจสอบข้อมูลดังกล่าว โดยเมื่อข้อมูลของท่านผ่านการ ตรวจสอบแล้ว ท่านจะได้รับ Email จากเรา ตามที่ท่านได้ลงทะเบียนไว้กับระบบ",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        });*/
	 
	// $('#modal_theme_primary').modal('show');
	 //validation-next
	/* $('#validation-next').click(function(){
		 $('#modal_theme_primary').modal('show');
		 });*/
       // $('#modal_theme_primary').modal('show');
	  
	 /* bootbox.dialog({
                title: "กรุณาระบุรหัสผ่าน",
                message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                        '<form class="form-horizontal">' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">Name</label>' +
                                '<div class="col-md-8">' +
                                    '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                                    '<span class="help-block">Here goes your name</span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="col-md-4 control-label">How awesome is this?</label>' +
                                '<div class="col-md-8">' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked">' +
                                            'Really awesomeness' +
                                        '</label>' +
                                    '</div>' +
                                    '<div class="radio">' +
                                        '<label>' +
                                            '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                                            'Super awesome' +
                                        '</label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '</div>',
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            bootbox.alert("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        ); */
    });
	
	 
	/*
	 $('#validation-next').on('click', function() {
        
		  swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
    });*/
	
	
	    $('#onshown_callback').on('click', function() {
        $('#modal_form_vertical').on('shown.bs.modal', function() {
            alert('onShown callback fired.')
        });
    });

 // Custom bootbox dialog with form
   

 $(".ion-custom-strings").ionRangeSlider({
        grid: true,
        from: 0,
        values: [
            "0", "1",
            "2", "3",
            "4", "5",
            "6", "7",
            "8", "9",
            "10"
        ]
    });
</script>



</body>




</html>