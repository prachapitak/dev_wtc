<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title> <?php echo $title;?></title>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>logo.ico">
     
     <?php $this->load->view('main/allcss');?>
      <?php $this->load->view('main/alljs3');?>
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/datatables_extension_fixed_columns.js"></script>
    
    
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/daterangepicker.js"></script>


    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
	
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	


	
	
   
	
    
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>


</head>


<body>


	<?php $this->load->view('main/navbar');?>


    
 


	<!-- Page container -->
	<div class="page-container">


        
        
		<!-- Page content -->
		<div class="page-content">
        
        <?php $this->load->view('main/navigation');?>

			


			<!-- Main content -->
			<div class="content-wrapper">

			
					
	


				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right15 position-left"></i> <span class="text-semibold">PO MANAGEMENT   </span> -  WTC  <?php // echo print_r($this->session->userdata());?></h4>
						</div>
                        
                        
                        <?php //$this->load->view('dashboard/wtc/headnoti');?>

						
					</div>

					
                    
                    
                    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>"><i class="icon-home2 position-left"></i> Dashboard</a></li>
							
							<li class="active">PO MANAGEMENT</li>
						</ul>

						
					</div>
                    
				</div>
				<!-- /page header -->

<?php //$this->load->view('dashboard/wtc/noti');?>
				
                                    

				<!-- Content area -->
				<div class="content">
                
            
            
						
                        
                          <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">PO List : รายการ PO </h5>
                            
                            <br>
                       
							
                        
                            
                            
                              <button type="button" class="btn btn-success btn-sm legitRipple" data-toggle="modal" data-target="#m_add_po">+ Add New PO <i class="icon-play3 position-right"></i></button>
                            
						</div>
                        
                        
                        
                      
                        
                        
                        

						<div class="panel-body">
                        <!--<div class="alert alert-primary no-border">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">คำแนะนำ!</span> รู้หรือไม่ท่านสามารถ ท่านสามารถ กดปุ่ม 
							 <code>Reject</code> เพื่อยกเลิกคู่ค้าออกจาก ระบบ หรือกดปุ่ม <code>ReEdit</code> เพื่อให้ระบบทำการแจ้งไปที่ Vendor ให้เข้ามาทำการแก้ไขข้อมูลอีกครั้ง และ เข้า สู่ขั้นตอนการ Approve อีกครั้ง 
								    </div>-->
                
                          <table class="table datatable-fixed-left" width="100%">
							<thead>
						        <tr>
                                 				
                                                <th >#</th>
                                                <th >PO Number</th>
												
												<th >PO Date</th>
                                             
												<th >Company</th>
                                                <th >Customer</th>
                                                <th > TOTAL VALUE</th>
                                                 
											 
						            <th class="text-center">Actions</th>
						        </tr>
						    </thead>
						    <tbody>
                            
                            <?php
                           // print_r($get_po);
							
							if($get_po){
								foreach($get_po as $get_pos){
									
									//Status
									//echo $get_pos->Status;
									?>
                                    
                                  <tr>
						            <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class=""><a href="<?php echo base_url(); ?>dashboard/podetailview/<?php echo $get_pos->po_id;?>/" class="text-default text-semibold"><?php echo $get_pos->po_id;?>   </a></div>
														
                                                        <!--<div class="text-muted text-size-small">
                                                         
                                                      
                                                        
                                                       
															
														</div>-->
													</div>
                                                    
                                                    
												</td>
                                               
                                                <td>
                                                
                                                
                                                
													
													<div class="media-left">
														<div class="media-left">
														<div class=""> 
														<?php echo $get_pos->po_number;?> 
														
														
                                                         </div>
														
													</div>
														
													</div>
												</td>
												<td>
                                                
                                                <div class="media-left">
														
														<div class="text-muted text-size-small">
                                                        
                                                        <?php echo $get_pos->po_date;?>
													
                                                   
															
														</div>
													</div></td>
                                                    
                                                   
                                                   
												<td>
                                                
                                                <div class="media-left">
														<div class=""> 
														
                                                        
                                                        	<?php echo $get_pos->customer_company_en;?> <br>
                                                            <?php echo $get_pos->customer_company_th;?> 
                                                        
															
 </div>
														
													</div>
                                                    
                                                    
                                               </td>
												<td>
                                                
                                                
                                                
                                                
                                                <div class="media-left">
														<div class="">
                                                        
                                                        
                                                        <?php echo $get_pos->enduser_name_th;?>
														
                                                    </div>
                                                        
														
														
													</div>
                                                
                                            </td>
												<td>
                                                
                                                <div class="media-left">
														<div class="">
                                                        
                                                        <?php echo $get_pos->total_value;?>
                                                        
                                                     </div>
                                                        
														
                                                        
														
													</div>
                                               </td>
                                               
											
						            <td class="text-center">
                                    
                                    <ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
															<ul class="dropdown-menu dropdown-menu-right">
                                                            
                                                            <li><a href="<?php echo base_url(); ?>dashboard/podetailview/<?php echo $get_pos->po_id;?>/"><i class="icon-eye"></i> View PO</a></li>
																<li><a href="<?php echo base_url(); ?>dashboard/podetail/<?php echo $get_pos->po_id;?>/"><i class="icon-file-stats"></i> Edit PO</a></li>
																
																
																<li class="divider"></li>
																<li>
                                                               
                                                                <a href="#nogo" onclick="delete_po('<?php echo $get_pos->po_id;?>','<?php echo $get_pos->po_number?>')"><i class="icon-bin"></i> Remove PO </a></li>
															</ul>
														</li>
													</ul>
                                    
                                    
													
												</td>
						        </tr>  
                                    <?php
									
									}
								
								}
							
							?>
                            
                            
                            
                            
                          
                             
                                
                             
                            
                            
                              
                            </tbody>
                            
						</table>
                             
						</div>

						
                        
                        
                        
                        
                        
					</div>
					<!-- /basic datatable -->




                    
                    
			
                    

					
                    
		        

		         



					<!-- Footer -->
                    <?php $this->load->view('main/footer');?>
				
					<!-- /footer -->
                    
                  


				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
   
        <!-- Vertical form modal -->
					<div id="m_add_po" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"> <i class="icon-arrow-right15 position-left"></i> Add New PO</h5>
								</div>

								   <form class="form-validation form-validate-jquery" id="regis_st1" action="#"  method="post" enctype="multipart/form-data" >
									
                                      
                                      <div class="modal-body">
                                    
                                    
                             
                                                
                                                
                                                
                                                <div class="form-group">
											<div class="row">
												

												<div class="col-sm-6">
													<label>PO No.</label>
													<input type="text" placeholder="PO No." class="form-control required" id="PONumber" name="PONumber">
												</div>
                                                
                                                <div class="col-sm-6">
													<label>PO Date</label>
													<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar5"></i></span>
											<input type="text" class="form-control pickadate-accessibility required" placeholder="Try me&hellip;" id="IssuedDate" name="IssuedDate">
										</div>
												</div>
											</div>
										</div>
                                                
                                                <div class="form-group">
											<div class="row">
                                            
												<div class="col-sm-12">
													  <label>Customer Name</label>
                                                      
                                                      
                                                      
                                         <select class="bootstrap-select required" data-live-search="true" data-width="100%" id="customer_enduser_id" name="customer_enduser_id">
                                        
                                        
                                        
                                        <?php
										
										//print_r($brand);
                                        if($get_customer){
											foreach($get_customer as $get_customers){
												
												
												?>
                                                
                                                
                                                <optgroup label="<?php echo $get_customers->Company_en;?>">
                                                <?php
                                                $get_enduser = $this->Query_Db->result($this->db->dbprefix.'customer_enduser','customer_id ='.$get_customers->id.'');
												if($get_enduser ){
													foreach($get_enduser as $get_endusers){
														
														echo '<option value="'.$get_endusers->customer_id.'"';
														
														
														echo '>'.$get_endusers->name_en.' '.$get_endusers->surname_en.'</option>';
														
														}
													
													
													}
												
												?>
                                                
                                               
                                                </optgroup>
                                                <?php
												
												}
											}
										?>
											
										</select>             
                                                      
                                                      
										
												</div>

												
                                                
                                                
                                                
											</div>
										</div>

										<div class="form-group">
											<div class="row">
                                            <div class="col-sm-4">
													<label>Total Value</label>
													<input type="text" placeholder="Total Value"  class="form-control required" id="TotalValue" name="TotalValue">
												</div>
                                            
												<div class="col-sm-4">
													<label>VAT Rate</label>
													<input type="text" placeholder="Vat Rate."  class="form-control required" id="VATRate" name="VATRate">
												</div>

												<div class="col-sm-4">
													<label>SL Number</label>
													<input type="text" placeholder="SL Number" class="form-control required" id="SlNumber" name="SlNumber">
												</div>
											</div>
										</div>

										
                                     
                                                
                                                </div>
                              
                                        
                                      
                                        
                                        
                                   

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">ยกเลิก</button>
										<button type="submit" class="btn btn-primary">Next Step</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /vertical form modal -->


 
<script type="text/javascript">

  	 $('.pickadate-accessibility').pickadate({
        labelMonthNext: 'Go to the next month',
        labelMonthPrev: 'Go to the previous month',
        labelMonthSelect: 'Pick a month from the dropdown',
        labelYearSelect: 'Pick a year from the dropdown',
        selectMonths: true,
        selectYears: true,
		format: 'yyyy-mm-dd',
		formatSubmit: 'yyyy-mm-dd'
    });
	 $('.bootstrap-select').selectpicker();
	 
	 
	  function delete_po(thisid,thisname){
   //alert(thisid);
   
  	swal({
			
			   title: "Are you sure? | คุณต้องการลบเอกสารนี้หรือไม่ ? ",
            text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะหายไป",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "Yes Delete it. | ยืนยันการลบ",
			cancelButtonText: "No | ยกเลิก ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
	 function(isConfirm){
            if (isConfirm) {
                
				swal({
                    title: "File Deleted!",
                    text: "ไฟล์ที่ท่านเลือกถูกลบเรียบร้อยแล้ว",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
				
				
					
                });
				
				
				$.ajax({
				
				
				url: '<?php echo base_url(); ?>dashboard/deletepo',	
			//url: "<?php echo base_url(); ?>vendorregister/vp1_fdelete2",        // Url to which the request is send
			type: "POST",
			
			
             data: { 
					fileid		: thisid,
					filename 	: 	thisname,
					
					},
			 success: function(data)   // A function to be called if request succeeds
			{
				
				
			setTimeout(function(){
   location.reload();
  },1000)
				
			
				
			}
		});		
				
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการลบ",
                    text: "Your  data has been Unsave. | ยกเลิกการลบ",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
		
		
   
}  


	 
	 
    var validatorst1 = $("#regis_st1").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
      
		submitHandler: function (form) {
	
			
			swal({
			
			   title: "Are you sure? | คุณต้องการบันทึกข้อมูลหรือไม่ ? ",
           // text: "You will not be able to recover this data! | เมื่อท่านกดตกลงข้อมูลดังกล่าวจะไม่สามารถแก้ไขได้จนกว่าจะได้รับการอนุมัติจากเจ้าหน้าที่ TOA ",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            confirmButtonText: "บันทึกและดำเนินการต่อ",
			cancelButtonText: "ยังไม่บันทึก และกลับไปแก้ไข ",
            closeOnConfirm: false,
            closeOnCancel: false  
			  
           
        },
		  function(isConfirm){
            if (isConfirm) {
                
				
				
		
		 var formData = new FormData($("#regis_st1")[0]);
		  $.ajax({ 
		  			 url: '<?php echo base_url(); ?>dashboard/createpo',
					
                     //url:"<?php //echo base_url(); ?>main/ajax_upload",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data: formData,  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
			//alert (data);
			
			 var resultAjax = JSON.parse(data);
			 
			 console.log(resultAjax);
			 
			 if(resultAjax.upload_st == 'true'){
				
				 swal({
                    title: "Data Save!",
                    text: "ข้อมูลของท่านถูกบันทึกเรียบร้อยแล้วระบบกำลังพาท่านไปยังขั้นตอนต่อไป",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
					
                });
				 
				  $.blockUI({ 
           
           timeout: 5000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent',
				onUnblock: function() { 
				
				 setTimeout( function(){ 
    // Do something after 1 second 
	//$("#displayerror").html(data);
	window.location='<?php echo base_url(); ?>dashboard/podetail/'+resultAjax.po_id+'/';
	//location.reload();
  }  , 1000 );
				
				
				}
            }
        });
				 
				 } else {
					 
					 //alert('fail');
					 swal({
            title: "Oop.. File type  incorrect...",
            text: "ขออภัยข้อมูลไม่ถูกต้อง | Sorry This file type incorrect.",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
					 }
				//console.log(data);
					 
		
						 
                     }  
                });  	
					
				
					
				
				
				
				
				
            }
            else {
                swal({
                    title: "ยกเลิกการบันทึก",
                    text: "Your  data has been Unsave. | ข้อมูลของคุณยังไม่ถูกบันทึกกรุณากรอกข้อมูลให้เรียบร้อย)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
			
		
		
            }
    });

</script>


</body>




</html>